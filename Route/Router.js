const _ = require('lodash');

const ReflectionCollection = use('Apricot/Reflection/Collection');

const RouteCollection = use('Apricot/Route/Collection');
const Route = use('Apricot/Route');
const ResourceRegistrar = use('Apricot/Route/ResourceRegistrar');

const NotBeFunctionException = use('Exceptions/NotBeFunction');

class Router {
	constructor () {
		this._routes = new RouteCollection();
		this._groupStack = [];
		this._patterns = {};
		this._middleware = [];
		this._reflection = null;
	}

	get (url, action = null) {
		return this._addRoute(['GET', 'HEAD'], url, action);
	}

	post (url, action = null) {
		return this._addRoute('POST', url, action);
	}

	put (url, action = null) {
		return this._addRoute('PUT', url, action);
	}

	patch (url, action = null) {
		return this._addRoute('PATCH', url, action);
	}

	delete (url, action = null) {
		return this._addRoute('DELETE', url, action);
	}

	options (url, action = null) {
		return this._addRoute('OPTIONS', url, action);
	}

	any (url, action = null) {
		return this._addRoute(['GET', 'HEAD', 'POST', 'PUT', 'PATCH', 'DELETE'], url, action);
	}

	use (namespace) {
	    return new Promise(resolve => {
            ReflectionCollection.instance
                .find(namespace)
                .then(reflection => {
                    this._reflection = reflection;

                    use(namespace);

                    this._reflection = null;

                    resolve();
                });
        });
	}

	match (methods, url, action = null) {
		return this
			._addRoute(
				_
					.map(methods, method => {
						return method.toUpperCase();
					}),
				url,
				action
			);
	}

	ws (url, action = null) {
		return this._addRoute('WS', url, action);
	}

	resources (resources) {
		for (var name in resources) {
			this.resource(name, resources[name])
		}
	}

	resource (name, controller, options) {
		let registrar = new ResourceRegistrar(this);

		registrar.register(name, controller, options);
	}

	auth () {
		this.get('login', 'Auth/AuthController.showLoginForm');
		this.post('login', 'Auth/AuthController.login');
		this.get('logout', 'Auth/AuthController.logout');

		// Registration Routes...
		this.get('register', 'Auth/AuthController@showRegistrationForm');
		this.post('register', 'Auth/AuthController@register');

		// Password Reset Routes...
		this.get('password/reset/{token?}', 'Auth/PasswordController.showResetForm');
		this.post('password/email', 'Auth/PasswordController.sendResetLinkEmail');
		this.post('password/reset', 'Auth/PasswordController.reset');
	}

	group (attributes, callback) {
		this._updateGroupStack(attributes);

		let promise = callback.call(this);

		if (promise && promise.constructor.name == 'Promise') {
		    promise.then(() => {
                this._groupStack.pop();
            });
        } else {
            this._groupStack.pop();
        }
	}

	dispatch (request) {
		return this
			._routes
			.match(request);
	}

	_addRoute (methods, url, action = null) {
		return this
			._routes
			.add(
				this._createRoute(methods, url, action)
			);
	}

	_createRoute (methods, url, action = null) {
		if (Function.isGenerator(action)) {
			throw new NotBeFunctionException(url, methods);
		}

		if (this._actionReferencesController(action)) {
			action = this._convertToControllerAction(action);
		}

		var route = this._newRoute(methods, this._prefix(url), action);
		
		if (Function.isGenerator(route.action.uses)) {
			let doc = this._reflection
				.methods
                .find(method => {
                    let tags = method.tags
                        .filter(tag => {
                           return ['http', 'url'].indexOf(tag.title) > -1;
                        });

                    if (tags.length > 0) {
                        for (let i = 0; i < tags.length; i++) {
                            let tag = tags[i];

                            switch (tag.title) {
                                case 'http':
                                    if (methods.indexOf(tag.value) == -1) {
                                        return false;
                                    }

                                    break;
                                case 'url':
                                    if (url != tag.value) {
                                        return false;
                                    }

                                    break;
                            }
                        }

                        return true;
                    } else {
                        return false;
                    }
                });
			
			if (doc) {
			    route.action.uses.parameters = doc.parameters;
            }
		}

		if (this._hasGroupStack()) {
			this._mergeGroupAttributesIntoRoute(route);
		}

		this._addWhereClausesToRoute(route);

		return route;
	}

	_actionReferencesController (action) {
		if (_.isFunction(action)) {
			return false;
		}

		return _.isString(action) ||
			(
				action['uses'] !== undefined &&
				_.isString(action['uses'])
			);
	}

	_convertToControllerAction (action) {
		if (_.isString(action)) {
			action = {'uses': action}
		}

		if (this._groupStack.length > 0) {
			action['uses'] = this._prependGroupUses(action['uses']);
		}

		action['controller'] = action['uses'];

		return action;
	}

	_prependGroupUses (uses) {
		var group = _.last(this._groupStack);

		return group['namespace'] !== undefined ?
			group['namespace'] + '/' + uses :
			uses; //&& uses.indexOf('/') !== 0  testing
	}

	_newRoute (methods, url, action = null) {
		let route = new Route(methods, url, action);
		route.router = this;

		return route;
	}

	_prefix (url) {
		let result = _
			.trim(
				`${ _.trim(this._getLastGroupPrefix(), '/') }/${ _.trim(url, '/') }`,
				'/'
			);

		return result ? result : '/';
	}

	_getLastGroupPrefix () {
		if (this._groupStack.length > 0) {
			var last = _.last(this._groupStack);

			return last['prefix'] !== undefined ?
				last['prefix'] :
				'';
		}

		return '';
	}

	_hasGroupStack () {
		return this._groupStack.length > 0;
	}

	_mergeGroupAttributesIntoRoute (route) {
		let action = this._mergeWithLastGroup(route.action);

		route.action = action;
	}

	_mergeWithLastGroup (data) {
		return this.mergeGroup(data, _.last(this._groupStack));
	}

	_addWhereClausesToRoute (route) {
		let where = route.action['where'] !== undefined ?
			route.action['where'] :
			{};

		route.where(_.merge(this._patterns, where));

		return route;
	}

	_updateGroupStack (attributes) {
		if (this._groupStack.length > 0) {
			attributes = this.mergeGroup(attributes, _.last(this._groupStack));
		}

		this._groupStack.push(attributes);
	}

	mergeGroup (data, old) {
		data['namespace'] = this.formatUsesPrefix(data, old);
		data['prefix'] = this.formatGroupPrefix(data, old);

		if (old['middleware'] !== undefined) {
			data['middleware'] = _.concat(
				old['middleware'],
				data['middleware'] !== undefined ? data['middleware'] : []
			);
		}

		if (data['domain'] !== undefined) {
			delete old['domain'];
		}

		data['where'] = _.merge(
			old['where'] !== undefined ? old['where'] : {},
			data['where'] !== undefined ? data['where'] : {}
		);

		if (old['as'] !== undefined) {
			data['as'] = old['as'] + '.' + (data['as'] !== undefined ? data['as'] : '');
		}

		return _.merge(
			_.pick(old, ['namespace', 'prefix', 'where', 'as']),
			data
		);
	}

	formatUsesPrefix (data, old) {
		if (data['namespace'] !== undefined) {
			return old['namespace'] !== undefined ?
				`${ _.trim(old['namespace'], '\\') }\\${ _.trim(data['namespace'], '\\') }` :
				_.trim(data['namespace'], '\\');
		}

		return old['namespace'] !== undefined ? old['namespace'] : '';
	}

	formatGroupPrefix (data, old) {
		var oldPrefix = old['prefix'] !== undefined ? old['prefix'] : null;

		if (data['prefix'] !== undefined) {
			return `${ _.trim(oldPrefix, '/') }/${ _.trim(data['prefix'], '/') }`;
		}

		return oldPrefix;
	}
}

module.exports = Router;