const _ = require('lodash');

class Request {
    get method() {
        return this._request.method;
    }

    get url() {
        return this
            ._request
            .url
            .replace(/\?.*/, '');
    }

    get rawUrl() {
        return this._request.url;
    }

    get ajax() {
        return 'XMLHttpRequest' === this._request.handles['x-requested-with'];
    }

    get secure() {
        return !!this._request.connection.encrypted;
    }

    get ips() {
        return this._request.connection.remoteAddress;
    }

    get rawRequest() {
        return this._request;
    }

    get headers() {
        return this._request.headers;
    }

    get all() {
        return this._input;
    }

    get cookies() {
        return this._cookies;
    }

    get host() {
        return this.headers('host');
    }

    get protocol() {
        return this._request.httpVersion;
    }

    constructor(request) {
        this._request = request;
        this._input = {};
        this._cookies = {};

        const BodyParser = use('Apricot/Request/Parser/Body');
        this._bodyParser = new BodyParser(this);

        const CookieParser = use('Apricot/Request/Parser/Cookie');
        this._cookieParser = new CookieParser(
            this._request.headers.cookie ?
                this._request.headers.cookie :
                ''
        );

        delete this._request.headers.cookie;
    }

    * make() {
        [this._input, this._files] = yield * this._bodyParser.parse();
        this._cookies = yield * this._cookieParser.parse();
    }

    header(key, defaultValue = false) {
        return this.headers[key] ? this.headers[key] : defaultValue;
    }

    input(key, defaultValue = false) {
        return this.all[key] ? this.all[key] : defaultValue;
    }

    cookie(key, defaultValue = false) {
        return this.cookies[key] ? this.cookies[key] : defaultValue;
    }

    except () {
        let args = _.isArray(arguments[0]) ? arguments[0] : _.toArray(arguments);

        return _.omit(this.all, args);
    }

    only () {
        let args = _.isArray(arguments[0]) ? arguments[0] : _.toArray(arguments);

        return _.pick(this.all, args);
    }
}

module.exports = Request;