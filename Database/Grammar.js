const Expression = use('Apricot/Database/Query/Expression')

class Grammar {
    constructor() {
        this._tablePrefix = '';
    }

    wrap(value, prefixAlias = false) {
        if (this.isExpression(value)) {
            return this.getValue(value);
        }

        if (value.toLowerCase().indexOf(' as ') > -1) {
            return this._wrapAliasedValue(value, prefixAlias);
        }

        return this._wrapSegments(value.split('.'));
    }

	/**
	 * Wrap a table in keyword identifiers.
	 *
     * @public
	 * @param {(Apricot/Database/Query/Expression|String)} table
	 * @return {String}
	 */
    wrapTable (table) {
        if (!this.isExpression(table)) {
            return this.wrap(this._tablePrefix + table, true);
        }

        return this.getValue(table);
    }

    /**
     * Convert an array of column names into a delimited string.
     *
     * @param {Array<String>} columns
     * @return {String}
     */
    columnize (columns) {
        return columns.map(column => {
            return this.wrap(column);
        })
            .join(', ');
    }

    isExpression(value) {
        return value instanceof Expression;
    }

    getValue(expression) {
        return expression.value;
    }

	/**
	 * Create query parameter place-holders for an array.
	 *
     * @public
	 * @param {Object} values
	 * @return {String}
	 */
	parameterize (values) {
	    return Object.values(values)
            .map(parameter => {
                return this.parameter(parameter);
            })
            .join(', ');
    }

	/**
	 * Get the appropriate query parameter place-holder for a value.
	 *
     * @public
	 * @param {Any} value
	 * @return {String}
	 */
	parameter (value) {
		return this.isExpression(value) ? this.getValue(value) : '?';
	}

    _wrapAliasedValue(value, prefixAlias = false) {
        let segments = value.split(/\s+as\s+/i);

        if (prefixAlias) {
            segments[1] = this._tablePrefix + segments[1];
        }

        return `${ this.wrap(segments[0]) } as ${ this._wrapValue(segments[1]) }`;
    }

    _wrapValue(value) {
        if (value !== '*') {
            return `"${ value.replace('"', '""') }"`;
        }

        return value;
    }

    _wrapSegments(segments) {
        return segments.map((segment, key) => {
            return key == 0 && segments.length > 1 ?
                this.wrapTable(segment) :
                this._wrapValue(segment);
        })
            .join('.');
    }
}

module.exports = Grammar;