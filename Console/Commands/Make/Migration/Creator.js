const dateFormat = require('dateformat');
const _ = require('lodash');

const File = use('File');

class Creator {
	get file () {
		return this._file;
	}

	get datePrefix () {
		return dateFormat(new Date(), 'yyyy_mm_dd_HHMMss');
	}

	get templatePath () {
		return this.file.join(__dirname, 'Templates')
	}

	constructor () {
		this._file = File;
	}

	* create (name, path, table = null, create = false) {
		let template = yield * this._getTemplate(table, create);
		path = this._getPath(name, path);

		yield * this.file.write(
			path,
			_.template(template)({
                name: name.studly(),
                table
            })
		);

		return this.file.basename(path);
	}

	* _getTemplate (table, create) {
		return yield * this.file.get(
			this.file.join(
				this.templatePath,
				table === null ?
					'blank.tmp' :
					(
						create ?
							'create.tmp' :
							'update.tmp'
					)
			)
		);
	}

	_getPath (name, path) {
		return this.file.join(path, `${ this.datePrefix }_${ name }.js`)
	}
}

module.exports = Creator;