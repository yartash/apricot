Object.defineProperty(
    Function,
    'isFunction',
    {
        value: value => {
            return Object.prototype.toString.call(value) === '[object Function]';
        },
        configurable: true,
        writable: true,
        enumerable: false
    }
);

Object.defineProperty(
    Function,
    'isGenerator',
    {
        value: value => {
            return Object.prototype.toString.call(value) === '[object GeneratorFunction]';
        },
        configurable: true,
        writable: true,
        enumerable: false
    }
);

Object.defineProperty(
    Function,
    'isPromise',
    {
        value: value => {
            return Object.prototype.toString.call(value) === '[object Promise]';
        },
        configurable: true,
        writable: true,
        enumerable: false
    }
);