Object.defineProperty(
	Boolean,
	'isBoolean',
	{
		value: value => {
			return Object.prototype.toString.call(value) === '[object Boolean]';
		},
		configurable: false,
		writable: false,
		enumerable: false
	}
);