const Command = use('Apricot/Console/Command');

class Migrate extends Command {
	/**
	 * Determine if the given path(s) are pre-resolved "real" paths.
	 *
	 * @protected
	 * @return {Boolean}
	 */
	get usingRealPath () {
		return this.hasOption('realpath') && this.option('realpath');
	}

	/**
	 * Get the path to the migration directory.
	 *
	 * @protected
	 * @return {String}
	 */
	get migrationPath () {
		return databasePath('migrations');
	}

	/**
	 * Get all of the migration paths.
	 *
	 * @protected
	 * @return {Array<String>}
	 */
	get migrationPaths () {
		// Here, we will check to see if a path option has been defined. If it has we will
		// use the path relative to the root of the installation folder so our database
		// migrations may be run for any customized path from within the application.
		if (this.hasOption('path') && this.option('path')) {
			let path = this.option('path');

			return [!this.usingRealPath ? basePath(path) : path];
		}

		return [this.migrationPath].concat(this._migrator.paths);
	}
}

module.exports = Migrate;