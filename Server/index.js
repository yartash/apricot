const fs = require('fs');
const net = require('net');
const http = require('http');
const https = require('https');

const _ = require('lodash');
const type = require('mime');
const relativeToDate = require('relative-to-date');

const Request = use('Apricot/Request');
const Response = use('Apricot/Response');
const ExceptionHandler = use('App/Exceptions/Handlers/Http');
const View = use('Apricot/View');
const ReflectionCollection = use('Apricot/Reflection/Collection');
const Collection = use('Apricot/Support/Collection');

const CertificateNotFoundException = use('Apricot/Exceptions/CertificateNotFound');
const RouteNotFoundException = use('Apricot/Exceptions/RouteNotFound');
const InvalidArgumentException = use('Apricot/Exceptions/InvalidArgument');

class Server {
    constructor() {
        this._instance = null;
        this._config = use('Config').get('app');
        this._controllersPath = 'Http/Controllers';
        this._middleware = use('Middleware');
        this._bind();
    }

    listen(host, port) {
        log.info(`start server listening on ${host}:${port}`);
        this._getInstance().proxy.listen(port, host);
    }

    _getInstance() {
        if (!this._instance) {
            this._instance = {};
            this._instance.proxy = net.createServer(this._proxyHandler.bind(this));

            if (this._config.server.http) {
                this._instance.http = http.createServer(this._serverHandler.bind(this));
            }

            if (this._config.server.https) {
                let key = `bootstrap/ssl/${this._config.server.ssl.key}`;
                let cert = `bootstrap/ssl/${this._config.server.ssl.cert}`;

                if (
                    !fs.existsSync(basePath(key)) ||
                    !fs.existsSync(basePath(cert))
                ) {
                    throw new CertificateNotFoundException();
                }

                this._instance.https = https.createServer(
                    {
                        key: fs.readFileSync(key),
                        cert: fs.readFileSync(cert)
                    },
                    this._serverHandler.bind(this)
                )
            }
        }

        return this._instance;
    }

    _proxyHandler(socket) {
        socket.once('data', buffer => {
            socket.pause();

            let byte = buffer[0];
            let protocol;

            if (byte === 22) {
                protocol = 'https';
            } else if (32 < byte && byte < 127) {
                protocol = 'http';
            }

            let proxy = this._instance[protocol];

            if (proxy) {
                socket.unshift(buffer);
                proxy.emit('connection', socket);
            }

            socket.resume();
        });
    }

    _bind() {
        process.on('unhandledRejection', error => {
            this._executeHandler(this._exceptionHandler(error));
        });
    }

    * _exceptionHandler(error) {
        let handler = new ExceptionHandler(error);
        let response = yield * handler.execute();

        if (
            response instanceof View &&
            error.request
        ) {
            return yield * this._prepareResponse(
                error.request,
                response
            )
        } else {
            return response
        }
    }

    _serverHandler(req, res) {
        let url = req.url.replace(/\?.*$/, '');
        let path = publicPath(url);

        if (
            req.url != '/' &&
            (
                fs.existsSync(path) ||
                (
                    this._config.response.contentEncoding &&
                    fs.existsSync(`${ path }.gz`)
                )
            )
        ) {
            this._renderFile(publicPath(url), res);
        } else {
            new Promise(resolve => {
                this._responseResolve = resolve;
            })
                .then(
                    response => {
                        response.send(res);
                    },
                    error => {
                        log.error(error);
                    }
                );

            this._executeHandler(this._handler(req));
        }
    }

    _executeHandler(generator, yieldValue) {
        let next = generator.next(yieldValue);

        if (!next.done) {
            if (
                next.value.constructor
            ) {
                switch (true) {
                    case next.value.constructor.name === 'Promise':
                        next.value.then(
                            result => {
                                if (result instanceof Collection) {
                                    result.run().then(result => {
                                        this._executeHandler(generator, result);
                                    });
                                } else {
                                    this._executeHandler(generator, result);
                                }
                            },
                            err => {
                                generator.throw(err);
                            }
                        );
                        break;
                    case
                        next.value.constructor[Symbol.toStringTag] &&
                        next.value.constructor[Symbol.toStringTag] === 'GeneratorFunction':

                        break;
					case next.value instanceof Collection:
						next.value.run().then(
							result => {
								this._executeHandler(generator, result.first())
							},
							err => {
								generator.throw(err);
							}
						);
						break;
                }
            }
        } else {
            this._responseResolve(next.value);
        }
    }

    * _handler(req) {
        let request = new Request(req);
        
        yield * request.make();
        let route = use('Route').dispatch(request);

        if (_.keys(route).length > 0) {
            return yield * this
                ._prepareResponse(
                    request,
                    yield * this._callRouteAction(route, request)
                );
        }

        throw new RouteNotFoundException(request);
    }

    * _prepareResponse(request, response) {
        if (!(response instanceof Response)) {
            response = new Response(response);
            yield * response.make();
        }

        return yield * response.prepare(request);
    }

    _renderFile(path, response) {
        let mime = type.getType(path);
        let readStream = null;
        let headers = {
            'Content-Type': mime
        };

        if (this._config.response.cache.enable) {
            let age = parseInt(
                (
                    relativeToDate(
                        this._config.response.cache.age ?
                            `${ this._config.response.cache.age } from now` :
                            '30 day from now'
                    ).getTime() -
                    relativeToDate('now').getTime()
                ) / 1000
            );

            headers['Cache-Control'] = `public, max-age=${ age }, must-revalidate`;
        }

        if (
            this._config.response.contentEncoding &&
            fs.existsSync(`${ path }.gz`)
        ) {
            let stat = fs.statSync(`${ path }.gz`);
            readStream = fs.createReadStream(`${ path }.gz`);
            headers['Content-Encoding'] = 'gzip';
        } else {
            let stat = fs.statSync(path);
            readStream = fs.createReadStream(path);
            headers['Content-Length'] = stat.size;
        }


        response
            .writeHead(
                200,
                headers
            );
        readStream.pipe(response);
    }

    _executeChain(generator, yieldValue) {
        if (generator instanceof Function) {
            generator = generator();
        }

        let next = generator.next(yieldValue);

        if (!next.done) {
            if (
                next.value.constructor
            ) {
                switch (true) {
                    case next.value.constructor.name === 'Promise':
                        next.value.then(
                            result => {
                                if (result instanceof Collection) {
                                    result.run().then(result => {
                                        this._executeChain(generator, result);
                                    });
                                } else {
                                    this._executeChain(generator, result);
                                }
                            },
                            err => {
                                generator.throw(err);
                            }
                        );
                        break;
                    case
                    next.value.constructor[Symbol.toStringTag] &&
                    next.value.constructor[Symbol.toStringTag] === 'GeneratorFunction':

                        break;
					case next.value instanceof Collection:
						next.value.run().then(
							result => {
								this._executeChain(generator, result)
							},
							err => {
								generator.throw(err);
							}
						);
						break;
                }
            }
        } else {
            this._chainResolve(next.value);
        }
    }

    * _callRouteAction (route, request) {
        if (!route.action.uses) {
            throw new RouteNotFoundException(request);
        }

        let action = yield * this._makeRouteAction(route);
        let chain = this._middleware.resolve(route.middleware)

        return yield new Promise((resolve, reject) => {
            this._chainResolve = resolve;

            try {
                this._executeChain(this._middleware.compose(chain, request, action));
            } catch (error) {
                reject(error);
            }
        });
    }

    * _makeRouteAction (route) {
        if (_.isFunction(route.action.uses)) {
            return this._makeClosureAction(route);
        } else if (_.isString(route.action.uses)) {
            return yield * this._makeControllerAction(route);
        }

        throw new InvalidArgumentException('Invalid route handler, attach a controllers method or a closure');
    }

    _makeClosureAction (route) {
        log.info('responding to route using closure');
        let parameters = this._makeActionParameters(route, route.action.uses.parameters);

        return route.action.uses.bind.apply(route.action.uses, [null].concat(parameters));
    }

    * _makeControllerAction (route) {
    	let namespace = application.makeNameSpace(this._controllersPath, route.action.uses);
        let result = application.makeFunction(namespace);
        let closure = result.instance[result.method].bind(result.instance);
        let reflection = yield ReflectionCollection.instance.find(namespace.split('.').first());
		let parameters = this._makeActionParameters(route, reflection.getMethod(result.method).parameters);

        return closure.bind.apply(closure, [null].concat(parameters));
    }

    _makeActionParameters (route, parameters) {
        return parameters.map(parameter => {
			switch (parameter.type.names.first()) {
				case 'Apricot/Request':
					return route.request;
				case 'number':
					return Number.parseNumber(route.parameters[parameter.name]);
				case 'string':
					return route.parameters[parameter.name].toString();
				default:
					return '';
			}
		});
    }
}

module.exports = Server;