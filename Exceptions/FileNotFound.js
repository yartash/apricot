class FileNotFound extends Error {
    constructor(path) {
        super(`File does not exist at path ${ path }`);
    }
}

module.exports = FileNotFound;