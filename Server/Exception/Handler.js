const Response = use('Apricot/Http/Response');

const RouteNotFoundException = use('Apricot/Exceptions/RouteNotFound');

class Handler {
    constructor(error) {
        this._error = error;
    }

    * render() {
        let response = new Response('');

        if (this._error instanceof RouteNotFoundException) {
            response.setStatusCode(404);
        } else {
            response.setStatusCode(500);
        }

        return response;
    }

    * execute() {
        return yield * this.render();
    }
}

module.exports = Handler;