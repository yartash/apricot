const InvalidArgumentException = use('Apricot/Exceptions/InvalidArgument')

class Seeder {
    /**
     * Set the console command instance.
     *
     * @public
     * @param {Apricot/Console/Command} command
     */
    set command (command) {
        this._command = command;
    }

    /**
     * Run the database seeds.
     *
     * @throws {Apricot/Exceptions/InvalidArgument}
     */
    * invoke () {
        if (! 'run' in this) {
            throw new InvalidArgumentException(`Method [run] missing from ${ getClass(this) }`);
        }

        yield * this.run();
    }
}

module.exports = Seeder;