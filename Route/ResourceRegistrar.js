const _ = require('lodash');

class ResourceRegistrar {
    get defaults() {
        return ['index', 'create', 'store', 'show', 'edit', 'update', 'destroy'];
    }

    constructor(router) {
        this._router = router;
        this._parameters = null;
    }

    register(name, controller, options = {}) {
        if (
            options['parameters'] !== undefined &&
            this._parameters === undefined
        ) {
            this._parameters = options['parameters'];
        }

        if (name.indexOf('/') > -1) {
            this._prefixedResource(name, controller, options);
        } else {
            _
                .each(
                    this._getResourceMethods(this.defaults, options),
                    method => {
                        this[`_addResource${_.upperFirst(method)}`].call(this, name, controller, options);
                    }
                )
        }
    }

    _prefixedResource(name, controller, options) {
        let data = this._getResourcePrefix(name);

        this
            ._router
            .group(
                {
                    prefix: data.prefix
                },
                router => {
                    router.resource(data.name, controller, options)
                }
            )
    }

    _getResourcePrefix(name) {
        let segments = name.split('/');
        let prefix = segments.slice(0, -1).join('/');

        return {
            name: _.last(segments),
            prefix: prefix
        }
    }

    _getResourceMethods(defaults, options) {
        if (options['only'] !== undefined) {
            return _
                .intersection(
                    defaults,
                    options['only']
                );
        } else if (options['except'] !== undefined) {
            return _
                .difference(
                    defaults,
                    options['except']
                );
        }

        return defaults;
    }

    _addResourceIndex(name, controller, options) {
        return this
            ._router
            .get(
                name,
                this._getResourceAction(name, controller, 'index', options)
            );
    }

    _addResourceCreate(name, controller, options) {
        return this
            ._router
            .get(
                `${name}/create`,
                this._getResourceAction(name, controller, 'create', options)
            );
    }

    _addResourceStore(name, controller, options) {
        return this
            ._router
            .post(
                name,
                this._getResourceAction(name, controller, 'store', options)
            );
    }

    _addResourceShow(name, controller, options) {
        return this
            ._router
            .get(
                `${name}/{${name}}`,
                this._getResourceAction(name, controller, 'show', options)
            );
    }

    _addResourceEdit(name, controller, options) {
        return this
            ._router
            .get(
                `${name}/{${name}}/edit`,
                this._getResourceAction(name, controller, 'edit', options)
            );
    }

    _addResourceUpdate(name, controller, options) {
        return this
            ._router
            .match(
                ['PUT', 'PATCH'],
                `${name}/{${name}}`,
                this._getResourceAction(name, controller, 'update', options)
            );
    }

    _addResourceDestroy(name, controller, options) {
        return this
            ._router
            .delete(
                `${name}/{${name}}`,
                this._getResourceAction(name, controller, 'destroy', options)
            );
    }

    _getResourceAction(resource, controller, method, options = {}) {
        let name = this._getResourceName(resource, method, options);

        return {
            'as': name,
            'uses': controller + '.' + method
        };
    }

    _getResourceName(resource, method, options = {}) {
        if (options['names'][method] !== undefined) {
            return options['names'][method];
        }

        let prefix = options['as'] !== undefined ? options['as'] + '.' : '';

        if (!this._router.hasGroupStack) {
            return prefix + resource + '.' + method;
        }

        return this._getGroupResourceName(prefix, resource, method);
    }

    _getGroupResourceName(prefix, resource, method) {
        let group = _.trim(this._router.lastGroupPrefix.replace('/', '.'), '.');

        if (group == '') {
            return _.trim(`{${ prefix }}{${ resource }}.{${ method }}`, '.');
        }

        return _.trim(`{${ prefix }}{${ group }}.{${ resource }}.{${ method }}`, '.');
    }
}

module.exports = ResourceRegistrar;