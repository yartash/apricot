const Command = use('Apricot/Console/Commands/Migrate');
const Foreground = use('Apricot/Console/Constants/Colors/Foreground');

const Migrator = use('Apricot/Console/Commands/Migrate/Migrator');

class Status extends Command {
	/**
	 * The console command name.
	 *
	 * @public
	 * @return {String}
	 */
	get signature () {
		return 'migrate:status';
	}

	get name () {
		return 'Migrate Status';
	}

	/**
	 * The console command description.
	 *
	 * @return {String}
	 */
	get description () {
		return 'Show the status of each migration.';
	}

	get optionsDefinition () {
		return {
			database: {
				description: 'The database connection to use.',
				short: 'd',
			},
			path: {
				description: 'The path of migrations files to be executed.',
				short: 'p'
			},
			realpath: {
				description: 'Indicate any provided migration file paths are pre-resolved absolute paths.',
				short: 'r'
			}
		};
	}

    constructor () {
        super();

        this._migrator = Migrator.instance;
    }

	* hundler () {
		this._migrator.connection = this.option('database');

        let exists = yield * this._migrator.repositoryExists();

        if (!exists) {
            return this.comment('Migration table not found.');
        }

        let ran = yield * this._migrator.repository.getRan();
        let batches = yield * this._migrator.repository.getMigrationBatches();
        let migrations = yield * this._getStatusFor(ran, batches);

        if (Object.keys(migrations).length) {
            this.table(['Ran?', 'Migration', 'Batch'], migrations);
        } else {
            this.error('No migrations found');
        }
	}

    /**
     * Get the status for the given ran migrations.
     *
     * @protected
     * @param array  $ran
     * @param array  $batches
     * @return {Array<Object>}
     */
    * _getStatusFor (ran, batches) {
        const file = use('File');
        let files = yield * this._getAllMigrationFiles();
        
        return yield collect(files)
            .map(migration => {
                let migrationName = file.basename(migration).replace('.js');

                return ran.indexOf(migrationName) > -1 ?
                    [
                        {style: 'info', message: 'Y'},
                        migrationName,
                        batches[migrationName]
                    ] :
                    [
                        {fg: Foreground.red, message: 'N'},
                        migrationName
                    ]
            })
            .all();
    }

    /**
     * Get an array of all of the migration files.
     *
     * @protected
     * @return {Array<Any>}
     */
    * _getAllMigrationFiles() {
        return yield this._migrator.getMigrationFiles(this.migrationPaths);
    }
}

module.exports = Status;