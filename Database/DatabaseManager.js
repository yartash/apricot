const InvalidArgumentException = use('Apricot/Exceptions/InvalidArgument');

class DatabaseManager {
    get defaultConnection() {
        return use('Config').get('database.default');
    }

    /**
     * @public
     * @param {string} name
     */
    set defaultConnection(name) {
        use('Config').set('database.default', name);
    }

    get proxy() {
        return {
            get: (target, name) => {
                if (name === 'prototype') {
                    return (new target).__proto__;
                } else {
                    return (...args) => {
                        return target[name].apply(target, args);
                    };
                }
            }
        }
    }

    constructor(factory) {
        this._factory = factory;
        this._connections = {};
        this._extensions = {};

        return new Proxy(
            this,
            this.proxy
        );
    }

    connection(name = '') {
        name = name !== '' ? name : this.defaultConnection;

        if (!this._connections[name]) {
            this._connections[name] = this._configure(this._makeConnection(name));
        }

        return this._connections[name];
    }

    extend(name, resolver) {
        this._extensions[name] = resolver;
    }

    reconnect(name = '') {
        name = name !== '' ? name : this.defaultConnection;
        this.disconnect(name);

        if (!this._connections[name]) {
            return this.connection(name);
        }

        return this._refreshConnection(name);
    }

    disconnect(name = '') {
        name = name !== '' ? name : this.defaultConnection;

        if (this._connections[name]) {
            this._connections[name].disconnect();
        }
    }

    _refreshConnection(name) {
        this._connections[name] = this._configure(this._makeConnection(name));

        return this._connections[name];
    }

    _configure(connection) {
        connection.reconnector = (connection) => {
            return this.reconnect(connection.name);
        }

        return connection;
    }

    _makeConnection(name) {
        let _config = config(`database.connections.${ name }`, false);

        if (_config === false) {
            throw new InvalidArgumentException(`Database ${ name } not configured.`)
        }

        if (this._extensions[name]) {
            return this._extensions[name].call(null, _config, name);
        }

        let driver = _config.driver;

        if (this._extensions[driver]) {
            return this._extensions[driver].call(null, _config, name);
        }

        _config.loggingQueries = config(`database.loggingQueries`);

        return this._factory.make(_config, name);
    }
}

module.exports = DatabaseManager;