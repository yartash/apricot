const _ = require('lodash');

const NotRegisteredMiddlewareException = use('Apricot/Middleware/Exceptions/NotRegisteredMiddleware');

class Middleware {
    constructor() {
        this._middleware = use('App/Http/Kernel');
    }

    resolve(routeMiddleware) {
        let middleware = this._formatMiddleware(routeMiddleware);

        return _
            .map(
                _.keys(middleware),
                item => {
                    let closure = application.makeFunction(`${ item }.handler`);
                    closure.parameters = middleware[item] || [];

                    return closure;
                }
            );
    }

    compose(chain, request, action) {
        chain = chain.reverse();
        let next = action;
        let index = 0;

        while (index < chain.length) {
            let closure = chain[index].instance[chain[index].method];

            if (index == chain.length - 1) {
                chain[index]
                    .parameters
                    .push(request);
            }

            chain[index].instance['next'] = next;
            next = chain[index].parameters.length ?
                closure
                    .bind
                    .apply(
                        closure,
                        [chain[index].instance]
                            .concat(
                                chain[index].parameters
                            )
                    ):
                closure
                    .bind(chain[index].instance);

            index++;
        }

        return next;
    }

    _formatMiddleware(middleware) {
        let result = {};

        _
            .each(
                this._middleware.globalMiddleware,
                namespace => {
                    result[namespace] = [];
                }
            );
        _
            .each(
                middleware,
                item => {
                    let options = item.split(':');
                    let namespace;

                    if (namespace = this._middleware.routeMiddleware[options[0]]) {
                        result[namespace] = this._fetchParams(options[1]);
                    } else if (namespace = this._middleware.middlewareGroups[options[0]]) {
                        _
                            .each(
                                namespace,
                                item => {
                                    result[item] = this._fetchParams(options[1])
                                }
                            );
                    }

                    if (!namespace) {
                        throw new NotRegisteredMiddlewareException(options[0]);
                    }

                }
            );

        return result;
    }

    _fetchParams(params) {
        return params ? params.split(',') : [];
    }
}

module.exports = Middleware;
