const fs = require('fs');
const path = require('path');

const _ = require('lodash');
const dateFormat = require('dateformat');

const Stream = use('Stream');

class ConsoleStream extends Stream {
    static get templates() {
        return ConsoleStream._templates;
    }

    get defaultColor() {
        return '\x1b[39m';
    }

    get defaultOption() {
        return {
            color: {
                debug: '\x1b[32m',
                info: '\x1b[36m',
                warning: '\x1b[35m',
                error: '\x1b[33m',
                fatal: '\x1b[31m'
            }
        }
    }

    get templatePath() {
        return path.join(super.templatePath, 'Console');
    }

    get template() {
        if (!ConsoleStream.templates[this.level]) {
            ConsoleStream.templates[this.level] = fs.readFileSync(
                path.join(this.templatePath, `${this.level.ucfirst()}.tpl`),
                'utf8'
            )
                .toString();
        }

        return ConsoleStream.templates[this.level];
    }

    constructor(args, level, option) {
        super(level);

        if (!this._validateLevel(level)) {
            return;
        }

        this._parseOption(option);
        this[`_compile${ level.ucfirst() }`](args);
    }    

    _compileDebug(args) {
        let stack = this.stack;
        
        console.log(_.template(this.template)({
            color: this.option.color.debug,
            defaultColor: this.defaultColor,
            date: dateFormat(new Date(), 'UTC:dd/mm/yyyy:HH:MM:ss o'),
            method: stack.method,
            file: stack.file,
            line: stack.line,
            position: stack.position,
            list: args.map(item => {
                return JSON.stringify(item);
            })
        }));
    }

    _compileInfo(message) {        
        console.log(_.template(this.template)({
            color: this.option.color.info,
            defaultColor: this.defaultColor,
            date: dateFormat(new Date(), 'UTC:dd/mm/yyyy:HH:MM:ss o'),
            message: message
        }));
    }
}

ConsoleStream._templates = {};

module.exports = ConsoleStream;