const _ = require('lodash');

class HeaderBag {
    get count() {
        return Object.keys(this._headers).length;
    }

    constructor(headers = {}) {
        this._headers = {};
        this._cacheControl = {};

        _
            .each(
                headers,
                (values, key) => {
                    this.set(key, values);
                }
            );
    }

    toString() {
        if (!Object.keys(this._headers).length) {
            return '';
        }
        let content = '';
        let sortKeys = _
            .sortBy(
                _.keys(this._headers),
                key => {
                    return key;
                }
            );

        this._headers = _
            .map(
                sortKeys,
                key => {
                    return this._headers[key];
                }
            );

        _.each(
            this._headers,
            (values, name) => {
                name = _
                    .map(
                        name.split('-'),
                        part => {
                            return _.upperFirst(part);
                        }
                    )
                    .join('-');

                _
                    .each(
                        values,
                        value => {
                            content += `${ name }: ${ value }\r\n`;
                        }
                    );
            }
        );

        return content;
    }

    all() {
        return this._headers;
    }

    keys() {
        return Object.keys(this._headers);
    }

    replace(headers) {
        this._headers = {};
        this.add(headers);
    }

    add(headers) {
        _
            .each(
                headers,
                (values, key) => {
                    this.set(key, values);
                }
            );
    }

    get(key, defaultValue = null, first = true) {
        key = key
            .toLowerCase()
            .replace(/_/g, '-');

        if (!this._headers[key]) {
            if (null === defaultValue) {
                return first ? null : [];
            }

            return first ? defaultValue : [defaultValue];
        }

        if (first) {
            return this._headers[key].length ?
                this._headers[key][0] :
                defaultValue;
        }

        return this._headers[key];
    }

    set(key, values, replace = true) {
        key = key
            .toLowerCase()
            .replace(/_/g, '-');

        values = _.isObject(values) ?
            _.values(values) :
            values;

        if (true === replace || !this._headers[key]) {
            this._headers[key] = values;
        } else {
            this._headers[key] = this._headers.concat(values);
        }

        if ('cache-control' === key) {
            this._cacheControl = this._parseCacheControl(values[0]);
        }
    }

    has(key) {
        return this
            ._headers
            .hasOwnProperty(
                key
                    .toLowerCase()
                    .replace(/_/g, '-')
            );
    }

    contains(key, value) {
        return this.get(key, null, false).indexOf(value) > -1;
    }

    remove(key) {
        key = key
            .toLowerCase()
            .replace(/_/g, '-');

        delete this._headers[key];

        if ('cache-control' === key) {
            this._cacheControl = {};
        }
    }

    addCacheControlDirective(key, value = true) {
        this._cacheControl[key] = value;
        this.set('Cache-Control', this._getCacheControlHeader());
    }

    hasCacheControlDirective(key) {
        return !!this._cacheControl[key];
    }

    getCacheControlDirective(key) {
        return this._cacheControl[key] ?
            this._cacheControl[key] :
            null;
    }

    removeCacheControlDirective(key) {
        delete this._cacheControl[key];

        this.set('Cache-Control', this._getCacheControlHeader());
    }

    _parseCacheControl(header) {
        let cacheControl = {};
        let reg = /([a-zA-Z][a-zA-Z_-]*)\s*(?:=(?:"([^"]*)"|([^ \t",;]*)))?/g;
        let match;

        while ((match = reg.exec(header)) !== null) {
            cacheControl[match[1].toLowerCase()] = match[3] ?
                match[3] :
                (
                    match[2] ?
                        match[2] :
                        true
                );
        }

        return cacheControl;
    }

    _getCacheControlHeader() {
        let patrs = [];

        let sortKeys = _
            .sortBy(
                _.keys(this._cacheControl),
                key => {
                    return key;
                }
            );

        this._cacheControl = _
            .map(
                sortKeys,
                key => {
                    return this._cacheControl[key];
                }
            );

        return _
            .map(
                this._cacheControl,
                (value, key) => {
                    if (value === true) {
                        patrs.push(key);
                    } else {
                        if (/[^a-zA-Z0-9._-]/.test(value)) {
                            value = `"${ value }"`;
                        }

                        patrs.push(`${ key }=${ value }`);
                    }
                }
            )
            .join(', ');
    }
}

module.exports = HeaderBag;