class SignatureValidator {
    match(command, signature) {
        return command.regex.test(signature);
    }
}

module.exports = SignatureValidator;