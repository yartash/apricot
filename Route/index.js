const _ = require('lodash');

const MethodValidator = use('Apricot/Route/Validators/Method');
const SchemeValidator = use('Apricot/Route/Validators/Scheme');
const HostValidator = use('Apricot/Route/Validators/Host');
const UrlValidator = use('Apricot/Route/Validators/Url');

const NoRouteActionException = use('Exceptions/NoRouteAction');
const InvalidRouteActionException = use('Apricot/Route/Exceptions/InvalidRouteAction');

class Route {
    set router(value) {
        this._router = value;
    }

    set action(value) {
        this._action = value;
    }

    set url(value) {
        this._url = value;
    }

    set middleware(value) {
        if (value === null) {
            return this.action['middleware'] !== undefined ?
                this.action['middleware'] :
                [];
        }

        let middlewareArray = [];

        if (_.isString(value)) {
            middlewareArray = [value];
        }

        this.action['middleware'] = (
            this.action['middleware'] !== undefined ?
                this.action['middleware'] :
                []
        )
            .concat(middlewareArray);
    }

    get action() {
        return this._action;
    }

    get middleware() {
        return this._action.middleware !== undefined ?
            this._action.middleware :
            [];
    }

    get domain() {
        return this._action.domain !== undefined ?
            this._action.domain :
            '';
    }

    get methods() {
        return this._methods;
    }

    get url() {
        return this._url;
    }

    get httpOnly() {
        let action = _.values(this.action);
        let index = action.indexOf('http');

        return index > -1 && action[index] === 'http';
    }

    get secure() {
        let action = _.values(this.action);
        let index = action.indexOf('https');

        return index > -1 && action[index] === 'https';
    }

    get regex() {
        let parts = this
            .url
            .match(
                new RegExp(
                    '([\\{]?[\\w|\\?]+[\\}]?)',
                    'g'
                )
            );

        return new RegExp(
            `^${
                parts != null ?
                    _
                        .map(
                            parts,
                            (part, index) => {
                                if (part.indexOf('{') > -1) {
                                    let where = part.replace(new RegExp('\\{|\\}|\\?', 'g'), '');
                                    
                                    if (this._wheres[where] === undefined) {
                                        if (index == parts.length - 1 && part.indexOf('{') > -1) {
                                            return '(?:\/([^\/]+))?';
                                        } else {
                                            return '\/([^\/]+)';
                                        }
                                    } else {
                                        if (index == parts.length - 1 && part.indexOf('{') > -1) {
                                            return `(?:\/(${ this._wheres[where] }))?`;
                                        } else {
                                            return `\/(${ this._wheres[where] })`;
                                        }
                                    }
                                } else {
                                    return `\/${ part }`;
                                }
                            }
                        )
                        .join('') :
                    this.url
            }$`
        );
    }

    get parameterNames() {
        if (this._parameterNames === undefined) {
            this._parameterNames = this._compileParameterNames();
        }

        return this._parameterNames;
    }

    get prefix() {
        return this.action['prefix'] !== undefined ?
            this.action['prefix'] :
            null;
    }

    get name() {
        return this.action['as'] !== undefined ?
            this.action['as'] :
            null;
    }

    get actionName() {
        return this.action['controller'] !== undefined ?
            this.action['controller'] :
            'Function';
    }

    get parameters() {
        return this._parameters;
    }

	/**
     * @public
	 * @return {Apricot/Request}
	 */
	get request() {
        return this._request;
    }

    static get validators() {
        return [
            new MethodValidator(),
            new UrlValidator(),
            new HostValidator(),
            new SchemeValidator()
        ];
    }

    constructor(methods, url, action) {
        this._router = null;
        this._action = this._parseAction(action);
        this._wheres = {};
        this._url = url;
        this._methods = _.isString(methods) ?
            [methods] :
            (
                _.isArray(methods) ?
                    methods :
                    []
            );
        this._parameterNames = [];
        this._parameters = {};

        if (
            this._methods.indexOf('GET') > -1 &&
            this._methods.indexOf('HEAD') == -1
        ) {
            this
                ._methods
                .push('HEAD');
        }

        if (this._action['prefix'] !== undefined) {
            this._prefix(this._action['prefix']);
        }
    }

    matches(request) {
        for (var index in Route.validators) {
            if (!Route.validators[index].match(this, request)) {
                return false;
            }
        }

        return true;
    }

    bind(request) {
        this._request = request;
        this._parameters = this._bindPathParameters(request);

        return this;
    }

    name(name) {
        this.action['as'] = this.action['as'] !== undefined ?
            this.action['as'] + name :
            name;

        return this;
    }

    uses(action) {
        return this.action = _.merge(this.action, this._parseAction(action));
    }

    where(name, expression = null)  {
        let wheres = this._parseWhere(name, expression);

        for (var i in wheres) {
            this._wheres[i] = wheres[i];
        }

        return this;
    }

    _parseAction(action = false) {
        if (!action) {
            return {
                uses: () => {
                    throw new NoRouteActionException(this._url);
                }
            };
        }

        if (_.isFunction(action)) {
            return {
                uses: action
            };
        } else if (_.isObject(action)) {
            if (action['uses'] === undefined) {
                action['uses'] = this._findCallable(action);
            }
        }

        if (_.isString(action['uses']) && action['uses'].indexOf('.') == -1) {
            throw new InvalidRouteActionException(action['uses']);
        }

        return action;
    }

    _findCallable(action) {
        return action[
                _.findKey(action, item => {
                    return _.isFunction(item);
                })
            ];
    }

    _prefix(prefix){
        this._url = _
            .trim(
                `${ _.trimEnd(prefix, '/') }/${ _.trimStart(this._url, '/') }`,
                '/'
            );

        return this;
    }

    _bindPathParameters(request) {
        let matches =  (`/${ _.trimStart(decodeURIComponent(request.url), '/') }`)
            .match(this.regex)
            .slice(1);
        let parameters = {};

        _
            .each(
                this
                    .url
                    .match(
                        new RegExp(
                            '([\\{]?[\\w|\\?]+[\\}]?)',
                            'g'
                        )
                    ),
                part => {
                    if (part.indexOf('{') > -1) {
                        parameters[part.replace(new RegExp('\\{|\\}|\\?', 'g'), '')] = matches.shift();
                    }
                }
            );

        return parameters;
    }

    _compileParameterNames() {
        return _
            .map(
                `${ this.domain }${ this.url }`
                    .match(
                        new RegExp('\\{(.*?)\\}', 'g')
                    ),
                match => {
                    return _
                        .trim(
                            match
                                .replace(
                                    new RegExp('[\\{|\\}]', 'g'),
                                    ''
                                ),
                            '?'
                        )
                }
            );
    }

    _parseWhere(name, expression) {
        if (_.isObject(name)) {
            return name;
        }

        let obj = {};
        obj[name.toString()] = expression;

        return obj;
    }
}

module.exports = Route;