const GeneratorHandler = use('Apricot/Support/GeneratorHandler');

class ManagesTransactions {
	/**
	 * Execute a Closure within a transaction.
	 *
	 * @public
	 * @param {Promise} callback
	 * @param {Int} attempts
	 * @return Any
	 *
	 * @throws \Exception|\Throwable
	 */
	transaction(callback, attempts = 1) {
		return new Promise(resolve => {
			let currentAttempt = 1;

			this.beginTransaction()
				.then(() => {
					return this._transactioCallbackExecutor(callback);
				})
				.then(() => {
					return this.commit();
				}, error => {
					return this._handleTransactionException(error, currentAttempt, attempts);
				})
                .then(() => {
                	resolve();
				});
			// for (let currentAttempt = 1; currentAttempt <= attempts; currentAttempt++)
		});
	}

	/**
	 * Start a new database transaction.
	 *
	 * @public
	 * @throws \Exception
	 */
	beginTransaction ()	{
		return new Promise(resolve => {
			this._createTransaction()
				.then(result => {
					this._transactions++;
					//todo add
					// $this->fireConnectionEvent('beganTransaction');
					resolve();
				});
		});
	}

    /**
     * Rollback the active database transaction.
     *
     * @public
     * @param {Int|Null} toLevel
     */
    rollBack (toLevel = null) {
        return new Promise(resolve => {
            // We allow developers to rollback to a certain transaction level. We will verify
            // that this given transaction level is valid before attempting to rollback to
            // that level. If it's not we will just return out and not attempt anything.
            toLevel = toLevel === null ?
                this._transactions - 1 :
                toLevel;

            if (toLevel < 0 || toLevel >= this._transactions) {
                resolve();
            }

            // Next, we will actually perform this rollback within this database and fire the
            // rollback event. We will also set the current transaction level to the given
            // level that was passed into this method so it will be right from here out.
            this._performRollBack(toLevel)
                .then(() => {
                    this._transactions = toLevel;

                    //todo
                    // $this->fireConnectionEvent('rollingBack');
                    resolve();
                });
        });
    }

	/**
	 * Handler an exception from a transaction beginning.
	 *
	 * @protected
	 * @param  {Error} error
	 *
	 * @throws \Exception
	 */
	_handleBeginTransactionException (error) {
		return new Promise(resolve => {
			if (this._causedByLostConnection(error)) {
				let connection = this.reconnect();

				this.client = connection.client;

				this.client
					.then(client => {
						return client.query('BEGIN');
					})
					.then(result => {
						resolve();
					}, error => {
						Promise.reject(error);
					});
			} else {
				Promise.reject(error);
			}
		});
	}

	/**
	 * Create a save point within the database.
	 *
	 * @protected
	 */
	_createSavepoint () {
		this.clinet
			.then(client => {
				return client.query(this.queryGrammar.compileSavepoint(`trans${ this._transactions + 1 }`))
			});
	}

	/**
	 *
	 *
	 * @protected
	 * @param {Function|Generator|Promise} callback
	 * @return {Promise<Any>}
	 */
	_transactioCallbackExecutor (callback) {
		return new Promise((resolve, reject) => {
			if (Function.isGenerator(callback)) {
				(new GeneratorHandler(resolve, reject)).executor(callback());
			} else {
			    try {
                    let result = callback(this);
                } catch (error) {
                    reject(error);
                }

				if (Function.isPromise(result)) {
					result.then(
						data => {
						    resolve(data);
					    }, error => {
						    reject(error);
                        }
					)
				} else {
					resolve(result);
				}
			}
		});
	}

    /**
     * Handler an exception encountered when running a transacted statement.
     *
     * @protected
     * @param {Exception} error
     * @param {Int} currentAttempt
     * @param {Int} maxAttempts
     *
     * @throws {Exception}
     */
    _handleTransactionException (error, currentAttempt, maxAttempts) {
        return new Promise((resolve, reject) => {
            // On a deadlock, MySQL rolls back the entire transaction so we can't just
            // retry the query. We have to throw this exception all the way out and
            // let the developer handler it in another way. We will decrement too.
            if (this._causedByDeadlock(error) && this._transactions > 1) {
                this._transactions--;

				reject(error);
            }

            this.rollBack()
                .then(() => {
                    if (this._causedByDeadlock(error) && currentAttempt < maxAttempts) {
                        resolve();
                    }

                    reject(error);
                })
        });
    }
}

module.exports = ManagesTransactions;