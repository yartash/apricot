const fs = require('fs');
const path = require('path');

const _ = require('lodash');
const dateFormat = require('dateformat');

const Stream = use('Stream');

class FileStream extends Stream {
    static get templates() {
        return FileStream._templates;
    }

    get defaultOption() {
        return {
            permission: 666,
            encoding: 'utf8'
        }
    }
    
    get path() {
        return this._path;
    }
    
    set path(value) {
        this._path = value;
    }

    get templatePath() {
        return path.join(super.templatePath, 'File');
    }

    get template() {
        if (!FileStream.templates[this.level]) {
            FileStream.templates[this.level] = fs.readFileSync(
                path.join(this.templatePath, `${this.level.ucfirst()}.tpl`),
                'utf8'
            )
                .toString();
        }

        return FileStream.templates[this.level];
    }

    constructor(args, level, option) {
        super(level);

        if (!this._validateLevel(level)) {
            return;
        }

        this._parseOption(option);
        this._createPath();
        this[`_compile${ level.ucfirst() }`](args);
    }

    _createPath() {
        this.path = path.join(this.option.path, dateFormat(new Date(), 'UTC:yyyy-mm-dd'));
        
        if (!fs.existsSync(this.path)) {
            fs.mkdirSync(this.path, this.option.permission)
        }
    }

    _compileDebug(args) {
        let stack = this.stack;

        this._appendFile(
            _.template(this.template)({
                date: dateFormat(new Date(), 'UTC:dd/mm/yyyy:HH:MM:ss o'),
                method: stack.method,
                file: stack.file,
                line: stack.line,
                position: stack.position,
                list: args.map(item => {
                    return JSON.stringify(item);
                })
            })
        );
    }

    _compileInfo(message) {
        this._appendFile(
            _.template(this.template)({
                date: dateFormat(new Date(), 'UTC:dd/mm/yyyy:HH:MM:ss o'),
                message: message
            })
        );
    }

    _appendFile(row) {
        fs.open(
            path.join(this.path, `${ this.level }.log`),
            'a',
            this.option.permission,
            (error, id) => {
                fs.write(
                    id,
                    row + require('os').EOL,
                    null,
                    this.option.encoding,
                    () => {
                        fs.close(id, () => {});
                    }
                );
            }
        );
    }
}

FileStream._templates = {};

module.exports = FileStream;