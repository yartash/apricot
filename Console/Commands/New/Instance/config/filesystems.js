const Env = use('Env');

module.exports = {
    default: Env.get('FILESYSTEM_DRIVER', 'local'),
    disks: {
        local: {
            driver: 'local',
            root: storagePath('app')
        }
    }
};
