const GeneratorCommand = use('Apricot/Console/Commands/Make/GeneratorCommand')

class Command extends GeneratorCommand {
    get signature () {
        return 'make:command {name}';
    }

    get name () {
        return 'Make:command';
    }

    get description () {
        return 'Create a new command class';
    }

    /**
     * Get the stub file for the generator.
     *
     * @protected
     * @return string
     */
    get _templatePath () {
        return this._files.join(__dirname, 'Templates', 'blank.tmp')
    }

    constructor () {
        super(use('File'));

        /**
         * The type of class being generated.
         *
         * @protected
         * @property {String}
         */
        this._type = 'Command';
    }

    /**
     * Get the default namespace for the class.
     *
     * @protected
     * @param {String} rootNamespace
     * @return {String}
     */
    _getDefaultNamespace (rootNamespace) {
        return `${ rootNamespace }/Console/Commands`;
    }
}

module.exports = Command;
