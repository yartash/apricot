const Type = use('Type');

class Parameter {
	/**
	 * @public
	 * @return {string}
	 */
	get name () {
		return this._data.name;
	}

	/**
	 * @public
	 * @return {(boolean|undefined)}
	 */
	get nullable () {
		return this._data.nullable;
	}

	/**
	 * @public
	 * @return {(boolean|undefined)}
	 */
	get optional () {
		return this._data.optional;
	}

	/**
	 * @public
	 * @return {*}
	 */
	get defaultValue () {
		return this._data.defaultvalue;
	}

	/**
	 * @public
	 * @return {Array<Apricot/Reflection/Type>}
	 */
	get type () {
		return this._type;
	}
	
	constructor (data) {
		this._data = data;
		this._type = new Type();
		
		this._parse();
	}

	/**
	 * @private
	 */
	_parse () {
		if (this._data.type) {
			this._type = new Type(this._data.type);
		}
	}
}

module.exports = Parameter;