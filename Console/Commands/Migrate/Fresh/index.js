const Command = use('Apricot/Console/Commands/Migrate');

const Migrator = use('Apricot/Console/Commands/Migrate/Migrator');

class Fresh extends Command {
	/**
	 * The console command name.
	 *
	 * @public
	 * @return {String}
	 */
	get signature () {
		return 'migrate:fresh';
	}

	get name () {
		return 'Migrate Fresh';
	}

	/**
	 * The console command description.
	 *
	 * @return {String}
	 */
	get description () {
		return 'Drop all tables and re-run all migrations.';
	}

	get optionsDefinition () {
		return {
			database: {
				description: 'The database connection to use.',
				short: 'd',
			},
			force: {
				description: 'Force the operation to run when in production.',
				short: 'f'
			},
			path: {
				description: 'The path of migrations files to be executed.',
				short: 'p'
			},
			realpath: {
				description: 'Indicate any provided migration file paths are pre-resolved absolute paths.',
				short: 'r'
			},
			seed: {
				description: 'Indicates if the seed task should be re-run.',
				short: 's'
			},
            seeder: {
                description: 'The class name of the root seeder.',
                short: 't'
			}
		};
	}

	get traits () {
		return [
			'Confirmable'
		];
	}

    constructor () {
        super();

        this._migrator = Migrator.instance;
    }

	* hundler () {
		if (!(yield * this.confirmToProceed())) {
			return;
		}

        this._migrator.connection = this.option('database');

        yield * this._migrator.dropAllTables();

        this.info('Dropped all tables successfully.');

        yield * this.call(
            'migrate',
            [],
            {
                database: this.option('database'),
                path: this.option('path'),
                force: true,
            }
        );

        if (this.option('seed') || this.option('seeder')) {
            yield * this.call(
                'db:seed',
                [],
                {
                    database: this.option('database'),
                    class: this.option('seeder') ? this.option('seeder') : 'DatabaseSeeder',
                    force: this.option('force'),
                }
            );
        }
	}
}

module.exports = Fresh;