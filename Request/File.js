const path = require('path');
const fs = require('fs');

const InvalidPathExceptions = use('Apricot/Request/Exceptions/InvalidPath');

class File {
    get clientName() {
        return this._clientName;
    }

    get mimeType() {
        return this._type;
    }

    get extension() {
        return path.extname(this.clientName).replace('.', '');
    }

    get size() {
        return this._content.length;
    }

    get name() {
        return this._name;
    }

    constructor(name, clientName, mime, content) {
        this._name = name;
        this._clientName = clientName;
        this._type = mime;
        this._content = content;
    }

    save(filePath) {
        if (path.isAbsolute(filePath)) {
           return new InvalidPathExceptions();
        }

        let writeStream = fs.createWriteStream(
            filePath,
            {
                flags: 'w',
                defaultEncoding: 'utf8',
                fd: null,
                mode: 0o666,
                autoClose: true
            }
        );

        writeStream.write(this._content);
        writeStream.end();
    }
}

module.exports = File;