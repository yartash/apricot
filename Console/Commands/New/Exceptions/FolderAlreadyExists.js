class FolderAlreadyExists extends Error {
    get command () {
        return this._command;
    }
    
    constructor(name, command) {
        super(`A directory already exists at: ${name}`);
        
        this._command = command;
    }
}

module.exports = FolderAlreadyExists;