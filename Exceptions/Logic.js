class Logic extends Error {
    constructor(message) {
        super(message);
    }
}

module.exports = Logic;