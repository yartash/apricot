const BaseProvider = use('Apricot/Application/BaseProvider');

class PeachProvider extends BaseProvider {
    register() {
        this.app.singleton('peach', () => {
            const Peach = use('Apricot/Console/Peach');
            let peach = new Peach();

            this.registerCommand(peach);

            return peach;
        });
    }

    registerCommand (peach) {
        let kernel = this.app.make(this.app.isGlobal ? 'Apricot/Console/Kernel' : 'App/Console/Kernel');

        kernel.coreCommands
            .concat(kernel.commands)
            .forEach(command => {
                let CommandClass = use(command);

                peach.addCommand(new CommandClass());
            });
    }
}

module.exports = PeachProvider;