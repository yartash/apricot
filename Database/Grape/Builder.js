class Builder {    
    constructor(query) {
        this._query = query;
        this._scopes = {};
    }

    hydrate(items) {
        let instance = this.newModelInstance();
    }

    newModelInstance(args = []) {
        this._model.newInstance(args);
    }

    withGlobalScope(identifier, scope) {
        this._scopes[identifier] = scope;

        if (scope.extend) {
            scope.extend(this);
        }

        return this;
    }
    
    setModel(model) {
        this._model = model;
        this._query.from(model.from);

        return this;
    }
    
    setProxy(proxy) {
        this._proxy = proxy;

        return this;
    }
    
    with() {
        return this;
    }
    
    withCount() {
        return this;
    }

    get(columns = ['*']) {
        let builder = this.applyScopes();

        return builder.getModels(columns)
            .then(models => {
                let f = 0;
                return models;
            });
    }

    getModels(columns = ['*']) {
        return new Promise(resolve => {
            this._query
                .get(columns)
                .then(results => {
                    this._proxy
                        .hydrate(results);
                })
        });
        
        // return this._query.get(columns).all();
    }

    applyScopes() {
        if (Object.keys(this._scopes).length == 0) {
            return this;
        }
    }
}

module.exports = Builder;