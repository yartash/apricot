class MethodValidator {
    match(route, request) {
        return route.methods.indexOf(request.method) > -1;
    }
}

module.exports = MethodValidator;