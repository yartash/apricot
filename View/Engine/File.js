const fs = require('fs');

class File {
    * get(path) {
        return fs.readFileSync(path, 'utf8');
    }
}

module.exports = File;