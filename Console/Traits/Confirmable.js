class Confirmable {
	/**
	 * @protected
	 * @return {Function}
	 */
	get defaultConfirmCallback () {
		return () => {
			return use('Env').get('APP_ENV') === 'production';
		}
	}

	/**
	 *
	 * @param {string} warning
	 * @param {function} callback
	 * @return {boolean}
	 */
	* confirmToProceed (warning = 'Application In Production!', callback = null) {
		callback = callback === null ? this.defaultConfirmCallback : callback;

		let shouldConfirm = callback instanceof Function ? callback() : callback;

		if (shouldConfirm) {
			if (this.option('force')) {
				return true;
			}

			this.output.alert(warning);

			let confirmed = yield this.output.confirm('Do you really wish to run this command?');

			if (!confirmed) {
				this.info('Command Cancelled!');

				return false;
			}
		}

		return true;
	}
}

module.exports = Confirmable;