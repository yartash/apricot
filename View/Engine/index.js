const InvalidArgumentException = use('Apricot/Exceptions/InvalidArgument');

class Engine {
    constructor() {
        this._resolvers = {};
        this._resolved = {};
    }

    register(engine, closure) {
        delete this._resolved[engine];

        this._resolvers[engine] = closure;
    }

    resolve(engine) {
        if (this._resolved[engine]) {
            return this._resolved[engine];
        }

        if (this._resolvers[engine]) {
            return this._resolved[engine] = this._resolvers[engine]();
        }

        throw new InvalidArgumentException(`Engine ${ engine } not found.`);
    }
}

module.exports = Engine;