const dotEnv = require('dotenv');

class Env {
    constructor() {
        dotEnv.load({
            path: basePath('.env'),
            silent: false,
            encoding: 'utf8'
        });
    }

    get (key, defaultValue = null) {
        let value = process.env[key] || defaultValue;

        if (value === 'true') {
            return true;
        }

        if (value === 'false') {
            return false;
        }

        return value;
    }

    set (key, value) {
        process.env[key] = value;
    }
}

module.exports = Env;