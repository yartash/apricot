class Expression {
    get value() {
        return this._value;
    }

    constructor(value) {
        this._value = value;
    }
}

module.exports = Expression;