class Env {
    static get facadeAccessor() {
        return 'env';
    }
}

module.exports = Env;