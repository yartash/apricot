const _ = require('lodash');

const Response = use('Apricot/Response');

class Redirect extends Response {
    constructor(url) {
        super();

        this._url = `${ this._request.host }/${ _.trimStart(url, '/') }`
    }

    render() {
        super.render();

        this._response.writeHead(
            301,
            {
                Location: this._url
            }
        );
        this._response.end();
    }

    to(url) {
        this._url = url;
    }
}

module.exports = Redirect;