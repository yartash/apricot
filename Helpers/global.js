Object.defineProperty(
    global,
    'rawurlencode',
    {
        value: string => {
            string = string + '';

            return encodeURIComponent(string)
                .replace(/!/g, '%21')
                .replace(/'/g, '%27')
                .replace(/\(/g, '%28')
                .replace(/\)/g, '%29')
                .replace(/\*/g, '%2A')
        },
        configurable: false,
        writable: false,
        enumerable: false
    }
);

Object.defineProperty(
    global,
    'view',
    {
        value: (view = null, data = {}, mergeData = {}) => {
            let factory = use('View');

            if (view === null) {
                return factory;
            }

            return factory.make(view, data, mergeData);
        },
        configurable: false,
        writable: false,
        enumerable: false
    }
);

Object.defineProperty(
    global,
    'env',
    {
        value: (key, defaultValue = null) => {
            const Env = use('Env');

            return Env.get(key, defaultValue)
        },
        configurable: false,
        writable: false,
        enumerable: false
    }
);

Object.defineProperty(
    global,
    'config',
    {
        value: (key = null, defaultValue = null) => {
            if (key === null) {
                return use('config');
            }

            if (Object.isObject(key)) {
                return use('config').set(key);
            }

            return use('config').get(key, defaultValue);
        },
        configurable: false,
        writable: false,
        enumerable: false
    }
);

Object.defineProperty(
    global,
    'collect',
    {
        value: (value = null) => {
            const Collection = use('Apricot/Support/Collection');

            return new Collection(value);
        },
        configurable: false,
        writable: false,
        enumerable: false
    }
);

Object.defineProperty(
    global,
    'value',
    {
        value: (value) => {
            return Function.isFunction(value) ? value() : value;
        },
        configurable: false,
        writable: false,
        enumerable: false
    }
);

/**
 * Get an item from an array or object using "dot" notation.
 *
 * @param {Any} target
 * @param {String|Array<String>} key
 * @param {Any} default
 * @return {Any}
 */
Object.defineProperty(
    global,
    'dataGet',
    {
        value: (target, key, defaultValue = null) => {
            if (key === null) {
                return target;
            }

            const Collection = use('Apricot/Support/Collection');
            let cloneKey = key.slice();

            cloneKey = Array.isArray(cloneKey) ? cloneKey : cloneKey.split('.');

            while (cloneKey.length > 0) {
                let segment = cloneKey.shift();

                if (Array.isArray(target)) {
                    target = segment == '*' ? target : target[segment];
                } else if (Object.isObject(target)) {
                    target = segment == '*' ? Object.values(target) : target[segment];
                }
            }

            return target;
        },
        configurable: false,
        writable: false,
        enumerable: false
    }
)

/**
 * Returns the name of the class of an object.
 *
 * @param {Object}
 * @return {String}
 */
Object.defineProperty(
	global,
	'getClass',
	{
		value: (instance) => {
			return instance.constructor.name;
		},
		configurable: false,
		writable: false,
		enumerable: false
	}
);