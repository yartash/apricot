const Builder = use('Builder');

class MongoBuilder extends Builder {
    get fileds() {
        return this._fileds;
    }

    set fileds(value) {
        this._fileds = value;
    }

    constructor(connection) {
        super(connection);

        this._fileds = null;
    }

    get(fileds = ['*']) {
        let original = this._fileds;

        if (original === null) {
            this._fileds = fileds;
        }

        let results = this._runSelect();
        //
        // $this->columns = $original;
        //
        // return collect($results);
    }

    toMongo() {
        return this._grammar.compileSelect(this)
    }

    _runSelect() {
        return this._connection.select(
            this.toJson() //$this->getBindings(), ! $this->useWritePdo
        );
    }
}

module.exports = MongoBuilder;