class Tag {
	/**
	 * @public
	 * @return {string}
	 */
	get title () {
		return this._data.title;
	}

	/**
	 * @public
	 * @return {string}
	 */
	get value () {
		return this._data.value;
	}

	constructor (data) {
		this._data = data;
	}
}

module.exports = Tag;