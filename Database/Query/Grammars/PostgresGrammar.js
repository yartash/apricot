const Grammar = use('Grammar');

class PostgresGrammar extends Grammar {
    constructor () {
        super();

        /**
         * All of the available clause operators.
         *
         * @protected
         * @property {Array<String>}
         */
        this._operators = [
            '=', '<', '>', '<=', '>=', '<>', '!=',
            'like', 'not like', 'ilike', 'not ilike',
            '&', '|', '#', '<<', '>>', '>>=', '=<<',
            '&&', '@>', '<@', '?', '?|', '?&', '||', '-', '-', '#-',
            'is distinct from', 'is not distinct from',
        ];
    }

    /**
     * @public
     * @return {String}
     */
    compileInsert (query, values) {
        let table = this.wrapTable(query.from());

        if (!Array.isArray(values)) {
            values = [values];
        }

        return values.length ?
            this._replaceBindingSymbols(super.compileInsert(query, values)) :
            `insert into ${ table } default values`;
    }

	/**
	 * Compile a delete statement into SQL.
	 *
	 * @public
	 * @param {Apricot/Database/Query/Builder} query
	 * @return {String}
	 */
	compileDelete (query) {
		let table = this.wrapTable(query.getFrom());

		return query.getJoins().length ?
			this._compileDeleteWithJoins(query, table) :
			super.compileDelete(query);
	}

	/**
	 * Compile a delete query that uses joins.
	 *
	 * @protected
	 * @param {Apricot/Database/Query/Builder} query
	 * @param {String} table
	 * @return {String}
	 */
	_compileDeleteWithJoins (query, table) {
		let using = ` USING ${ query.getJoins().map(join => { return this.wrapTable(join.table); }).join(', ') }`;
		let where = query.getWheres().length ? ` ${ this._compileUpdateWheres(query) }` : '';

		return `delete from ${ table }${ using }${ where }`.trim();
	}

	/**
     * Compile the additional where clauses for updates with joins.
     *
     * @param {Apricot/Database/Query/Builder} query
     * @return {String}
     */
    _compileUpdateWheres(query) {
        let baseWheres = this._compileWheres(query);

        if (!query.getJoins().length) {
            return baseWheres;
        }

        // Once we compile the join constraints, we will either use them as the where
        // clause or append them to the existing base where clauses. If we need to
        // strip the leading boolean we will do so when using as the only where.
        let joinWheres = this._compileUpdateJoinWheres(query);

        if (baseWheres.trim() == '') {
            return `where ${ this._removeLeadingBoolean(joinWheres) }`;
        }

        return `${ baseWheres } ${ joinWheres }`;
    }

	/**
	 * Compile the "join" clause where clauses for an update.
	 *
	 * @param {Apricot/Database/Query/Builder} query
	 * @return {String}
	 */
	_compileUpdateJoinWheres (query) {
		let joinWheres = [];

		// Here we will just loop through all of the join constraints and compile them
		// all out then implode them. This should give us "where" like syntax after
		// everything has been built and then we will join it to the real wheres.
		query.getJoins().forEach(join => {
			join.getWheres().forEach(where => {
				let method = `where${ where.type }`;

				joinWheres.push(`${ where.boolean } ${ this[method](query, where) }`);
			});
		});

		return joinWheres.join(' ');
	}

    /**
     * Compile the "where" portions of the query.
     *
     * @protected
     * @param {Apricot/Database/Query/Builder} query
     * @return {String}
     */
    _compileWheres (query) {
        return this._replaceBindingSymbols(super._compileWheres(query));
    }

    _replaceBindingSymbols(query) {
        let index = 1;

        return query.replace(/\?/g, () => {
            return `$${ index++ }`;
        });
    }

    _wrapValue(value) {
        if (value === '*') {
            return value;
        }

        if (value.indexOf('->') > -1) {
            return this._wrapJsonSelector(value);
        }

        return `"${ value.replace('"', '""') }"`;
    }

    _wrapJsonSelector(value) {
        let path = value.split('->');
        let field = this._wrapValue(path.shift());
        let wrappedPath = this._wrapJsonPathAttributes(path);
        let attribute = wrappedPath.pop();

        if (wrappedPath.length) {
            return `${ field }->${ wrappedPath.join('->') }->>${ attribute }`;
        }

        return `${ field }->>${ attribute }`;
    }

    _wrapJsonPathAttributes(path) {
        return path.map(attribute => {
            return `'${ attribute }'`
        });
    }
}

module.exports = PostgresGrammar;