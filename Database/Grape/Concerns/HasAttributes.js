class HasAttributes {
    get options() {
        return {
            attributes: {},
            original: {},
            change: {},
            casts: {},
            dates: [],
            dateFormat: '',
            appends: {}
        };
    }
}

module.exports = HasAttributes;