const Command = use('Apricot/Console/Commands/Migrate');

const Migrator = use('Apricot/Console/Commands/Migrate/Migrator');

class Migrate extends Command {
    get signature () {
        return 'migrate';
    }

    get name () {
        return 'Migrate';
    }

    get description () {
        return 'Run the database migrations';
    }

    get optionsDefinition () {
        return {
            database: {
                description: 'The database connection to use.',
                short: 'd',
            },
            force: {
                description: 'Force the operation to run when in production.',
                short: 'f'
            },
            path: {
                description: 'The path of migrations files to be executed.',
                short: 'p'
            },
            pretend: {
                description: 'Dump the SQL queries that would be run.',
                short: 'a'
            },
            seed: {
                description: 'Indicates if the seed task should be re-run.',
                short: 's'
            },
            step: {
                description: 'Force the migrations to be run so they can be rolled back individually.',
                short: 'b'
            },
			realpath: {
				description: 'Indicate any provided migration file paths are pre-resolved absolute paths.',
            	short: 'r'
			}
        };
    }

    get traits () {
        return [
            'Confirmable'
        ];
    }
    
    constructor () {
        super();

        this._migrator = Migrator.instance;
    }

    * handler () {
		if (!(yield * this.confirmToProceed())) {
			return;
		}

		yield * this._prepareDatabase();

		// Next, we will check to see if a path option has been defined. If it has
		// we will use the path relative to the root of this installation folder
		// so that migrations may be run for any path within the applications.
		yield * this._migrator.run(
			this.migrationPaths,
			{
				pretend: this.option('pretend'),
				step: this.option('step'),
			}
		);

		// Once the migrator has run we will grab the note output and send it out to
		// the console screen, since the migrator itself functions without having
		// any instances of the OutputInterface contract passed into the class.
		this._migrator
			.notes
			.forEach(note => {
				this[note.style](note.message);
			});

		// Finally, if the "seed" option has been given, we will re-run the database
		// seed task to re-populate the database, which is convenient when adding
		// a migration and a seed at the same time, as it is only this command.
		if (this.option('seed')) {
			yield * this.call('db:seed', [], {force: true});
		}
    }

    * _prepareDatabase () {
        this._migrator.connection = this.option('database');
        let exists = yield * this._migrator.repositoryExists();

        if (!exists) {
            let options = {};
            
            if (this.option('database')) {
                options.database = this.option('database');
            }
            
            yield * this.call('migrate:install', [], options);
        }
    }
}

module.exports = Migrate;