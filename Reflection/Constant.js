class Constant {
	/**
	 * @public
	 * @return {string}
	 */
	get name () {
		return this._data.name;
	}
	
	constructor (data) {
		this._data = data;
	}
}

module.exports = Constant;