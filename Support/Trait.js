class Trait {
    /**
     * @public
     * @const
     * @return {Number}
     */
    static bothType () {
        return 3;
    }

    /**
     * @public
     * @const
     * @return {Number}
     */
    static staticType () {
        return 2;
    }

    /**
     * @public
     * @const
     * @return {Number}
     */
    static normalType () {
        return 1;
    }

    static make(main, traits, prefix = '', type = Trait.bothType, callback = false) {
        prefix = prefix.trimRigth('/');

        traits.forEach(trait => {
            Trait._populate(main, use(`${ prefix }/${ trait.trimLeft('/') }`), type, callback)
        });
    }

    static _populate(main, trait, type, callback) {
        if ([Trait.bothType, Trait.staticType].indexOf(type) > -1) {
            Trait._copyStaticProperties(main, trait, callback);
        }

        if ([Trait.bothType, Trait.normalType].indexOf(type) > -1) {
            Trait._copyProperties(main.prototype, trait.prototype, callback);
        }
    }

    static _copyStaticProperties(destination, source,callback) {
        let sourcePropertyDescriptors = Object.getOwnPropertyDescriptors(source)
            .except(['length', 'name', 'namespace', 'prototype']);

        for (let key in sourcePropertyDescriptors) {
            if (!callback || callback(key, Trait.getValue(sourcePropertyDescriptors[key]), 'static') !== false) {
                Object.defineProperty(
                    destination,
                    key,
                    sourcePropertyDescriptors[key]
                );
            }
        }
    }

    static _copyProperties(destination, source, callback) {
        let sourcePropertyDescriptors = Object.getOwnPropertyDescriptors(source)
            .except(['constructor']);

        for (let key in sourcePropertyDescriptors) {
            if (!callback || callback(key, Trait.getValue(sourcePropertyDescriptors[key]), 'normal') !== false) {
                Object.defineProperty(
                    destination,
                    key,
                    sourcePropertyDescriptors[key]
                );
            }
        }
    }

    static getValue(decriptor) {
        return decriptor.value !== undefined ? decriptor.value : decriptor.get();
    }
}

module.exports = Trait;