class InvalidPath extends Error {
    constructor(){
        super('Invalid Path');
    }
}

module.exports = InvalidPath;