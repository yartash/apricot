class CompilesConditionals {
    compileIf(expression) {
        return `<% if (${ expression }) { %>`;
    }

    compileElse() {
        return `<% } else { %>`;
    }

    compileElseIf(expression) {
        return `<% else if (${ expression }) { %>`;
    }

    compileEndIf() {
        return `<% } %>`;
    }
}

module.exports = CompilesConditionals;