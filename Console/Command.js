const SignatureValidator = use('Validators/Signature');
const Output = use('Apricot/Console/Output');
const Table = use('Apricot/Console/Output/Helper/Table')

const Trait = use('Apricot/Support/Trait');

const CommandSignatureNotFoundException = use('Apricot/Exceptions/CommandSignatureNotFound');

class Command {
    static get validators () {
        return Command._validators;
    }
    
    static set validator (validator) {
        Command._validators.push(validator);
    }
    
    get regex () {
        if (!this._regex) {
            this._regex = this._generateRegex();
        }

        return this._regex
    }
    
    get arguments () {
        if (!this._arguments) {
            this._arguments = this._parseArguments();
        }

        return this._arguments;
    }

    get options () {
		if (!this._options) {
			this._options = this._parseOptions();
		}

		return this._options;
    }
    
    get input () {
        return this._input;
    }

    get output () {
    	return this._output;
	}

    get optionsDefinition () {
        return {};
    }
    
    constructor () {
        this._output = Output.instance;

        Trait.make(Command, this.traits || [], 'Apricot/Console/Traits')
    }

    bindInput (input) {
        this._input = input;
    }

    argument (name, defaultValue = false) {
        return this.arguments[name] || defaultValue;
    }
    
    option (name, defaultValue) {
        return this.options[name] ? this.options[name] : defaultValue;
    }

	/**
	 * Returns true if an Option object exists by name.
	 *
	 * @param {String} name The InputOption name
	 * @return {Boolean} true if the Option object exists, false otherwise
	 */
    hasOption (name) {
    	return name in this.options;
	}

    check(signature) {
        for (let index in Command.validators) {
            if (!Command.validators[index].match(this, signature)) {
                return false;
            }
        }

        return true;
    }

    info (text) {
        this._output.info(text);
    }

	comment (text) {
        this._output.comment(text);
    }

    error (text) {
        this._output.error(text);
    }

    /**
     * Call another console command.
     *
     * @param {string} signature
     * @param {Array} arguments
     * @param {Object} options
     */
    * call (signature, argument, options) {
        let command = use('Peach').match(`${ signature }${ argument.join(' ') }`);

        let newInput = Object.assign(this.input);
        newInput.rawArguments = argument;
        newInput.rawOptions = options;
        command.bindInput(newInput);
        yield * command.handler();
    }

	/**
	 * Format input to textual table.
	 *
     * @public
	 * @param {Array<String>} headers
	 * @param {Array<Any>} rows
	 * @param {String} tableStyle
	 * @param {Array<>} columnStyles
	 */
	table (headers, rows, tableStyle = 'default', columnStyles = []) {
		let table = new Table(this._output);

		// 	if ($rows instanceof Arrayable) {
		// 	$rows = $rows->toArray();
		// }
		//
		// $table->setHeaders((array) $headers)->setRows($rows)->setStyle($tableStyle);
		//
		// foreach ($columnStyles as $columnIndex => $columnStyle) {
		// 	$table->setColumnStyle($columnIndex, $columnStyle);
		// }
		//
		// $table->render();
	}

    _generateRegex() {
        if (!this.signature) {
            throw new CommandSignatureNotFoundException(this.constructor.name);
        }

        let options = [];

        for (let option in this.optionsDefinition) {
            let setting = this.optionsDefinition[option];

            options.push(`(--${ option }|-${ setting.short })?\\s*(=)?\\s*(\\w*)`);
        }

        return new RegExp(`^${ 
            this.signature
                .split(/\s+/)
                .map(part => {
                    return /^\{.*\}$/.test(part) ? '\\S+' : part;
                })
                .join('\\s+')
        }\\s*${ options.join('\\s*') }$`);
    }

    _parseArguments() {
        let result = {};
        
        this.signature
            .split(/\s+/)
            .splice(1)
            .forEach((item, index) => {
                result[item.replace(/(\W*)(\w+)(\W*)/, '$2')] = this.input._rawArguments[index];
            });
        
        return result;
    }

	_parseOptions () {
        let result = {};

        for (let option in this.optionsDefinition) {
        	let setting = this.optionsDefinition[option];

            result[option] = this.input._rawOptions[option] || this.input._rawOptions[setting.short] || setting.default;
		}

		return result;
    }
}

Command._validators = [
    new SignatureValidator()
];

module.exports = Command;