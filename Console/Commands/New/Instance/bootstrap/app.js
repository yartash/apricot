module.exports = {
    providers: [
        'App/Providers/App',
        'Apricot/Database/DatabaseProvider',
        'Apricot/FileSystem/FileSystemProvider',
        'Apricot/Console/PeachProvider'
    ],

    aliases: {
        DB: 'Apricot/Support/Facade/Database',
        File: 'Apricot/Support/Facade/File',
        Peach: 'Apricot/Support/Facade/Peach',
		Schema: 'Apricot/Support/Facade/Schema'
    }
};