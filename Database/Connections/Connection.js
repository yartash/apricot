const MongoGrammar = use('Apricot/Database/Query/Grammars/MongoGrammar');
const PostgresGrammar = use('Apricot/Database/Query/Grammars/PostgresGrammar');
const SchemaBuilder = use('Apricot/Database/Schema/Builders/Builder');
const QueryBuilder = use('Apricot/Database/Query/Builder');

const PostgresProcessor = use('Apricot/Database/Query/Processors/PostgresProcessor');

const GeneratorHandler = use('Apricot/Support/GeneratorHandler');
const Trait = use('Apricot/Support/Trait');

const LogicException = use('Apricot/Exceptions/Logic');
const QueryExceptionException = use('Apricot/Exceptions/QueryException');

class Connection {
    get driver() {
        return this._config.driver;
    }

    set reconnector(value) {
        this._reconnector = value;
    }

    get config() {
        return this._config;
    }

    get name() {
        return this.config['name'] ? this.config['name'] : '';
    }

    set client (value) {
        this._client = value;
    }

    get client() {
        return this._client;
    }

    get queryGrammar () {
        return this._queryGrammar;
    }

	get schemaGrammar () {
		return this._schemaGrammar;
	}

	get schemaBuilder () {
		if (!this._schemaGrammar) {
			this.useDefaultSchemaGrammar;
		}

		return new SchemaBuilder(this);
	}

	get useDefaultSchemaGrammar () {
		this._schemaGrammar = this.defaultSchemaGrammar;
	}

	get defaultSchemaGrammar () {

	}

    get processor() {
        return this._processor;
    }

	/**
	 * Get the table prefix for the connection.
	 *
	 * @return {string}
	 */
	get tablePrefix () {
		return this.config.prefix;
	}

	/**
	 * Get a new query builder instance.
	 *
	 * @public
	 * @return {Apricot/Database/Query/Builder}
	 */
	get query () {
		return new QueryBuilder(this, this.queryGrammar, this.processor);
	}

	get traits () {
		return [
			'ManagesTransactions',
			'DetectsDeadlocks'
		];
	}
    
    constructor(client, config, error) {
        this._client = client;
        this._config = config;
        this._error = error;
        this._transactions = 0;

        /**
         * Indicates whether queries are being logged.
         *
         * @protected
         * @property {Boolean}
         */
        this._loggingQueries = false;
        
        /**
         * All of the queries run against the connection.
         *
         * @protected
         * @property {Array<Any>}
         */
        this._queryLog = [];
        
        /**
         * Indicates if the connection is in a "dry run".
         *
         * @protected
         * @property {Boolean}
         */
        this._pretending = false;

		/**
		 * The number of active transactions.
		 *
		 * @protected
		 * @property {Int}
		 */
		this._transactions = 0;

		Trait.make(Connection, this.traits || [], 'Apricot/Database/Traits')

        this.useQueryGrammar();
        this.usePostProcessor();
    }

    static resolverFor(driver, callback) {
        Connection._resolvers[driver] = callback;
    }
    
    static getResolver(driver) {
        return Connection._resolvers[driver] ? Connection._resolvers[driver] : null;
    }

    /**
     * Execute the given callback in "dry run" mode.
     *
     * @public
     * @param {Function} callback
     * @return {Promise<Array<String>>}
     */
    pretend (callback) {
        return this._withFreshQueryLog(() => {
        	return new Promise(resolve => {
				this._pretending = true;

				// Basically to make the database connection "pretend", we will just return
				// the default values for all the query methods, then we will return an
				// array of queries that were "executed" within the Closure callback.
				this._pretendCallbackExecutor(callback).then(() => {
					this._pretending = false;

					resolve(this._queryLog);
				});
			});
        });
    }

	/**
	 * Begin a fluent query against a database table.
	 *
     * @public
	 * @param {String} table
	 * @return {Apricot/Database/Query/Builder}
	 */
	table (table) {
		return this.query.from(table);
	}

    select(query, bindings = []) {
        return this._run(query, bindings, (query, bindings) => {
            if (this.pretending()) {
                return Promise.resolve([]);
            }

            return this.client
                .then(client => {
                    return client.query(query, this.prepareBindings(bindings));
                });
        });
    }

    /**
     * Run an insert statement against the database.
     *
     * @public
     * @param {String} query
     * @param {Array<Object>|} bindings
     * @return {Boolean}
     */
    insert (query, bindings = []) {
        return this.statement(query, bindings);
    }

    /**
     * Execute an SQL statement and return the boolean result.
     *
     * @public
     * @param {String} query
     * @param {Array<Any>} bindings
     * @return {Boolean}
     */
    statement(query, bindings = []) {
        return this._run(query, bindings, (query, bindings) => {
            if (this.pretending()) {
                return Promise.resolve([]);
            }

            this.recordsHaveBeenModified();

            return this.client
                .then(client => {
                    return client.query(query, this.prepareBindings(bindings));
                });
        });
    }

	/**
	 * Run a delete statement against the database.
	 *
	 * @param {$tring} query
	 * @param {Array<Any>} bindings
	 * @return {Int}
	 */
	delete (query, bindings = []) {
		return this.affectingStatement(query, bindings);
	}

	/**
	 * Run an SQL statement and get the number of rows affected.
	 *
	 * @param {String} query
	 * @param {Array} bindings
	 * @return {Int}
	 */
	affectingStatement (query, bindings = []) {
		return this._run(query, bindings, (query, bindings) => {
			if (this.pretending()) {
				Promise.resolve(0);
			}

			return new Promise(resolve => {
				this.client
					.then(client => {
						return client.query(query, this.prepareBindings(bindings));
					})
					.then(result => {
						this.recordsHaveBeenModified(result.rows.length > 0);

						resolve(result.rows.length);
					});
			});
		});
	}

    /**
     * Indicate if any records have been modified.
     *
     * @public
     * @param {Boolean} value
     */
    recordsHaveBeenModified (value = true) {
        if (!this._recordsModified) {
            this._recordsModified = value;
        }
    }

    prepareBindings(bindings) {
        return bindings;
    }

    useQueryGrammar() {
        let grammar;

        switch (this.driver) {
            case 'mongo':
                grammar = new MongoGrammar();
                break;
            case 'pgsql':
                grammar = new PostgresGrammar();
                break;
        }

        this._queryGrammar = grammar;
    }

    usePostProcessor() {
        let processor;

        switch (this.driver) {
            case 'pgsql':
                processor = new PostgresProcessor();
                break;
        }

        this._processor = processor;
    }

    disconnect() {
        this.client = null;
    }

	withTablePrefix (grammar) {
		grammar.tablePrefix = this.config.prefix;

		return grammar;
	}

    reconnect() {
        if (Function.isFunction(this._reconnector)) {
            return this._reconnector.call(null, this);
        }

        throw new LogicException('Lost connection and no reconnector available.');
    }

    pretending() {
        return this._pretending === true;
    }

	/**
	 * Get an option from the configuration options.
	 *
	 * @public
	 * @param {(String|Null)} option
	 * @return {*}
	 */
	getConfig (option = null) {
		return option === null ? this.config : this.config[option];
	}

    /**
     * Enable the query log on the connection.
     *
     * @public
     */
    enableQueryLog () {
        this._loggingQueries = true;
    }

    _reconnectIfMissingConnection() {
        if (this._client === null) {
            let connection = this.reconnect()

            this.client = connection.client;
        }
    }

    _run(query, bindings, callback) {
        this._reconnectIfMissingConnection();

        let start = new Date().getTime();

        return new Promise((resolve, reject) => {
            this._runQueryCallback(query, bindings, callback)
                .then(
                    result => {
                        this._logQuery(query, bindings, new Date().getTime() - start);

                        resolve(result);
                    },
                    error => {
                        this._handleQueryException(error, query, bindings, callback)
                            .then(
                                result => {
                                    resolve(result);
                                },
                                error => {
                                    reject(error);
                                }
                            );
                    }
                );
        });
    }

    _handleQueryException(error, query, bindings, callback) {
        if (this._transactions >= 1) {
        	return Promise.reject(error);
        }

        return this._tryAgainIfCausedByLostConnection(error, query, bindings, callback);
    }

    _tryAgainIfCausedByLostConnection(error, query, bindings, callback) {
        if (this._causedByLostConnection(error.previous)) {
            let connection = this.reconnect();

            this.client = connection.client;

            return this._runQueryCallback(query, bindings, callback);
        }

        Promise.reject(error);
    }

	/**
	 * Log a query in the connection's query log.
	 *
	 * @private
	 * @param {String} query
	 * @param {Array<Any>} bindings
	 * @param {Int} time
	 */
    _logQuery(query, bindings, time) {
        if (this._config.loggingQueries) {
            log.debug(query, bindings, time);
        }

		if (this._loggingQueries) {
			this._queryLog.push({query, bindings, time});
		}
    }

    _runQueryCallback(query, bindings, callback) {
        return new Promise((resolve, reject) => {
            callback(query, bindings)
                .then(
                    result => {
                        resolve(result);
                    },
                    error => {
                        reject(new QueryExceptionException(query, this.prepareBindings(bindings), error));
                    }
                );
        });
    }

    /**
     * Execute the given callback in "dry run" mode.
     *
     * @public
     * @param {Function} callback
     * @return {Promise<Array<String>>}
     */
    _withFreshQueryLog (callback) {
    	return new Promise(resolve => {
			let loggingQueries = this._loggingQueries;

			// First we will back up the value of the logging queries property and then
			// we'll be ready to run callbacks. This query log will also get cleared
			// so we will have a new log of all the queries that are executed now.
			this.enableQueryLog();

			this._queryLog = [];

			// Now we'll execute this callback and capture the result. Once it has been
			// executed we will restore the value of query logging and give back the
			// value of hte callback so the original callers can have the results.
			callback().then(result => {
				this._loggingQueries = loggingQueries;
				resolve(result);
			});
		});
    }

	/**
	 * Execute pretend callback function.
	 *
	 * @private
	 * @param {Function|Generator} callback
	 * @return {Promise<any>}
	 */
    _pretendCallbackExecutor (callback) {
    	return new Promise(resolve => {
			if (Function.isGenerator(callback)) {
				(new GeneratorHandler(resolve)).executor(callback());
			} else {
				let result = callback(this);

				if (Function.isPromise(result)) {
					result.then(data => {
						resolve(data);
					})
				} else {
					resolve(result);
				}
			}
		});
	}
}

Connection._resolvers = {};

module.exports = Connection;