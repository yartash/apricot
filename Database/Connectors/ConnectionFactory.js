const Connection = use('Apricot/Database/Connections/Connection');
const MongoConnection = use('Apricot/Database/Connections/MongoConnection');
const PostgresConnection = use('Apricot/Database/Connections/PostgresConnection');

const MongoConnector = use('MongoConnector');
const PostgresConnector = use('PostgresConnector');

const InvalidArgumentException = use('Apricot/Exceptions/InvalidArgument');

class ConnectionFactory {
    make(config, name = null) {
        config = this._parseConfig(config, name);

        return this._createSingleConnection(config);
    }

    _parseConfig(config, name) {
        if(!config['prefix'] || config['prefix'] === null) {
            config['prefix'] = '';
        }
        
        config['name'] = name;

        return config;
    }

    _createSingleConnection(config) {
        let client = this._createConnector(config).connect(config);
        
        return this._createConnection(client, config);
    }

    // _createConnectionResolver(config) {
    //     return () => {
    //         // if (!ConnectionFactory.connections[config.driver]) {
    //         //     ConnectionFactory.connections[config.driver] = this._createConnector(config).connect(config);
    //         // }
    //         //
    //         // return ConnectionFactory.connections[config.driver];
    //
    //         return this._createConnector(config).connect(config);
    //     // };
    // }

    _createConnector(config) {
        if (!config.driver) {
            throw new InvalidArgumentException('A driver must be specified.');
        }

        switch (config.driver) {
            case 'mongo':
                return new MongoConnector(config);
            // case 'mysql':
            //     return new MySqlConnector;
            case 'pgsql':
                return new PostgresConnector(config);
            // case 'sqlite':
            //     return new SQLiteConnector;
            // case 'sqlsrv':
            //     return new SqlServerConnector;
        }

        throw new InvalidArgumentException(`Unsupported driver [${ config.driver }]`);
    }

    _createConnection(client, config) {
        let resolver = Connection.getResolver(config.driver)

        if (resolver) {
            return resolver(client, config);
        }

        switch (config.driver) {
            case 'mongo' :
                return new MongoConnection(client, config);
            // case 'mysql':
            //     return new MySqlConnection($connection, $database, $prefix, $config);
            case 'pgsql':
                return new PostgresConnection(client, config);
            // case 'sqlite':
            //     return new SQLiteConnection($connection, $database, $prefix, $config);
            // case 'sqlsrv':
            //     return new SqlServerConnection($connection, $database, $prefix, $config);
        }

        throw new InvalidArgumentException(`nsupported driver ${ config.driver }`);
    }
}

ConnectionFactory.connections = {};

module.exports = ConnectionFactory;