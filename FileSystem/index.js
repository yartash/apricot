const crypto = require('crypto');
const fs = require('fs');
const path = require('path');
const glob = require('glob');

const mimeType = require('mime');

const FileNotFoundException = use('Apricot/Exceptions/FileNotFound');
const NotFileException = use('Exceptions/NotFile');

class FileSystem {
    exists(file) {
        return new Promise(resolve => {
            fs.access(file, error => {
                resolve(!error);
            })
        });
    }

	resolve (...paths) {
        return path.resolve.apply(null, paths);
    }

    join (...paths) {
        return path.join.apply(null, paths);
    }

    /**
     * 
     * @param {string} directory
     * @param {string} extension
     * @return {string}
     */
    basename (directory, extension) {
        return path.basename(directory, extension);
    }

    /**
     *
     * @param {string} directory
     * @return {string}
     */
    dirname (directory) {
        return path.dirname(directory);
    }

    * get(file) {
        if (yield this.isFile(file)) {
            return yield new Promise((resolve, reject) => {
                fs.readFile(file, 'utf8', (error, data) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve(data);
                    }
                });
            });
        }

        throw new FileNotFoundException(file);
    }

    isFile(file) {
        return new Promise(resolve => {
            this
                ._getStat(file)
                .then(stat => {
                    resolve(stat.isFile());
                });
        });
    }

    isDirectory(file) {
        return new Promise(resolve => {
            this
                ._getStat(file)
                .then(stat => {
                    resolve(stat.isDirectory());
                });
        });
    }

    * getRequire(file) {
        if (yield this.isFile(file)) {
            return require(file);
        }

        throw new FileNotFoundException(file);
    }

    * hash(file) {
        if (yield this.isFile(file)) {
            return yield new Promise((resolve, reject) => {
                let output = crypto.createHash('md5');
                let input = fs.createReadStream(file);

                input.on('error', error => {
                    reject(error);
                });

                output.once('readable', () => {
                    resolve(
                        output
                            .read()
                            .toString('hex')
                    );
                });

                input.pipe(output);
            });
        }

        throw new FileNotFoundException(file);
    }
    
    * write (file, content) {
        if ((yield this.exists(file)) && (yield this.isDirectory(file))) {
            throw new NotFileException(file);
        }

    		return yield new Promise((resolve, reject) => {
    			fs.writeFile(
    				file,
    				content,
    				{
    					encoding: 'utf8',
    					mode: 0o777,
    					flag: 'w'
    				},
    				error => {
    					if (error) {
    						reject(error);
    					} else {
    						resolve();
    					}
    				}
    			)
    		});
    }
    
    * writeJson (file, json) {
        return yield * this.write(
            file,
			JSON.stringify(
				json,
				null,
				'\t'
			).replace(/\n/g, '\n')
        );
    }

    * prepend(file, content) {
        return yield * this.write(file, content + (yield * this.get(file)));
    }

    * append(file, content) {
        if (yield this.isFile(file)) {
            return yield new Promise((resolve, reject) => {
                fs.appendFile(
                    file,
                    content,
                    'utf8',
                    error => {
                        if (error) {
                            reject(error);
                        } else {
                            resolve();
                        }
                    }
                )
            });
        }

        throw new FileNotFoundException(file);
    }

    delete (files) {
        files = Array.isArray(files) ?
            files :
            Object.keys(arguments);

        return Promise
            .all(
                Array.map(
                    files,
                    file => {
                        return new Promise((resolve, reject) => {
                            fd.unlink(
                                file,
                                error => {
                                    if (error) {
                                        reject(error);
                                    } else {
                                        resolve();
                                    }
                                }
                            );
                        });
                    }
                )
            );
    }

    move (oldPath, newPath) {
        return new Promise((resolve, reject) => {
            fs.rename(
                oldPath,
                newPath,
                error => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve();
                    }
                }
            )
        });
    }

    makeDirecory (directory, mode = 0o777, recursive = false, force = false) {
		let make = (dir) => {
			return new Promise(resolve => {
				fs.mkdir(dir, mode, error => {
					if (error && !force) {
						Promise.reject(error);
					} else {
						resolve();
					}
				});
			});
		}

		if (recursive) {
			return new Promise(resolve => {
				let parts = path.normalize(directory).split(path.sep);
				let index = 1;
				let cycle = () => {
					if (index <= parts.length) {
						let dir = parts.slice(0, index).join(path.sep);

						this.exists(dir).then(exists => {
							if (exists) {
								index++;
								cycle();
							} else {
								make(dir).then(() => {
									index++;
									cycle();
								})
							}
						});
					} else {
						resolve();
					}
				};

				cycle();
			})
		} else {
			return make(directory);
		}
    }

    * copy(oldPath, newPath) {
      	if (yield this.isFile(oldPath)) {
  			return yield new Promise((resolve, reject) => {
  				fs.copyFile(
  					oldPath,
  					newPath,
  					error => {
  						if (error) {
  							reject(error);
  						} else {
  							resolve();
  						}
  					}
  				)
  			});
  		} else {
      	    yield this.makeDirecory(newPath)
      	    
      	    let files = yield this.scan(oldPath);
  
      	    for (let i = 0; i < files.length; i++) {
      	        yield * this.copy(this.join(oldPath, files[i]), this.join(newPath, files[i]))
              }
  		}
  	}

	scan (directory) {
        return new Promise((resolve, reject) => {
            fs.readdir(directory, (error, files) => {
                if (error) {
                    reject(error);
                }

                resolve(files.filter(file => {
                    return ['.', '..'].indexOf(file) == -1;
                }));
            });
        });
    }

    link(target, link) {
        return new Promise((resolve, reject) => {
            fs.symlink(
                target,
                link,
                error => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve();
                    }
                }
            )
        });
    }

    type(file) {
        return new Promise(resolve => {
            this
                ._getStat(file)
                .then(stat => {
                    resolve(
                        stat.isDirectory() ?
                            'dir' :
                            (
                                stat.isFile() ?
                                    'file' :
                                    ''
                            )
                    );
                });
        });
    }

	/**
	 * Find path names matching a given pattern.
	 *
	 * @public
	 * @param {String} pattern
	 * @return {Array<String>}
	 */
	glob (pattern) {
		return new Promise(resolve => {
			glob(pattern, (error, files) =>{
				if (error) {
					Promise.reject(error);
				}

				resolve(files);
			})
		});
	}

    size(file) {
        return new Promise(resolve => {
            this
                ._getStat(file)
                .then(stat => {
                    resolve(stat.size);
                });
        });
    }

    lastModified(file) {
        return new Promise(resolve => {
            this
                ._getStat(file)
                .then(stat => {
                    resolve(stat.mtime);
                });
        });
    }

    name(file) {
        return path
            .parse(file)
            .name;
    }

    base(file) {
        return path
            .parse(file)
            .base;
    }

    dirName(file) {
        return path
            .parse(file)
            .dir;
    }

    extension(file) {
        return path
            .parse(file)
            .ext;
    }

    mimeType(file) {
        return mimeType.getType(file);
    }

    _getStat(file) {
        return new Promise((resolve, reject) => {
            fs.stat(
                file,
                (error, stat) => {
                    if (!error) {
                        resolve(stat);
                    } else {
                        reject(error);
                    }
                }
            );
        });
    }
}

module.exports = FileSystem;