class CompilesAsset {
    compileScript(url, type = 'text/javascript', options = {}) {
        return `<script type="${ type }" src="<%= __env.evaluateSrc(${ url }) %>"></script>`;
    }

    compileCss(url, type = 'text/css', options = {}) {
        return `<link rel="stylesheet" type="${ type }" href="<%= __env.evaluateSrc(${ url }) %>">`;
    }
}

module.exports = CompilesAsset;