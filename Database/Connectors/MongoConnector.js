const { MongoClient } = require('mongodb');

const Connector = use('Connector');

class MongoConnector extends Connector {
    get dsn() {
        let config = this._config;

        return `mongodb://${ config.host }:${ config.port }/${ config.database }`;
    }

    get options() {
        return !this._config.options ? this._config.options : {};
    }

    connect(config) {
        return new Promise((resolve, reject) => {
            MongoClient.connect(this.dsn, this.options, (error, client) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(client);
                }
            });
        });
    }
}

module.exports = MongoConnector;