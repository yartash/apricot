class NotRegisteredMiddleware extends Error {
    constructor(middleware) {
        super(`${ middleware } is not registered as a named middleware`);
    }
}

module.exports = NotRegisteredMiddleware;