class GuardsAttributes {
    get options() {
        return {
            fillable: [],
            guarded: ['*']
        };
    }

    get fillable() {
        return this._fillable;
    }

    get guarded() {
        return this._guarded;
    }

	/**
	 * Run the given callable while being unguarded.
	 *
	 * @public
	 * @param {Function} callback
	 * @return {Promise<Any>}
	 */
	static unguarded (callback) {
        const Model = use('Apricot/Database/Grape/Model');

		if (Model._unguarded) {
			return callback();
		}

        Model.unguard();

		try {
			return callback();
		} finally {
            Model.reguard();
		}
	}

    /**
     * Disable all mass assignable restrictions.
     *
     * @public
     * @static
     * @param {Boolean} state
     */
    static unguard (state = true) {
        const Model = use('Apricot/Database/Grape/Model');

        Model._unguarded = state;
    }

    /**
     * Enable the mass assignment restrictions.
     *
     * @public
     * @static
     */
    static reguard () {
        const Model = use('Apricot/Database/Grape/Model');

        Model._unguarded = false;
    }
    
    totallyGuarded() {
        return this.fillable.length == 0 && this.guarded == ['*'];
    }

    _fillableFromObject(attributes) {
        if (this.fillable.length > 0 && !Model._unguarded) {
            // return array_intersect_key($attributes, array_flip($this->getFillable()));
        }
//
// return $attributes;
    }
}

/**
 * Indicates if all mass assignment is enabled.
 *
 * @static
 * @protected
 * @property bool
 */
GuardsAttributes._unguarded = false;

module.exports = GuardsAttributes;