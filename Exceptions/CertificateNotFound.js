class CertificateNotFound extends Error {
    constructor() {
        super('Certificate or key not exist.');
    }
}

module.exports = CertificateNotFound;