const BaseGrammar = use('Apricot/Database/Grammar');
const JoinClause = use('Apricot/Database/Query/JoinClause');

class Grammar extends BaseGrammar {
	/**
	 * Determine if the grammar supports savepoints.
	 *
     * @public
	 * @return {Boolean}
	 */
	get supportsSavepoints () {
		return true;
	}
	
	get operators () {
	    return this._operators;
    }

    constructor() {
        super();

        this._selectComponents = [
            'aggregates',
            'columns',
            'from',
            'joins',
            'wheres',
            'groups',
            'havings',
            'orders',
            'limit',
            'offset',
            'unions',
            'lock'
        ];

        /**
         * The grammar specific operators.
         *
         * @protected
         * @property {Array<String>}
         */
        this._operators = [];
    }

    compileSelect(query) {
        let original = query.columns;

        if (query.columns === null) {
            query.columns = ['*'];
        }

        let sql = this._concatenate(this._compileComponents(query)).trim();

        query.columns = original;

        return sql;
    }

	/**
	 * Compile the SQL statement to define a savepoint.
	 *
	 * @param {String} name
	 * @return {String}
	 */
	compileSavepoint (name)	{
		return `SAVEPOINT ${ name }`;
	}

    /**
     * Compile the SQL statement to execute a savepoint rollback.
     *
     * @public
     * @param {String} name
     * @return {String}
     */
    compileSavepointRollBack (name) {
        return `ROLLBACK TO SAVEPOINT ${ name }`;
    }

	/**
	 * Compile an insert statement into SQL.
	 *
	 * @public
	 * @param {Apricot/Database/Query/Builder} query
	 * @param {Array<any>} values
	 * @return {String}
	 */
	compileInsert (query, values) {
		// Essentially we will force every insert to be treated as a batch insert which
		// simply makes creating the SQL easier for us since we can utilize the same
		// basic routine regardless of an amount of records given to us to insert.
        let table = this.wrapTable(query.from());

		if (!Array.isArray(values)) {
			values = [values];
		}

		let columns = this.columnize(Object.keys(values.first()));

		// We need to build a list of parameter place-holders of values that are bound
		// to the query. Each insert should have the exact same amount of parameter
		// bindings so we will loop through the record and parameterize them all.
		let parameters = values.map(record => {
            return `(${ this.parameterize(record) })`;
		})
            .join(', ');
		
		return `insert into ${ table } (${ columns }) values ${ parameters }`;
	}

	/**
	 * Compile a delete statement into SQL.
	 *
	 * @public
	 * @param {Apricot/Database/Query/Builder} query
	 * @return {String}
	 */
	compileDelete (query) {
		let wheres = query.getWheres().length ? this._compileWheres(query) : '';

		return `delete from ${ this.wrapTable(query.getFrom()) } ${ wheres }`.trim();
	}

	/**
	 * Prepare the bindings for a delete statement.
	 *
	 * @param {Object} bindings
	 * @return {Array<Any>}
	 */
	prepareBindingsForDelete (bindings) {
		return Array.flatten(Object.values(bindings));
	}

    _concatenate(segments) {
        return segments.filter(segment => {
            return segment.toString() !== '';
        })
            .join(' ');
    }

    _compileComponents(query) {
        let sql = [];

        return this._selectComponents
            .map(component => {
            	component = component.ucfirst();
                let data = query[`get${ component }`]();

                if (Array.isArray(data) && !data.length) {
                    return;
                }

                if (Object.isObject(data) && !Object.keys(data).length) {
                    return;
                }

                if (data !== undefined) {
                    return this[`_compile${ component }`](query, data);
                }
            })
            .filter(sql => {
                return sql !== undefined;
            });
    }

    _compileColumns(query, columns) {
        if (Object.keys(query.aggregates).length) {
            return;
        }

        let select = query.isDistinct ? 'select distinct ' : 'select ';

        return select + this.columnize(columns);
    }

	/**
	 * Compile the "limit" portions of the query.
	 *
	 * @protected
	 * @param {Apricot/Database/Query/Builder} query
	 * @param {Int} limit
	 * @return {String}
	 */
	_compileLimit (query, limit) {
		return `limit ${ limit }`;
	}

    _compileFrom(query, table) {
        return 'from ' + this.wrapTable(table);
    }

    /**
     * Compile the "where" portions of the query.
     *
     * @protected
     * @param {Apricot/Database/Query/Builder} query
     * @return {String}
     */
    _compileWheres (query) {
        // Each type of where clauses has its own compiler function which is responsible
        // for actually creating the where clauses SQL. This helps keep the code nice
        // and maintainable since each clause has a very small method that it uses.
        if (!query.wheres.length) {
            return '';
        }

        // If we actually have some where clauses, we will strip off the first boolean
        // operator, which is added by the query builders for convenience so we can
        // avoid checking for the first clauses in each of the compilers methods.
        let sql = this._compileWheresToArray(query);

        if (sql.length) {
            return this._concatenateWhereClauses(query, sql);
        }

        return '';
    }

    /**
     * Compile the "order by" portions of the query.
     *
     * @protected
     * @param {Apricot/Database/Query/Builder} query
     * @param {Array<Object>} orders
     * @return {String}
     */
    _compileOrders (query, orders) {
        if (orders.length) {
            return `order by ${ this._compileOrdersToArray(query, orders).join(', ') }`;
        }

        return '';
    }
    
    /**
     * Compile the query orders to an array.
     *
     * @param {Apricot/Database/Query/Builder} query
     * @param {Array<Object>} orders
     * @return {Array<String>}
     */
    _compileOrdersToArray (query, orders) {
        return orders.map(order => {
            return order.sql ?
                order.sql :
                `${ this.wrap(order.column) } ${ order.direction }`;
        });
    }

    /**
     * Compile an aggregated select clause.
     *
     * @param {Apricot/Database/Query/Builder} query
     * @param {Array<Object>} aggregate
     * @return {String}
     */
    _compileAggregates (query, aggregate) {
        let column = this.columnize(aggregate['columns']);

        // If the query has a "distinct" constraint and we're not asking for all columns
        // we need to prepend "distinct" onto the column name so that the query takes
        // it into account when it performs the aggregating operations on the data.
        if (query.isDistinct && column !== '*') {
            column = `distinct ${ column }`;
        }

        return `select ${ aggregate.functionName }(${ column }) as aggregate`;
    }

    /**
     * Get an array of all the where clauses for the query.
     *
     * @protected
     * @param {Apricot/Database/Query/Builder} query
     * @return {Array<String>}
     */
    _compileWheresToArray (query) {
        return query.wheres
            .map(where => {
                return `${ where.boolean} ${ this[`_where${ where.type.ucfirst() }`](query, where) }`;
            });
    }

    /**
     * Compile a basic where clause.
     *
     * @protected
     * @param {Apricot/Database/Query/Builder} query
     * @param {Array<QueryBuilderWhere>} where
     * @return string
     */
    _whereBasic (query, where) {
        let value = this.parameter(where.value);

        return `${ this.wrap(where.column) } ${ where.operator } ${ value }`;
    }

    /**
     * Format the where clause statements into one string.
     *
     * @protected
     * @param {Apricot/Database/Query/Builder} query
     * @param {Array<string>} sql
     * @return {String}
     */
    _concatenateWhereClauses (query, sql) {
        let conjunction = query instanceof JoinClause ? 'on' : 'where';

        return `${ conjunction } ${ this._removeLeadingBoolean(sql.join(' ')) }`;
    }

    /**
     * Remove the leading boolean from a statement.
     *
     * @protected
     * @param {String} value
     * @return {String}
     */
    _removeLeadingBoolean (value) {
        return value.replace(/and |or /i, '');
    }
}

module.exports = Grammar;