class NotBeFunction extends Error {
    get methods () {
    	return this._methods;
	}

	get url () {
    	return this._url;
	}

    constructor (url, methods) {
        super(`Route [${ methods.join(' | ') }]${ url } action there cannot be a function.`);
        
        this._methods = methods;
        this._url = url;
    }
}

module.exports = NotBeFunction;