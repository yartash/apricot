const Env = use('Env');

module.exports = {
    providers: {
        users: {
            driver: 'grape',
            model:  use('App/Models/User').prototype.constructor.name,
        }
    }
};
