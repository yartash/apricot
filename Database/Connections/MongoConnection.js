const Connection = use('Connection');

class MongoConnection extends Connection {
    select(query, bindings = {}) {
        return this._run(query, bindings, (query, bindings) => {
            if (this.pretending()) {
                return [];
            }
        })
    //
    //     // For select statements, we'll simply execute the query and return an array
    //     // of the database result set. Each element in the array will be a single
    //     // row from the database table, and will either be an array or objects.
    //     $statement = $this->prepared($this->getPdoForSelect($useReadPdo)
    // ->prepare($query));
    //
    //     $this->bindValues($statement, $this->prepareBindings($bindings));
    //
    //     $statement->execute();
    //
    //     return $statement->fetchAll();
    // });
    }

    _run(query, bindings, callback) {
        this._reconnectIfMissingConnection();

        let start = new Date().getTime();
        let result;

    try {
        result = this._runQueryCallback(query, bindings, callback);
    } catch (exception) {
//     $result = $this->handleQueryException(
//         $e, $query, $bindings, $callback
//     );
    }
//
// // Once we have run the query we will calculate the time that it took to run and
// // then log the query, bindings, and execution time so we will report them on
// // the event that the developer needs them. We'll log time in milliseconds.
// $this->logQuery(
//     $query, $bindings, $this->getElapsedTime($start)
// );
//
// return $result;
    }

    _runQueryCallback(query, bindings, callback) {
        try {
            return callback(query, bindings);
        } catch (exception) {

        }
    }
}

module.exports = MongoConnection;