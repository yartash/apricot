class SchemeValidator {
    match(route, request) {
        if (route.httpOnly) {
            return !request.secure;
        } else if (route.secure) {
            return request.secure;
        }

        return true;
    }
}

module.exports = SchemeValidator;