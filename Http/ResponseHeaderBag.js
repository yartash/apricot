const _ = require('lodash');

const BaseHeaderBag = use('HeaderBag');
const Cookie = use('Cookie');

const InvalidArgumentException = use('Exceptions/InvalidArgument');

class HeaderBag extends BaseHeaderBag {
    static get cookiesFlat() {
        return 'flat';
    }

    static get cookiesObject() {
        return 'object';
    }

    static get dispositionAttachment() {
        return 'attachment';
    }

    static get dispositionInline() {
        return 'inline';
    }

    get all() {
        let headers = super.all();
        headers['set-cookie'] = [];

        _
            .each(
                this.getCookies(),
                cookie => {
                    headers['set-cookie'].push(cookie);
                }
            );

        return headers;
    }

    constructor(headers = {}) {
        super(headers);

        this._cookies = {};
        this._headerNames = {};
        this._computedCacheControl = {};

        if (!this._headers['cache-control']) {
            this.set('Cache-Control', ['']);
        }
    }

    // toString() {
    //     let cookies = '';
    //
    //     _
    //         .each(
    //             this.getCookies(),
    //             cookie => {
    //                 cookies += `Set-Cookie: ${ cookie }\r\n`
    //             }
    //         );
    //
    //     let sortKeys = _
    //         .sortBy(
    //             _.keys(this._headerNames),
    //             key => {
    //                 return key;
    //             }
    //         );
    //
    //     this._headerNames = _
    //         .map(
    //             sortKeys,
    //             key => {
    //                 return this._headerNames[key];
    //             }
    //         );
    //
    //     return super.toString() + cookies;
    // }

    allPreserveCase() {
        let result = {};

        _
            .each(
                this.all,
                (value, key) => {
                    result[(this._headerNames[key] ? this._headerNames[key] : key)] = value;
                }
            );

        return result;
    }

    allPreserveCaseWithoutCookies() {
        let headers = this.allPreserveCase();

        if (this._headerNames['set-cookie']) {
            delete headers[this._headerNames['set-cookie']];
        }

        return headers;
    }

    replace(headers = {}) {
        this._headerNames = {};

        super.replace(headers);

        if (!this._headers['cache-control']) {
            this.set('Cache-Control', '');
        }
    }

    set(key, values, replace = true) {
        let uniqueKey = key
            .toLowerCase()
            .replace(/_/g, '-');

        if (uniqueKey === 'set-cookie') {
            if (replace) {
                this._cookies = {};
            }

            _
                .each(
                    values,
                    cookie => {
                        this.setCookie(Cookie.fromString(cookie))
                    }
                );

            this._headerNames[uniqueKey] = key;

            return;
        }

        this._headerNames[uniqueKey] = key;
        super.set(key, values, replace);

        if (['cache-control', 'etag', 'last-modified', 'expires'].indexOf(uniqueKey) > -1) {
            let computed = this._computeCacheControlValue();

            this._headers['cache-control'] = [computed];
            this._headerNames['cache-control'] = 'Cache-Control';
            this._computedCacheControl = this._parseCacheControl(computed);
        }
    }

    remove(key) {
        let uniqueKey = key
            .toLowerCase()
            .replace(/_/g, '-');

        delete this._headerNames[uniqueKey];

        if ('set-cookie' === uniqueKey) {
            this._cookies = {};

            return;
        }

        super.remove(key);

        if (uniqueKey === 'cache-control') {
            this._computedCacheControl = {};
        }
    }

    hasCacheControlDirective(key) {
        return !!this._computedCacheControl[key];
    }

    getCacheControlDirective(key) {
        return this._computedCacheControl[key] ?
            this._computedCacheControl[key] :
            null;
    }

    setCookie(cookie) {
        if (!this._cookies[cookie.domain]) {
            this._cookies[cookie.domain] = {};
        }

        if (!this._cookies[cookie.domain][cookie.path]) {
            this._cookies[cookie.domain][cookie.path] = {};
        }

        this._cookies[cookie.domain][cookie.path][cookie.name] = cookie;
        this._headerNames['set-cookie'] = 'Set-Cookie';
    }

    removeCookie(name, path = '/', domain = null) {
        if (null === path) {
            path = '/';
        }

        delete this._cookies[domain][path][name];

        if (_.keys(this._cookies[domain][path]).length == 0) {
            delete this._cookies[domain][path];

            if (_.keys(this._cookies[domain]).length == 0) {
                delete this._cookies[domain];
            }
        }

        if (_.keys(this._cookies).length == 0) {
            delete this._headerNames['set-cookie'];
        }
    }

    getCookies(format = HeaderBag.cookiesFlat) {
        let formats = [HeaderBag.cookiesFlat, HeaderBag.cookiesObject];

        if (formats.indexOf(format) == -1) {
            throw new InvalidArgumentException(
                `Format "${ format }" invalid (${ formats.join(', ') }).`
            )
        }

        if (format === HeaderBag.cookiesObject) {
            return this._cookies;
        }

        let flattenedCookies = [];

        _
            .each(
                this._cookies,
                path => {
                    _
                        .each(
                            path,
                            cookies => {
                                _
                                    .each(
                                        cookies,
                                        cookie => {
                                            flattenedCookies.push(cookie);
                                        }
                                    )

                            }
                        )
                }
            );

        return flattenedCookies;
    }

    clearCookie(name, path = '/', domain = null, secure = false, httpOnly = true) {
        this.setCookie(new Cookie(name, null, 1, path, domain, secure, httpOnly));
    }

    makeDisposition(disposition, filename, filenameFallback = '') {
        if (
            [HeaderBag.dispositionAttachment, HeaderBag.dispositionInline].indexOf(disposition) == -1
        ) {
            throw new InvalidArgumentException(
                `The disposition must be either "${ HeaderBag.dispositionAttachment }" or "${ HeaderBag.dispositionInline }".`
            );
        }

        if ('' == filenameFallback) {
            filenameFallback = filename;
        }

        // filenameFallback is not ASCII.
        if (/^[\x20-\x7e]*$/.test(filenameFallback)) {
            throw new InvalidArgumentException('The filename fallback must only contain ASCII characters.');
        }

        // percent characters aren't safe in fallback.
        if (filenameFallback.indexOf('%') > -1) {
            throw new InvalidArgumentException('The filename fallback cannot contain the "%" character.');
        }

        // path separators aren't allowed in either.
        if (
            filename.indexOf('/') > -1 ||
            filename.indexOf('\\') > -1 ||
            filenameFallback.indexOf('/') > -1 ||
            filenameFallback.indexOf('\\') > -1
        ) {
            throw new InvalidArgumentException('The filename and the fallback cannot contain the "/" and "\\" characters.');
        }

        let result = `${ disposition }; filename="${ filenameFallback.replace(/"/g, '\\"') }"`;

        if (filename !== filenameFallback) {
            result += `; filename*=utf-8''${ encodeURIComponent(filename) }`;
        }

        return result;
    }

    _computeCacheControlValue() {
        if (
            !_.keys(this._cacheControl).length &&
            !this.has('ETag') &&
            !this.has('Last-Modified') &&
            !this.has('Expires')
        ) {
            return 'no-cache, private';
        }

        if (!_.keys(this._cacheControl).length) {
            return 'private, must-revalidate';
        }

        let header = this._getCacheControlHeader();

        if (
            this._cacheControl['public'] ||
            this._cacheControl['private']
        ) {
            return header;
        }

        if (!this._cacheControl['s-maxage']) {
            return `${ header }, private`;
        }

        return header;
    }
}

module.exports = HeaderBag;