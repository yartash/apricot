class UnexpectedValue extends Error {
    constructor(message) {
        super(message);
    }
}

module.exports = UnexpectedValue;