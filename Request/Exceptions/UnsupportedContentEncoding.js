class UnsupportedContentEncoding extends Error {
    constructor(encoding){
        super('Unsupported Content-Encoding: ' + encoding);
    }
}

module.exports = UnsupportedContentEncoding;