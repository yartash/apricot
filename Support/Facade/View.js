class View {
    static get facadeAccessor() {
        return 'view';
    }
}

module.exports = View;