class NotFile extends Error {
    get path () {
        return this._path;
    }
    
    constructor(path) {
        super(`Specified path '${ path }' is not a file}`);
        
        this._path = path;
    }
}

module.exports = NotFile;