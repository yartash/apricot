const zLib = require('zlib');
const iconv = require('iconv-lite');
const bytes = require('bytes');
const _ = require('lodash');

const File = use('Apricot/Request/File');

const UnsupportedContentEncodingExceptions = use('Apricot/Request/Exceptions/UnsupportedContentEncoding');

class Body {
    constructor(request) {
        this._request = request;
        this._stream = this._decodeStream(request.rawRequest);
        this._config = use('Config').get('app.bodyParser');
        this._limit = bytes.parse(this._config.limit);
    }

    * parse() {
        let regex = new RegExp('\\?(.*)$');
        let query = this
            ._urlencodedParser(
                regex.test(this._request.rawUrl) ?
                    this._request.rawUrl.match(regex)[1] :
                    ''
            );
        let body = yield this._getRawBody();
        let files = {};

        if (body != '') {
            let contentType = this._request.header('content-type').toLowerCase();

            if (contentType.startsWith('application/x-www-form-urlencoded')) {
                body = this._urlencodedParser(body);
            } else if (contentType.startsWith('multipart/form-data')) {
                [body, files] = this._formDataParser(body);

                files = _
                    .map(
                        files,
                        (file, name) => {
                            return new File(name, file.name, file.mimeType, file.content);
                        }
                    );
            } else if (contentType.startsWith('application/json')) {
                try {
                    body = JSON.parse(body);
                } catch (error) {

                }
            }

            Object.assign(query, body);
        }

        return [query, files];
    }

    _decodeStream(request) {
        let encoding = (
                request.headers &&
                request.headers['content-encoding']
            ) ||
            'identity';

        switch (encoding) {
            case 'gzip':
            case 'deflate':
                break;
            case 'identity':
                return request;
            default:
                let error = new UnsupportedContentEncodingExceptions(encoding);
                error.status = 415;
                throw error;
        }

        return request.pipe(zLib.Unzip({}));
    }

    _getRawBody(){
        return new Promise((resolve, reject) => {
            this._received = 0;
            let decoder;

            try {
                decoder = iconv.getDecoder(this._config.encoding)
            } catch (error) {
                reject(error);
            }

            this._buffer = decoder ?
                '' :
                [];

            this._stream.on('aborted', this._onAborted.bind(this, reject));
            this._stream.on('data', this._onData.bind(this, reject, decoder));
            this._stream.on('close', this._onClose.bind(this));
            this._stream.on('end', this._onEnd.bind(this, resolve, reject, decoder));
            this._stream.on('error', this._onEnd.bind(this, resolve, reject, decoder));
        });
    }

    _onAborted(reject) {
        reject(400);
    }

    _onData(reject, decoder, data) {
        this._received += data.length;

        if (decoder) {
            this._buffer +=decoder.write(data);
        } else {
            this._buffer.push(data);
        }

        // if (this._limit < this._received) {
        //     reject(413);
        // }
    }

    _onClose() {
        this._buffer = null;

        this._stream.removeListener('aborted', this._onAborted);
        this._stream.on('data', this._onData);
        this._stream.on('close', this._onClose);
        this._stream.on('end', this._onEnd);
        this._stream.on('error', this._onEnd);
    }

    _onEnd(resolve, reject, decoder, error) {
        if (error) {
            reject(error);
        }

        resolve(decoder ? this._buffer + (decoder.end() || '') : Buffer.concat(this._buffer));
    }

    _urlencodedParser(query) {
        let data = {};

        if (!_.isString(query) || query.length === 0) {
            return data;
        }

        _
            .each(
                query.split('&'),
                segment => {
                    segment = segment.replace(/\+/g, '%20').split('=');

                    let key = decodeURIComponent(segment[0]);
                    let value = segment[1] ? decodeURIComponent(segment[1]) : undefined;
                    let match = null;

                    if (match = key.match(/(.+)\[\]/)) {
                        let array = _.get(data, match[1]);
                        let index = array ? array.length : 0;
                        key = key.replace(/(.+)\[\]/g, `$1[${index}]`)
                    }

                    _.set(data, key, value);
                }
            );

        return data;
    }

    _formDataParser(body) {
        let data = {};
        let files = {};

        if (!_.isString(body) || body.length === 0) {
            return data;
        }

        let formKey = this
            ._request
            .header('content-type')
            .replace('multipart/form-data; boundary=', '');

        _
            .each(
                body
                    .replace(new RegExp(`^--${formKey}\r\n`), '')
                    .replace(new RegExp(`--${formKey}--$`), '')
                    .split(`--${formKey}\r\n`),
                part => {
                    if (/^Content-Disposition:\sform-data;\sname=".*";\sfilename=".*"/.test(part)) {
                        let segment = part.match(/^Content-Disposition:\sform-data;\sname="(.*)";\sfilename="(.*)"\r\nContent-Type:\s(.*)\r\n\r\n(.*)\r\n/);

                        if (segment === null) {
                            return true;
                        }

                        let key = segment[1];
                        let file = {
                            name: segment[2],
                            mimeType: segment[3],
                            content: segment[4]
                        };
                        let match;

                        if (match = key.match(/(.+)\[\]/)) {
                            let array = _.get(data, match[1]);
                            let index = array ? array.length : 0;
                            key = key.replace(/(.+)\[\]/g, `$1[${index}]`)
                        }

                        _.set(files, key, file);
                    } else {
                        let segment = part.match(/^Content-Disposition:\sform-data;\sname="(.*)"\r\n\r\n(.*)\r\n/);

                        if (segment === null) {
                            return true;
                        }

                        let key = segment[1];
                        let value = segment[2] != '' ? segment[2] : undefined;
                        let match;

                        if (match = key.match(/(.+)\[\]/)) {
                            let array = _.get(data, match[1]);
                            let index = array ? array.length : 0;
                            key = key.replace(/(.+)\[\]/g, `$1[${index}]`)
                        }

                        _.set(data, key, value);
                    }
                }
            );

        return [data, files];
    }
}

module.exports = Body;