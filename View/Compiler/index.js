const crypto = require('crypto');
const fs = require('fs');
const _ = require('lodash');

const InvalidArgumentException = use('Apricot/Exceptions/InvalidArgument');

class Compiler {
    constructor(cachePath) {
        if (!cachePath) {
            throw new InvalidArgumentException('Please provide a valid cache path.');
        }

        this._cachePath = cachePath;
        this.registerTrait();
    }

    registerTrait() {
        _
            .each(
                this.traits,
                trait => {
                    let traitClass = use(`Apricot/View/Compiler/${ trait }`);
                    _
                        .chain(
                            Object
                                .getOwnPropertyNames(
                                    Object
                                        .getPrototypeOf(new traitClass())
                                )
                        )
                        .filter(
                            method => {
                                return ['constructor'].indexOf(method) == -1;
                            }
                        )
                        .each(
                            method => {
                                this[method] = traitClass.prototype[method];
                            }
                        )
                        .value();
                }
            );
    }

    getCompiledPath(path) {
        let hash = crypto
            .createHash('sha1')
            .update(path)
            .digest('hex');

        return `${ this._cachePath }/${ hash }.html`;
    }

    isExpired(path) {
        let compiled = this.getCompiledPath(path);

        if (!fs.existsSync(compiled)) {
            return true;
        }

        return fs.statSync(path).mtime.getTime() >=
            fs.statSync(compiled).mtime.getTime();
    }
}

module.exports = Compiler;