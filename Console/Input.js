const process = require('process');

class Input {
    get text() {
        return this._text;
    }
    
    get cwd () {
        return process.cwd();
    }
    
    get platform () {
        return process.platform;
    }
    
    set rawArguments (value) {
        this._rawArguments = value;
    }
    
    set rawOptions (value) {
        this._rawOptions = value;
    }
    
    constructor () {
        this._rawArguments = {};
        this._rawOptions = {};

        this._parse();
    }

    changeDirectory(directory) {
		process.chdir(directory);
	}
    
    _parse () {
        let data = process.argv;

        this._nodePath = data.splice(0, 1);
        this._cwf = data.splice(0, 1);
        this._text = data.join(' ');
        this._command = data.splice(0, 1);

        data.forEach((item, index) => {
            if (/^[-]{1,2}/.test(item)) {
                let parts = item.replace(/^([-]{1,2})(.*)$/, '$2')
                    .split('=');
                this._rawOptions[parts[0]] = parts[1] ? parts[1] : true;
            } else {
                this._rawArguments[index] = item;
            }
        });
    }
}

module.exports = Input;