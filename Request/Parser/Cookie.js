const _ = require('lodash');

const Encryption = use('Apricot/Encryption')

class Cookie {
    constructor(cookies) {
        this._cookies = cookies;
        this._encrypter = new Encryption();
    }

    * parse() {
        let result = {};
        _
            .each(
                this
                    ._cookies
                    .replace(/;\s*$/, '')
                    .split(/;\s*/),
                cookie => {
                    let part = cookie.split('=');

                    try {
                        JSON.parse(part[1]);

                        result[part[0]] = part[1] ?
                            this._encrypter.decrypt(part[1]) :
                            undefined;
                    } catch (ex) {

                    }
                }
            );

        return result;
    }
}

module.exports = Cookie;