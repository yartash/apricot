const _ = require('lodash');
const dateFormat = require('dateformat');
const fs = require('fs');
const os = require('os');
const path = require('path');

const ConsoleStream = use('Streams/ConsoleStream');
const FileStream = use('Streams/FileStream');

class Logger {
    static get debug() {
        return 'debug';
    }
    
    static get info() {
        return 'info';
    }

    constructor() {
        this._config = use('Config').get('logger');
    }

    debug(...args) {
        this._appendLog(args, Logger.debug);
    }

    info(message) {
        this._appendLog(message, Logger.info);
    }

    warning(log) {
        this._appendLog(log, Logger.WARNING);
    }

    error(log) {
        this._appendLog(log, Logger.ERROR);
    }

    fatal(log) {
        this._appendLog(log, Logger.FATAL);
    }

    _appendLog(args, level) {
        let streams = this._config['streams']

        for (let i = 0; i < streams.length; i++) {
            let option = streams[i];

            switch ((String.isString(option) ? option : option.type)) {
                case 'file':
                    new FileStream(args, level, option);
                    break;
                case 'console':
                    new ConsoleStream(args, level, option);
                    break;
            }
        }
    }
}

module.exports = Logger;