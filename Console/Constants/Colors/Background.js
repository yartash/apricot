class Background {
    static get system () {
        return '\x1b[49m';
    }

    static get black () {
        return '\x1b[40m';
    }

    static get red () {
        return '\x1b[41m';
    }

    static get green () {
        return '\x1b[42m';
    }

    static get yellow () {
        return '\x1b[43m';
    }

    static get blue () {
        return '\x1b[44m';
    }

    static get magenta () {
        return '\x1b[45m';
    }

    static get cyan () {
        return '\x1b[46m';
    }

    static get lightGray () {
        return '\x1b[47m';
    }

    static get darkGray () {
        return '\x1b[100m';
    }

    static get lightRed () {
        return '\x1b[101m';
    }

    static get lightGreen () {
        return '\x1b[102m';
    }

    static get lightYellow () {
        return '\x1b[103m';
    }

    static get lightBlue () {
        return '\x1b[104m';
    }

    static get lightMagenta () {
        return '\x1b[105m';
    }

    static get lightCyan () {
        return '\x1b[106m';
    }

    static get white () {
        return '\x1b[107m';
    }
}

module.exports = Background;