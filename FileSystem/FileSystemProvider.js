const BaseProvider = use('Apricot/Application/BaseProvider');
const FileSystem = use('Apricot/FileSystem');

class FileSystemProvider extends BaseProvider {
    register() {
        this.app.singleton('files', () => {
            return new FileSystem();
        });
    }
}

module.exports = FileSystemProvider;