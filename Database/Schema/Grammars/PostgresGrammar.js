const Grammar = use('Grammar');

class PostgresGrammar extends Grammar {
	/**
	 * Compile the query to determine if a table exists.
	 *
	 * @return {string}
	 */
	get compileTableExists () {
		return 'select * from information_schema.tables where table_schema = $1 and table_name = $2';
	}

	/**
	 * @public
	 * @return {boolean}
	 */
	get temporary () {
		return this._temporary;
	}

	/**
	 * The possible column modifiers.
	 *
	 * @protected
	 * @return {Array<String>}
	 */
	get modifiers () {
		return ['Increment', 'Nullable', 'Default'];
	}

	constructor () {
		super();

		/**
		 * The commands to be executed outside of create or alter command.
		 *
		 * @protected
		 * @property {Array<String>}
		 */
		this._fluentCommands = ['Comment'];

		/**
		 * The columns available as serials.
		 *
		 * @protected
		 * @property {Array<String>}
		 */
		this._serials = ['bigInteger', 'integer', 'mediumInteger', 'smallInteger', 'tinyInteger'];

		/**
		 * If this Grammar supports schema changes wrapped in a transaction.
		 *
		 * @protected
		 * @property {Boolean}
		 */
		this._transactions = true;
	}

	/**
	 * Compile a create table command.
	 *
	 * @public
	 * @param {Apricot/Database/Schema/Blueprint} blueprint
	 * @param {Apricot/Support/Fluent} command
	 * @return string
	 */
	compileCreate (blueprint, command) {
		let temporary = blueprint._temporary ? 'create temporary' : 'create';
		let table = this.wrapTable(blueprint);
		let columns = this._getColumns(blueprint).join(', ');

		return `${ temporary } table ${ table } (${ columns })`;
	}

    /**
     * Compile a drop table (if exists) command.
     *
     * @public
     * @param {Apricot/Database/Schema/Blueprint} blueprint
     * @param {Apricot/Support/Fluent} command
     * @return {String}
     */
    compileDropIfExists (blueprint, command) {
        return `drop table if exists ${ this.wrapTable(blueprint) }`;
    }

    /**
     * Compile the SQL needed to retrieve all table names.
     *
	 * @public
     * @param {String} schema
     * @return {String}
     */
    compileGetAllTables (schema) {
        return `select tablename from pg_catalog.pg_tables where schemaname = '${ schema }'`;
    }

    /**
     * Compile the SQL needed to drop all tables.
     *
     * @param {String} tables
     * @return {String}
     */
    compileDropAllTables (tables) {
        return `drop table "${ tables.join('", "') }" cascade`;
    }

	/**
	 * Create the column definition for an integer type.
	 *
	 * @protected
	 * @param {Apricot/Support/Fluent} column
	 * @return {String}
	 */
	_typeInteger (column) {
		return column.autoIncrement ? 'serial' : 'integer';
	}

	/**
	 * Create the column definition for a string type.
	 *
	 * @protected
	 * @param {Apricot/Support/Fluent} column
	 * @return {String}
	 */
	_typeString (column) {
		return `varchar(${ column.length })`;
	}

	/**
	 * Create the column definition for a timestamp type.
	 *
	 * @protected
	 * @param {Apricot/Support/Fluent} column
	 * @return {String}
	 */
	_typeTimestamp(column) {
		let columnType = `timestamp(${ column.precision }) without time zone`;

		return column.useCurrent ? `${ columnType } default CURRENT_TIMESTAMP` : columnType;
	}

	/**
	 * Get the SQL for an auto-increment column modifier.
	 *
	 * @protected
	 * @param {Apricot/Database/Schema/Blueprint} blueprint
	 * @param {Apricot/Support/Fluent} column
	 * @return {(String|Null)}
	 */
	_modifyIncrement (blueprint, column) {
		if (this._serials.indexOf(column.type) > -1 && column.autoIncrement) {
			return ' primary key';
		}

		return '';
	}

	/**
	 * Get the SQL for a nullable column modifier.
	 *
	 * @protected
	 * @param {Apricot/Database/Schema/Blueprint} blueprint
	 * @param {Apricot/Support/Fluent} column
	 * @return {(String|Null)}
	 */
	_modifyNullable (blueprint, column) {
		return column.methodNullable ? ' null' : ' not null';
	}

	/**
	 * Get the SQL for a default column modifier.
	 *
	 * @protected
	 * @param {Apricot/Database/Schema/Blueprint} blueprint
	 * @param {Apricot/Support/Fluent} column
	 * @return {(String|Null)}
	 */
	_modifyDefault (blueprint, column) {
		if (column.default !== null) {
			return ` default ${ this._getDefaultValue(column.default) }`;
		}

		return '';
	}
}

module.exports = PostgresGrammar;