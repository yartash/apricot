class CompilesEchos {
    get echoMethods() {
        return [
            '_compileRawEchos',
            '_compileEscapedEchos',
            '_compileRegularEchos'
        ];
    }

    * _compileEchos(content) {
        let methods = this.echoMethods;

        for (let i = 0; i < methods.length; i++) {
            content = this[methods[i]](content);
        }

        return content;
    }

    _compileRawEchos(content) {
        let pattern = `(@)?${ this.rawTags[0] }\\s*([\s\S]+?)\\s*${ this.rawTags[1] }(\r?\n)?`;

        return content.replace(new RegExp(pattern, 'gm'), (...match) => {
            if (match[1]) {
                return  match[0].substr(1);
            } else {
                let whitespace = match[3] ? match[3] + match[3] : '';

                return `<%= ${ this._compileEchoDefaults(match[2]) } %>${ whitespace }`;
            }
        });
    }

    _compileRegularEchos(content) {
        let pattern = `(@)?${ this.contentTags[0] }\\s*([\\s\\S]+?)\\s*${ this.contentTags[1] }(\r?\n)?`;

        return content.replace(new RegExp(pattern, 'gm'), (...match) => {
            if (match[1]) {
                return  match[0].substr(1);
            } else {
                let whitespace = match[3] ? match[3] + match[3] : '';

                return `<%= ${ this.echoFormat.replace('%s', this._compileEchoDefaults(match[2])) } %>${ whitespace }`;
            }
        });
    }

    _compileEscapedEchos(content) {
        let pattern = `(@)?${ this.escapedTags[0] }\\s*([\\s\\S]+?)\\s*${ this.escapedTags[1] }(\r?\n)?`;

        return content.replace(new RegExp(pattern, 'gm'), (...match) => {
            if (match[1]) {
                return  match[0].substr(1);
            } else {
                let whitespace = match[3] ? match[3] + match[3] : '';

                return `<%= _.escape(${ this._compileEchoDefaults(match[2]) }) %>${ whitespace }`;
            }
        });
    }

    _compileEchoDefaults(content) {
        return content.replace(/^(?=\$)([\s\S]+?)(?:\s+or\s+)([\s\S]+?)$/, match => {
            return `typeof(${ match[1] }) !== 'undefined' ? ${ match[1]} : ${ match[2] }`;
        })
    }
}

module.exports = CompilesEchos;