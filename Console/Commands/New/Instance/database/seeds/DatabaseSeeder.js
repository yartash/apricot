const Seeder = use('Apricot/Database/Seeder');

class DatabaseSeeder extends Seeder {
    /**
     * Seed the application's database.
     *
     * @public
     */
    * run () {
        // $this->call(UsersTableSeeder::class);
    }
}

module.exports = DatabaseSeeder;