const LodashEngin = use('Apricot/View/Engine/Lodash')

class Compiler extends LodashEngin {
    constructor(compiler) {
        super();

        this._compiler = compiler;
        this._lastCompiled = [];
    }

    * get(path, data= {}) {
        this._lastCompiled.push(path);

        if (this._compiler.isExpired(path)) {
            yield * this._compiler.compile(path);
        }

        let compiled = this._compiler.getCompiledPath(path);
        let result = yield this._evaluatePath(compiled, data);

        this._lastCompiled.pop();

        return result;
    }
}

module.exports = Compiler;