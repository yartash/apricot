const Command = use('Apricot/Console/Command');
const Creator = use('Creator');

class Migration extends Command {
    get signature () {
        return 'make:migration {name}';
    }

    get optionsDefinition () {
        return {
            create: {
                description: 'The table to be created.',
                short: 'c',
                default : false
            },
            table: {
                description: 'The table to migrate.',
                short: 't'
            },
            path: {
                description: 'The location where the migration file should be created.',
                short: 'p'
            }
        };
    }

    get name () {
        return 'Make:migration';
    }

    get description () {
        return 'Create new migration file.';
    }

    /**
     * @protected
     * @return {string}
     */
    get migretionPath () {
        let path = this.option('path');

        return path ? basePath(path) : databasePath('migrations');
    }

    constructor () {
        super();
        this._creator = new Creator();
    }

    * handler () {
		let name = this.argument('name').trim();
		let create = this.option('create');
		let table = this.option('table');

		if (!table) {
			let mathces = name.match(/^create_(\w+)_table$/)

			if (mathces) {
				create = true;
				table = mathces[1];
			}
		}

		yield * this._writeMigration(name, table, create);
    }

	/**
	 * @protected
	 * @param {string} name
	 * @param {string} table
	 * @param {boolean} create
	 */
	* _writeMigration (name, table, create) {
        let file = yield * this._creator
            .create(name, this.migretionPath, table, create);

        this.info(`Created Migration: ${ file }`);
	}
}

module.exports = Migration;