class Fluent {
	get methods () {
		return this._methods;
	}

    get proxy() {
        return {
            get: (target, key) => {
				if (target.methods.indexOf(key) > -1) {
					return (...args) => {
						return target.call.apply(target, [key].concat(args));
					}
				} else if (Function.isFunction(target[key])) {
					return (...args) => {
						return target[key].apply(target, args);
					}
				} else {
					return target.get(key);
				}
            },
			has: (target, key) => {
				if (Function.isFunction(target[key])) {
					return target[key].apply(target, args);
				} else {
					return target.offsetExists(key);
				}
			}
        }
    }
    
    /**
     * Create a new fluent container instance.
     *
     * @param {Object} attributes
     */
    constructor (attributes = {}, methods = []) {
        /**
         * All of the attributes set on the container.
         *
         * @protected
         * @type {Object}
         */
        this._attributres = {};
		/**
		 *
		 * @protected
		 * @type {Array<String>}
		 */
		this._methods = methods;

        for (let key in attributes) {
            this._attributres[key] = attributes[key]
        }

        this._proxy = new Proxy(
            this,
            this.proxy
        );

        return this._proxy;
    }

	/**
	 * Get an attribute from the container.
	 *
	 * @public
	 * @param {String} key
	 * @param {*} default
	 * @return {*}
	 */
	get (key, defaultValue = null) {
		if (this._attributres[key] !== undefined) {
			return this._attributres[key];
		}

		return value(defaultValue);
	}

	/**
	 * Determine if the given offset exists.
	 *
	 * @public
	 * @param {String} offset
	 * @return {Boolean}
	 */
	offsetExists (offset) {
		return offset in this._attributres;
	}

	call (method, ...args) {
		this._attributres[`method${ method.ucfirst() }`] = args.length ? args.first() : true;

		return this._proxy;
	}
}

module.exports = Fluent;