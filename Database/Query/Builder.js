const Collection = use('Apricot/Support/Collection');
const Expression = use('Expression');

const InvalidArgumentException = use('Apricot/Exceptions/InvalidArgument');

class Builder {
    get driver() {
        return this._connection.driver;
    }

    get columns() {
        return this._columns;
    }

    set columns(value) {
        this._columns = value;
    }

    get orders () {
        return this._orders;
    }

    /**
     * @typedef QueryBuilderWhere
     * @type {Object}
     * @property {String} type
     * @property {String} column
     * @property {String} operator
     * @property {Any} value
     * @property {String} boolean
     *
     * @return {Array<QueryBuilderWhere>}
     */
    get wheres () {
    	return this._wheres;
	}

	/**
	 * Get the current query value bindings in a flattened array.
	 *
	 * @public
	 * @return {Array<Any>}
	 */
    get bindings () {
        return Array.flatten(Object.values(this._bindings));
    }

    get aggregates () {
        return this._aggregates;
    }

    get isDistinct () {
        return this._isDistinct;
    }
    
    constructor(connection) {
        this._bindings = {
            select: [],
            join: [],
            where: [],
            having: [],
            order: [],
            union: []
        };
        this.columns = null;
        this._connection = connection;
        this._grammar = connection.queryGrammar;
        this._processor = connection.processor

		/**
		 * The orderings for the query.
		 *
		 * @protected
		 * @property {Array<Object>}
		 */
		this._orders = [];
        /**
         * The query union statements.
         *
         * @protected
         * @property {Array<Object>}
         */
        this._unions = [];
        /**
         * The orderings for the union query.
         *
         * @protected
         * @property {Array<Object>}
         */
        this._unionOrders = [];
        /**
         * An aggregate function and column to be run.
         *
         * @protected
         * @property {Object}
         */
        this._aggregates = {};
        /**
         * The groupings for the query.
         *
         * @protected
         * @property {Object}
         */
        this._groups = {};

        /**
         * Indicates if the query returns distinct results.
         *
         * @protected
         * @property {Boolean}
         */
        this._isDistinct = false;

		/**
		 * The where constraints for the query.
		 *
		 * @protected
		 * @property {Array<Any>}
		 */
		this._wheres = [];

        /**
         * All of the available clause operators.
         *
         * @protected
         * @property {Array<String>}
         */
        this._operators = [
            '=', '<', '>', '<=', '>=', '<>', '!=', '<=>',
            'like', 'like binary', 'not like', 'ilike',
            '&', '|', '^', '<<', '>>',
            'rlike', 'regexp', 'not regexp',
            '~', '~*', '!~', '!~*', 'similar to',
            'not similar to', 'not ilike', '~~*', '!~~*',
        ];

        /**
         * The maximum number of union records to return.
         *
         * @protected
         * @property {Int}
         */
        this._unionLimit;

        /**
         * The maximum number of records to return.
         *
         * @protected
         * @property {Int}
         */
        this._limit = undefined;

		/**
		 * The table joins for the query.
		 *
		 * @protected
		 * @property {Array<Object>}
		 */
		this._joins = [];

		/**
		 * The having constraints for the query.
		 *
		 * @protected
		 * @property {Array<Object>}
		 */
		this._havings = [];

		/**
		 * The number of records to skip.
		 *
		 * @protected
		 * @property {Int}
		 */
		this._offset = undefined;

		/**
		 * Indicates whether row locking is being used.
		 *
		 * @protected
		 * @property {String|Boolean}
		 */
		this._lock = undefined;
    }

    getAggregates () {
    	return this._aggregates;
	}

    getColumns () {
    	return this._columns;
	}

    getFrom () {
    	return this._from;
	}

    getJoins () {
    	return this._joins;
	}

    getWheres () {
    	return this._wheres;
	}

    getGroups () {
    	return this._groups;
	}

    getHavings () {
    	return this._havings;
	}

    getOrders () {
    	return this._orders;
	}

    getLimit () {
    	return this._limit;
	}

    getOffset () {
    	return this._offset;
	}

    getUnions () {
    	return this._unions;
	}

    getLock () {
    	return this._lock;
	}

    from(from) {
        if (!from) {
            return this._from;
        }

        this._from = from;

        return this;
    }

	/**
	 * Add a basic where clause to the query.
	 *
	 * @public
	 * @param {String|Object|Function} column
	 * @param {Any} operator
	 * @param {Any} value
	 * @param {String} boolean
	 * @return {Apricot/Database/Query/Builder}
	 */
	where (column, operator = null, value = null, boolean = 'and') {
		// If the column is an array, we will assume it is an array of key-value pairs
		// and can add them each as a where clause. We will maintain the boolean we
		// received when the method was called and pass it into the nested where.
		if (Object.isObject(column)) {
			return this._addArrayOfWheres(column, boolean);
		}

		// Here we will make some assumptions about the operator. If only 2 values are
		// passed to the method, we will assume that the operator is an equals sign
		// and keep going. Otherwise, we'll require the operator to be passed in.
        let valueAndOperator = this.prepareValueAndOperator(value, operator, arguments.length == 2);
		value = valueAndOperator.value;
		operator = valueAndOperator.operator;

		// If the columns is actually a Closure instance, we will assume the developer
		// wants to begin a nested where statement which is wrapped in parenthesis.
		// We'll add that Closure to the query then return back out immediately.
        if (Function.isFunction(column)) {
            return this.whereNested(column, boolean);
        }

		// If the given operator is not found in the list of valid operators we will
		// assume that the developer is just short-cutting the '=' operators and
		// we will set the operators to '=' and set the values appropriately.
        if (this._invalidOperator(operator)) {
            value = operator;
            operator = '=';
        }

		// If the value is a Closure, it means the developer is performing an entire
		// sub-select within the query and we will need to compile the sub-select
		// within the where clause to get the appropriate query record results.
        if (Function.isFunction(value)) {
            return this._whereSub(column, operator, value, boolean);
        }

		// If the value is "null", we will just assume the developer wants to add a
		// where null clause to the query. So, we will allow a short-cut here to
		// that method for convenience so the developer doesn't have to check.
        if (value === null) {
            return this.whereNull(column, boolean, operator !== '=') ;
        }

		// If the column is making a JSON reference we'll check to see if the value
		// is a boolean. If it is, we'll add the raw boolean string as an actual
		// value to the query to ensure this is properly handled by the query.
        if (column.contains('->') && Boolean.isBoolean(value)) {
            value = new Expression(value ? 'true' : 'false');
        }

		// Now that we are working with just a simple query we can put the elements
		// in our array and add the query binding to our array of bindings that
		// will be bound to each SQL statements when it is finally executed.
		let type = 'Basic';

		this._wheres.push({type, column, operator, value, boolean});

		if (!(value instanceof Expression)) {
		    this.addBinding(value, 'where')
        }

		return this;
	}

    /**
     * Add a "where null" clause to the query.
     *
     * @public
     * @param {String} column
     * @param {String} boolean
     * @param {Boolean} not
     * @return {Apricot/Database/Query/Builder}
     */
    whereNull (column, boolean = 'and', not = false) {
        let type = not ? 'NotNull' : 'Null';

        this._wheres.push({type, column, boolean});

        return this;
    }

    /**
     * Prepare the value and operator for a where clause.
     *
     * @typedef ValueAndOperator
     * @type {Object}
     * @property {Any} value
     * @property {String} operator
     *
     * @public
     * @param {String} value
     * @param {String} operator
     * @param {Boolean} useDefault
     * @return {ValueAndOperator}
     *
     * @throws {Apricot/Exceptions/InvalidArgumentException}
     */
    prepareValueAndOperator (value, operator, useDefault = false) {
        if (useDefault) {
            return {
                value: operator,
                operator: '='
            };
        } else if (this._invalidOperatorAndValue(operator, value)) {
            throw new InvalidArgumentException('Illegal operator and value combination.');
        }

        return {value, operator};
    }

	/**
	 * Add a nested where statement to the query.
	 *
	 * @public
	 * @param {Function} callback
	 * @param {String} boolean
	 * @return {Apricot/Database/Query/Builder}
	 */
	whereNested (callback, boolean = 'and') {
		let query = this.forNestedWhere();

		callback(query);

		return this.addNestedWhereQuery(query, boolean);
	}

	/**
	 * Create a new query instance for nested where condition.
	 *
	 * @public
	 * @return {Apricot/Database/Query/Builder}
	 */
	forNestedWhere () {
		return this.newQuery().from(this._from);
	}

	/**
	 * Get a new instance of the query builder.
	 *
	 * @public
	 * @return {Apricot/Database/Query/Builder}
	 */
	newQuery ()	{
		return new Builder(this._connection, this._grammar, this._processor);
	}

	/**
	 * Add another query builder as a nested where to the query builder.
	 *
	 * @public
	 * @param {Apricot/Database/Query/Builder} query
	 * @param {String} boolean
	 * @return {Apricot/Database/Query/Builder}
	 */
	addNestedWhereQuery (query, boolean = 'and') {
		if (query._wheres.length) {
			let type = 'Nested';

			this._wheres.push({type, query, boolean});

			this.addBinding(query.bindings, 'where');
		}

		return this;
	}

	/**
	 * Add a binding to the query.
	 *
	 * @public
	 * @param {Any} value
	 * @param {String} type
	 * @return {Apricot/Database/Query/Builder}
	 *
	 * @throws {Apricot/Exceptions/InvalidArgumentException}
	 */
	addBinding (value, type = 'where') {
		if (!(type in this._bindings)) {
			throw new InvalidArgumentException(`Invalid binding type: ${ type }.`);
		}

		if (Array.isArray(value)) {
			this._bindings[type] = this._bindings[type].concat(value);
		} else {
			this._bindings[type].push(value);
		}

		return this;
	}

	/**
	 * Add an "order by" clause to the query.
	 *
	 * @public
	 * @param {String} column
	 * @param {String} direction
	 * @return {Apricot/Database/Query/Builder}
	 */
	orderBy (column, direction = 'asc') {
	    this[`${ this._unions.length ? '_unionOrders' : '_orders' }`]
            .push({
                column: column,
                direction: direction.toLowerCase() == 'asc' ? 'asc' : 'desc'
            });

		return this;
	}

    /**
     * Alias to set the "limit" value of the query.
     *
     * @public
     * @param {Int} value
     * @return {Apricot/Database/Query/Builder}
     */
    take (value) {
        return this.limit(value);
    }

    /**
     * Set the "limit" value of the query.
     *
     * @public
     * @param {Int} value
     * @return {Apricot/Database/Query/Builder}
     */
    limit (value) {
    	value = parseInt(value);

        if (value >= 0) {
            this[`${ this._unions.length ? '_unionLimit' : '_limit' }`] = value;
        }

        return this;
    }

    /**
     * Get an array with the values of a given column.
     *
     * @public
     * @param {String} column
     * @param {(String|Null)} key
     * @return {Apricot/Support/Collection}
     */
    pluck (column, key = null) {
        return this.get(key === null ? [column] : [column, key])
            .then(result => {
                return result.pluck(
                    this._stripTableForPluck(column),
                    this._stripTableForPluck(key)
                );
            });
    }

    /**
     * Retrieve the maximum value of a given column.
     *
     * @public
     * @param {String} column
     * @return {Any}
     */
    max (column) {
        return this.aggregate('max', [column]);
    }

    /**
     * Execute an aggregate function on the database.
     *
     * @public
     * @param {String} functionName
     * @param {Array<String>} columns
     * @return {Any}
     */
    aggregate (functionName, columns = ['*']) {
        return new Promise(resolve => {
            this.cloneWithout(['columns'])
                .cloneWithoutBindings(['select'])
                .setAggregate(functionName, columns)
                .get(columns)
                .then(result => {
                    result.all()
                        .run()
                        .then(row => {
                            resolve(
                                row.length && row[0].aggregate !== null ?
                                    parseInt(row[0].aggregate) :
                                    0
                            );
                        });
                });
        });
    }

    /**
     * Clone the query without the given properties.
     *
     * @public
     * @param {Array<String>} properties
     * @return {Apricot/Databse/Query/Builder}
     */
    cloneWithout (properties) {
        let clone = Object.assign(this);

        properties.forEach(property => {
            clone[property] = null;
        });

        return clone;
    }

    /**
     * Clone the query without the given bindings.
     *
     * @public
     * @param {Array<String>} except
     * @return {Apricot/Databse/Query/Builder}
     */
    cloneWithoutBindings (except) {
        let clone = Object.assign(this);

        except.forEach(type => {
            clone._bindings[type] = [];
        });

        return clone;
    }

    /**
     * Set the aggregate property without running the query.
     *
     * @public
     * @param {String} functionName
     * @param {Array<String>} columns
     * @return {Apricot/Databse/Query/Builder}
     */
    setAggregate (functionName, columns) {
        this._aggregates = {functionName, columns};

        if (Object.keys(this._groups).length == 0) {
            this._orders = [];

            this._bindings.order = [];
        }

        return this;
    }

    with() {
        return this;
    }

    withCount() {
        return this;
    }

    get(columns = ['*']) {
        let original = this.columns;

        if (original === null) {
            this.columns = columns;
        }

        return this._processor
                .processSelect(this, this._runSelect())
                .then(results => {
                    this.columns = original;

                    return collect(results.rows);
                });
    }

    toSql() {
        return this._grammar.compileSelect(this);
    }

	/**
	 * Insert a new record into the database.
	 *
	 * @public
	 * @param {Array<Object>|Object} values
	 * @return {Boolean}
	 */
	insert (values)	{
		return new Promise(resolve => {
			// Since every insert gets treated like a batch insert, we will make sure the
			// bindings are structured in a way that is convenient when building these
			// inserts statements by verifying these elements are actually an array.
			if (
				(Array.isArray(values) && values.length == 0) ||
				Object.keys(values).length == 0
			) {
				resolve(true);
			}

			if (Array.isArray(values)) {
                // foreach ($values as $key => $value) {
                //     ksort($value);
                //
                //     $values[$key] = $value;
                // }
			} else {
                // Here, we will sort the insert keys for every record so that each insert is
                // in the same order for the record. We need to make sure this is the case
                // so there are not any errors or problems when inserting these records.
				values = [values];
			}

            // Finally, we will run this query against the database connection and return
            // the results. We will need to also flatten these bindings before running
            // the query so they are all in one huge, flattened array for execution.
            this._connection.insert(
                this._grammar.compileInsert(this, values),
                this._cleanBindings(Array.flatten(values, 1))
            )
                .then(result => {
                    resolve(result.rowCount);
                });
		});
	}

    /**
     * Delete a record from the database.
     *
     * @public
     * @param {Any} id
     * @return {Int}
     */
    delete (id = null)  {
		// If an ID is passed to the method, we will set the where clause to check the
		// ID to let developers to simply and quickly remove a single row from this
		// database without manually specifying the "where" clauses on the query.
		if (id !== null) {
			this.where(`${ this._from }.id`, '=', id);
		}

		return this._connection.delete(
			this._grammar.compileDelete(this),
			this._cleanBindings(this.bindings)
		);
    }

	/**
	 * Remove all of the expressions from a list of bindings.
	 *
	 * @protected
	 * @param {Array} bindings
	 * @return {Array}
	 */
	_cleanBindings (bindings) {
		return bindings.filter(binding => {
			return !(binding instanceof Expression);
		});
	}

    _runSelect() {
        return this._connection.select(this.toSql(), this.bindings);
    }

    /**
     * Strip off the table name or alias from a column identifier.
     *
     * @protected
     * @param {String} column
     * @return {(String|Null)}
     */
    _stripTableForPluck (column) {
        return column === null ? column : column.split(/~\.| ~/).last();
    }

	/**
	 * Add an array of where clauses to the query.
	 *
	 * @protected
	 * @param {Object} column
	 * @param {String} boolean
	 * @param {String} method
	 * @return {Apricot/Database/Query/Builder}
	 */
	_addArrayOfWheres (column, boolean, method = 'where') {
		return this.whereNested(
			(query) => {
				for (let key in column) {
					if (Number.isNumber(key) && Array.isArray(column[key])) {
						query[method].apply(query, column[key]);
					} else {
						query[method](key, '=', column[key], boolean);
					}
				}
			},
			boolean
		);
	}

    /**
     * Determine if the given operator and value combination is legal.
     *
     * Prevents using Null values with invalid operators.
     *
     * @param {String} operator
     * @param {Any} value
     * @return {Boolean}
     */
    _invalidOperatorAndValue (operator, value) {
        return value === null && this._operatosrs.indexOf(operator) > -1 && ['=', '<>', '!='].indexOf(operator) == -1;
    }

    /**
     * Determine if the given operator is supported.
     *
     * @protected
     * @param {String} operator
     * @return {Boolean}
     */
    _invalidOperator (operator) {
        return this._operators.indexOf(operator.toLowerCase()) == -1 &&
            this._grammar.operators.indexOf(operator.toLowerCase()) == -1;
    }

    /**
     * Add a full sub-select to the query.
     *
     * @protected
     * @param {String} column
     * @param {String} operator
     * @param {Function} callback
     * @param {String} boolean
     * @return {Apricot/Database/Query/Builder}
     */
    _whereSub (column, operator, callback, boolean) {
        let type = 'Sub';
        let query = this._forSubQuery();

        // Once we have the query instance we can simply execute it so it can add all
        // of the sub-select's conditions to itself, and then we can cache it off
        // in the array of where clauses for the "main" parent query instance.
        callback(query);

        this._wheres.push({type, column, operator, query, boolean});

        this.addBinding(query.bindings, 'where');

        return this;
    }

    /**
     * Create a new query instance for a sub-query.
     *
     * @protected
     * @return {Apricot/Database/Query/Builder}
     */
    _forSubQuery () {
        return this.newQuery();
    }
}

module.exports = Builder;