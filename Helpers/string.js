Object.defineProperty(
    String.prototype,
    'ucfirst',
    {
        value: function() {
            return this.charAt(0).toUpperCase() + this.slice(1);
        },
        configurable: false,
        writable: false,
        enumerable: false
    }
);

Object.defineProperty(
	String.prototype,
	'lcfirst',
	{
		value: function() {
			return this.charAt(0).toLowerCase() + this.slice(1);
		},
		configurable: false,
		writable: false,
		enumerable: false
	}
);

Object.defineProperty(
    String.prototype,
    'contains',
    {
        value: function(needles) {
            if (!Array.isArray(needles)) {
                needles = [needles];
            }

            for (let i = 0; i < needles.length; i++) {
                if (needles[i] !== '' && this.indexOf(needles[i]) > -1) {
                    return true;
                }
            }

            return false;
        },
        configurable: false,
        writable: false,
        enumerable: false
    }
);

Object.defineProperty(
    String.prototype,
    'snakeCase',
    {
        value: function() {
            let index = 0;
            
            return this.replace(/[A-Z]/g, item => {
                return index++ > 0 ? `_${ item.toLowerCase() }` : item.toLowerCase();
            })
        },
        configurable: false,
        writable: false,
        enumerable: false
    }
);

Object.defineProperty(
    String.prototype,
    'trimLeft',
    {
        value: function(charlist) {
            if (charlist === undefined) {
                charlist = '\s';
            }

            return this.replace(new RegExp(`^[${ charlist }]+`), '');
        },
        configurable: false,
        writable: false,
        enumerable: false
    }
);

Object.defineProperty(
    String.prototype,
    'trimRigth',
    {
        value: function(charlist) {
            if (charlist === undefined) {
                charlist = '\s';
            }

            return this.replace(new RegExp(`[${ charlist }]+$`), '');
        },
        configurable: false,
        writable: false,
        enumerable: false
    }
);

Object.defineProperty(
    String,
    'isString',
    {
        value: value => {
            return Object.prototype.toString.call(value) === '[object String]';
        },
        configurable: false,
        writable: false,
        enumerable: false
    }
);

Object.defineProperty(
    String.prototype,
    'studly',
    {
        value: function() {
            return this.split(/[-_]/g)
                .map(item => {
                    return item.ucfirst();
                })
                .join('');
        },
        configurable: false,
        writable: false,
        enumerable: false
    }
);
