class TableStyle {
    constructor () {
        /**
         * @private
         * @property {String}
         */
        this._paddingChar = ' ';

        /**
         * @private
         * @property {String}
         */
        this._horizontalBorderChar = '-';

        /**
         * @private
         * @property {String}
         */
        this._verticalBorderChar = '|';

        /**
         * @private
         * @property {String}
         */
        this._crossingChar = '+';

        /**
         * @private
         * @property {Object}
         */
        this._cellHeaderFormat = {style: 'info', message: '%s'};

        /**
         * @private
         * @property {String}
         */
        this._cellRowFormat = '%s';

        /**
         * @private
         * @property {String}
         */
        this._cellRowContentFormat = ' %s ';

        /**
         * @private
         * @property {String}
         */
        this._borderFormat = '%s';
        // this._padType = STR_PAD_RIGHT;
    }

    /**
     * Sets horizontal border character.
     *
     * @public
     * @param {String} horizontalBorderChar
     * @return {TableStyle}
     */
    setHorizontalBorderChar (horizontalBorderChar) {
        this._horizontalBorderChar = horizontalBorderChar;

        return this;
    }
}

module.exports = TableStyle;