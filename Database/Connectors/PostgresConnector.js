const { Client } = require('pg');

const Connector = use('Connector');

class PostgresConnector extends Connector {
    get dsn() {
        let config = this._config;

        return `postgresql://${ config.username }:${ config.password }@${ config.host }:${ config.port }/${ config.database }`;
    }

    get options() {
        return !this._config.options ? this._config.options : {};
    }

    connect(config) {
        return new Promise((resolve, reject) => {
            const client = new Client({
                connectionString: this.dsn,
            });
            
            client.disconnect = this.disconnect;

            client.connect()
                .then(
                    success => {
                        resolve(client);
                    },
                    error => {
                        reject(error);
                    }
                );
        });
    }

    disconnect() {

    }
}

// PostgresConnector.

module.exports = PostgresConnector;