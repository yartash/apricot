const Command = use('Apricot/Console/Command');

const NameMissingException = use('Exceptions/NameMissing');
const FolderAlreadyExistsException = use('Exceptions/FolderAlreadyExists');

const File = use('File');

class New extends Command {
    get signature () {
        return 'new {name}';
    }

    get name () {
        return 'New';
    }

    get description () {
        return 'Create new instance from apricot-js';
    }

    get appPath () {
    	return this._appPath;
	}

	set appPath (path) {
    	this._appPath = path;
	}

    * handler () {
        yield * this._copyInstance();
        yield * this._createPakageJson();
        yield * this._installPakage();
	}

    * _copyInstance () {
		let name = this.argument('name');

		if (!name) {
			throw new NameMissingException(this);
		}

		this.appPath = File.resolve(this.input.cwd, name);

		if (yield File.exists(this.appPath)) {
			throw new FolderAlreadyExistsException(name, this);
		}

		this.info('Copying instance files.');
		
		yield * File.copy(File.join(__dirname, 'Instance'), this.appPath);

		this.info('Copied instance files.');
		this.info('Creating .env file.');

        yield * File.copy(
        	File.join(this.appPath, '.env.example'),
        	File.join(this.appPath, '.env') 
		);

        this.info('Created .env file.');
	}

    * _createPakageJson() {
		let name = this.argument('name');
		this.info('Creating package.json file.');
		
		yield * File.writeJson(
			File.join(this.appPath, 'package.json'),
			{
				name: name,
				version: '0.0.1',
				description: name,
				repository: {
					type: 'git',
					url: ''
				},
				license: 'MIT',
				dependencies: {
					'apricot-js': application.frameworkVersion
				},
				devDependencies: {}
			}
		);

		this.info('Created package.json file.')
    }

    * _installPakage() {
		const spawn = require('child_process').spawn;
		let npm = this.input.platform == 'win32' ? 'npm.cmd' : 'npm';

		return yield new Promise((resolve, reject) => {
			this.input.changeDirectory(this.appPath);

			let install = spawn(npm, ['i']);

			install.stderr.on('data', data => {
				this.info(data.toString());
			});

			install.on('close', () => {
				this.info('Finish');
				resolve();
			});
		});
	}
}

module.exports = New;