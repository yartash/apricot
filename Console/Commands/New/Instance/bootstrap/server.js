const Application = require('apricot-js');

process.on('unhandledRejection', error => {
    console.log('unhandledRejection', error.message);
});

module.exports = function (callback) {
    new Promise(
        resolve => {
            Application.instance;

            const Env = use('Env');

            use('Config');
            use('Server').listen(Env.get('HOST'), Env.get('PORT'));

            resolve();
        }
    )
        .then(
            () => {
                callback();
            }
        );
};