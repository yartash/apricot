const BaseRouteProvider = use('Apricot/Route/RouteProvider');

class RouteProvider extends BaseRouteProvider {
    boot() {
        const Route = use('Route');

        Route.group(
            {
                
            },
            () => {
				return Route.use('App/Routes/Web');
            }
        );

        Route.group(
            {
                prefix: 'api',
                as: 'api'
            },
            () => {
				return Route.use('App/Routes/Api');
            }
        );
    }
}

module.exports = RouteProvider;