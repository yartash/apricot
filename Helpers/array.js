Object.defineProperty(
    Array,
    'flatten',
    {
        value: (array, depth = Infinity) => {
			let result = [];

			array.forEach(item => {
				switch (true) {
					case !(Array.isArray(item) || Object.isObject(item)) :
						result.push(item);
						break;
					case depth === 1:
						result = result.concat(Array.isArray(item) ? item : Object.values(item));
						break;
					default:
						result = result.concat(Array.flatten(item, depth - 1));
						break;
				}
			});

			return result;
        },
        configurable: false,
        writable: false,
        enumerable: false
    }
);

Object.defineProperty(
    Array.prototype,
    'random',
    {
        value: function () {
            return this[Math.floor(Math.random() * (this.length - 1))];
        },
        configurable: false,
        writable: false,
        enumerable: false
    }
);

Object.defineProperty(
	Array.prototype,
	'first',
	{
		value: function () {
			return this.length > 0 ? this[0] : undefined;
		},
		configurable: false,
		writable: false,
		enumerable: false
	}
);

Object.defineProperty(
	Array.prototype,
	'last',
	{
		value: function () {
			return this.length > 0 ? this[this.length - 1] : undefined;
		},
		configurable: false,
		writable: false,
		enumerable: false
	}
);