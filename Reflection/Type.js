class Type {
	/**
	 * @public
	 * @return {Array<string>}
	 */
	get names () {
		return this._data.names || [];
	}
	
	constructor (data = {}) {
		this._data = data
	}
}

module.exports = Type;