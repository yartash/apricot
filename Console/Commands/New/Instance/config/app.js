const Env = use('Env');

module.exports = {
    env: env('APP_ENV', 'development'),
    key: env('APP_KEY'),
    server: {
        http: true,
        https: false,

        ssl: {
            key: '',
            cert: ''
        }
    },
    bodyParser: {
        encoding: 'utf-8',
        limit: '1mb'
    },
    response: {
        contentEncoding: false,
        algorithm: 'gzip',
        cache: {
            enable: true,
            age: '30 day'
        }
    }
};
