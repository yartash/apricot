class Peach {
    static get facadeAccessor() {
        return 'peach';
    }
}

module.exports = Peach;