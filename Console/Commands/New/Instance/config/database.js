const Env = use('Env');

module.exports = {
    default: env('DB_CONNECTION', 'mongo'),
    loggingQueries: true,
    connections: {
        sqlite: {
            driver: 'sqlite',
            database: env('DB_DATABASE', databasePath('database.sqlite')),
            prefix: ''
        },
        mysql: {
            driver: 'mysql',
            host: env('DB_HOST', '127.0.0.1'),
            port: env('DB_PORT', 3306),
            database: env('DB_DATABASE', 'forge'),
            username: env('DB_USERNAME', 'forge'),
            password: env('DB_PASSWORD', ''),
            unix_socket: env('DB_SOCKET', ''),
            charset: 'utf8mb4',
            collation: 'utf8mb4_unicode_ci',
            prefix: '',
            strict: true,
            engine: null
        },
        pgsql: {
            driver: 'pgsql',
            host: env('DB_HOST', '127.0.0.1'),
            port: env('DB_PORT', 5432),
            database: env('DB_DATABASE', 'apricot'),
            username: env('DB_USERNAME', 'apricot'),
            password: env('DB_PASSWORD', ''),
            charset: 'utf8',
            prefix: '',
            schema: 'public',
            sslmode: 'prefer'
        },
        sqlsrv: {
            driver: 'sqlsrv',
            host: env('DB_HOST', 'localhost'),
            port: env('DB_PORT', 1433),
            database: env('DB_DATABASE', 'forge'),
            username: env('DB_USERNAME', 'forge'),
            password: env('DB_PASSWORD', ''),
            charset: 'utf8',
            prefix: ''
        },
        mongo: {
            driver: 'mongo',
            host: env('DB_HOST', 'localhost'),
            port: env('DB_PORT', 27017),
            database: env('DB_DATABASE'),
            username: env('DB_USERNAME'),
            password: env('DB_PASSWORD'),
            options: {
            //     database: 'admin'
            }
        },
    }
};
