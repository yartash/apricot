class RouteNotFound extends Error {
    get request() {
        return this._request;
    }

    get statusCide() {
        return this._statusCode;
    }

    constructor(request) {
        super(`Route not found "${ request.url }" .`);

        this._request = request;
        this._statusCode = 404;
    }
}

module.exports = RouteNotFound;