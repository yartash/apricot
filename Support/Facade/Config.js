class Config {
    static get facadeAccessor() {
        return 'config';
    }
}

module.exports = Config;