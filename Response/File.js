const fs = require('fs');
const type = require('mime');

const Response = use('Apricot/Response');

class FileResponse extends Response {
    constructor(file) {
        super();

        this._file = publicPath(file);
    }

    render() {
        super.render();

        if (!fs.existsSync(this._file)) {
            this._notFound();
        }

        this._setHeader();

        let readStream = fs.createReadStream(this._file);
        readStream.pipe(this._response);
    }

    //todo implement cache control

    _setHeader() {
        let mime = type.lookup(this._file);
        let stat = fs.statSync(this._file);

        this
            ._response
            .writeHead(
                200,
                {
                    'Content-Type': mime,
                    'Content-Length': stat.size
                }
            );
    }

    _notFound() {
        this
            ._response
            .writeHead(
                404,
                'Not Found'
            );
    }
}

module.exports = FileResponse;