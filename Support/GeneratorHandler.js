const Collection = use('Apricot/Support/Collection');

class GeneratorHandler {
	constructor (resolve = false, reject = false) {
		/**
		 * @private
		 * @type {Promise<Any>|Boolean}
		 */
		this._resolve = resolve;
		/**
		 * @private
		 * @type {Promise<Any>|Boolean}
		 */
		this._reject = reject;
	}

	/**
	 *
	 * @param iterator
	 */
	executor (iterator) {
		let handler = (iterator, value) => {
			let next = iterator.next(value);

			if (next.done) {
				if (this._resolve) {
					this._resolve(next.value);
				}
			} else {
				switch (true) {
					case Function.isPromise(next.value):
						this.promiseHandler(next.value, iterator, handler);
						break;
					case next.value instanceof Collection:
						this.collactionHandler(next.value, iterator, handler);
				}
			}
		}

		handler(iterator);
	}

	collactionHandler (value, iterator, handler) {
		value.run()
			.then(
				result => {
					handler(iterator, result);
				},
				error => {
					if (this._reject) {
						this._reject(error);
					}
				}
			);
	}

	promiseHandler (value, iterator, handler) {
		value.then(
			result => {
				switch (true) {
					case result instanceof Collection:
						this.collactionHandler(result, iterator, handler);
						break;
					default:
						handler(iterator, result);
						break;
				}
			},
			error => {
				if (this._reject) {
					this._reject(error);
				}
			}
		);
	}
}

module.exports = GeneratorHandler;