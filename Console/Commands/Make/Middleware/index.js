const GeneratorCommand = use('Apricot/Console/Commands/Make/GeneratorCommand');

class Middleware extends GeneratorCommand {
    get signature () {
        return 'make:middleware {name}';
    }

    get name () {
        return 'Make:middleware';
    }

    get description () {
        return 'Create a new middleware class';
    }

    /**
     * Get the stub file for the generator.
     *
     * @protected
     * @return string
     */
    get _templatePath () {
        return this._files.join(__dirname, 'Templates', 'blank.tmp')
    }

    constructor () {
        super(use('File'));

        /**
         * The type of class being generated.
         *
         * @protected
         * @property {String}
         */
        this._type = 'Middleware';
    }

    /**
     * Get the default namespace for the class.
     *
     * @protected
     * @param {String} rootNamespace
     * @return {String}
     */
    _getDefaultNamespace (rootNamespace) {
        return `${ rootNamespace }/Http/Middleware`;
    }
}

module.exports = Middleware;