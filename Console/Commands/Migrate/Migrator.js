const MigrationRepository = use('MigrationRepository');

class Migrator {
    static get instance () {
        if (!Migrator._instance) {
            Migrator._instance = new Migrator(
                MigrationRepository.instance,
                use('DB'),
                use('File')
            );
        }

        return Migrator._instance;
    }
    
    /**
     * @public
     * @param name
     */
    set connection(name) {
        if (name) {
            this._resolver.defaultConnection = name;
        }

        this._repository.source = name;
        this._connection = name;
    }

	/**
	 * Get all of the custom migration paths.
	 *
	 * @public
	 * @return {Array<Strig>}
	 */
	get paths () {
		return this._paths;
	}

	/**
	 * Get the notes for the last operation.
	 *
	 * @public
	 * @return {Array<Object>}
	 */
	get notes () {
		return this._notes;
	}

    /**
     * Get the migration repository instance.
     *
     * @public
     * @return {Apricot/Console/Commands/Migrate/MigrationRepository}
     */
    get repository () {
        return this._repository;
    }

    constructor (repository, resolver, files) {
        this._repository = repository;
        this._resolver = resolver;
        this._files = files;

		/**
		 * The paths to all of the migration files.
		 *
		 * @protected
		 * @property {Array<String>}
		 */
		this._paths = [];
		/**
		 * The notes for the current operation.
		 *
		 * @var array* @protected
		 * @property {Array<String>}
		 */
		this._notes = [];
    }

    /**
     * Drop all of the database tables.
     *
     * @public
     */
    * dropAllTables() {
        return yield * this._repository.dropAllTables();
    }

    /**
     * Rolls all of the currently applied migrations back.
     *
	 * @public
     * @param {Array<String>|String} paths
     * @param {Boolean} pretend
     * @return {Array<Any>}
     */
    * reset (paths = [], pretend = false) {
        this._notes = [];

        // Next, we will reverse the migration list so we can run them back in the
        // correct order for resetting this database. This will allow us to get
        // the database back into its "empty" state ready for the migrations.
        let migrations = yield * this._repository.getRan();

        if (migrations.length === 0) {
            this.note({
                style: 'info',
                message: 'Nothing to rollback.'
            });

            return [];
        }

        return yield * this._resetMigrations(migrations.reverse(), paths, pretend);
    }

    /**
     * @return {boolean}
     */
    * repositoryExists () {
        return yield * this._repository.repositoryExists();
    }

	/**
	 * Run the pending migrations at a given path.
	 *
	 * @param {(Array<String>|String)} paths
	 * @param {Object} options
	 * @return {Array<String>}
	 */
	* run(paths = [], options = {}) {
		this._notes = [];

		// Once we grab all of the migration files for the path, we will compare them
		// against the migrations that have already been run for this package then
		// run each of the outstanding migrations against a database connection.
		let files = yield this.getMigrationFiles(paths);
		let migrations = this._pendingMigrations(files, yield * this._repository.getRan());

		// Once we have all these migrations that are outstanding we are ready to run
		// we will go ahead and run them "up". This will execute each migration as
		// an operation against a database. Then we'll return this list of them.
		yield * this.runPending(migrations, options);

		return migrations;
	}

    /**
     * Run an array of migrations.
     *
     * @public
     * @param {Object} migrations
     * @param {Object} options
     */
    * runPending (migrations, options = {}) {
        // First we will just make sure that there are any migrations to run. If there
        // aren't, we will just make a note of it to the developer so they're aware
        // that all of the migrations have been run against this database system.
        if (!Object.keys(migrations).length) {
            this.note({
                style: 'info',
                message: 'Nothing to migrate'
            });

            return;
        }

        // Next, we will get the next batch number for the migrations so we can insert
        // correct batch number in the database migrations repository when we store
        // each migration's execution. We will also extract a few of the options.
        let batch = yield * this._repository.getNextBatchNumber();
        let pretend = options.pretend !== undefined ? options.pretend : false;
        let step = options.step !== undefined ? options.step : false;

        // Once we have the array of migrations, we will spin through them and run the
        // migrations "up" so the changes are made to the databases. We'll then log
        // that the migration was run so we don't repeat it next time we execute.
        for (let key in migrations) {
            yield * this._runUp(migrations[key], batch, pretend);

            if (step) {
                batch++;
            }
        }
    }

    /**
     * Rollback the last migration operation.
     *
	 * @public
     * @param {(Array<String>|String)} paths
     * @param {Object} options
     * @return {Array<String>}
     */
    * rollback (paths = [], options = {}) {
        this._notes = [];

		// We want to pull in the last batch of migrations that ran on the previous
		// migration operation. We'll then reverse those migrations and run each
		// of them "down" to reverse the last migration "operation" which ran.
		let migrations = yield * this._getMigrationsForRollback(options);

		if (migrations.length === 0) {
			this.note({
                style: 'info',
                message: 'Nothing to rollback.'
            });

			return [];
		}

		return yield * this._rollbackMigrations(migrations, paths, options);
	}

    /**
     * Resolve a migration instance from a file.
     *
     * @public
     * @param {String} file
     * @return {Apricot/Database/Migration}
     */
    resolve (file) {
        let classFile = require(file);

        return new classFile();
    }

	/**
	 * Get all of the migration files in a given path.
	 *
	 * @public
	 * @param {(String|Array<String>)} paths
	 * @return {Array<String>}
	 */
	getMigrationFiles (paths) {
		return collect(paths)
			.flatMap(path => {
				return this._files.glob(this._files.join(path, '*_*.js'));
			})
			.filter()
			.sortBy(file => {
				return this._files.name(file);
			})
			.values()
			.keyBy(file => {
				return this._files.name(file);
			})
			.all();
	}

    /**
     * Resolve the database connection instance.
     *
     * @param {String} connection
     * @return {Apricot/Database/Connections/Connections}
     */
    resolveConnection (connection) {
        return this._resolver.connection(connection ? connection : this._connection);
    }

	/**
	 * Raise a note event for the migrator.
	 *
	 * @public
	 * @param {Object} message
	 */
	note (message) {
		this._notes.push(message);
	}

    /**
     * Run "up" a migration instance.
     *
     * @protected
     * @param {String} file
     * @param {Int} batch
     * @param {Boolean} pretend
     */
    * _runUp (file, batch, pretend) {
        // First we will resolve a "real" instance of the migration class from this
        // migration file name. Once we have the instances we can run the actual
        // command such as "up" or "down", or we can just simulate the action.
        let name = this._files.name(file);
        let migration = this.resolve(file);

        if (pretend) {
            return yield * this._pretendToRun(migration, 'up');
        }

        this.note({
			style: 'comment',
			message: `Migrating: ${ name }`
		});

        yield * this._runMigration(migration, 'up');

        // Once we have run a migrations class, we will log that it was run in this
        // repository so that we don't try to run it next time we do a migration
        // in the application. A migration repository keeps the migrate order.
		yield * this._repository.log(name, batch);

        this.note({
            style: 'info',
            message: `Migrated: ${ name }`
        });
    }

	/**
	 * Run a migration inside a transaction if the database supports it.
	 *
	 * @protected
	 * @param {Object} migration
	 * @param {String} method
	 */
	* _runMigration (migration, method) {
		let connection = this.resolveConnection(migration.connection);
		let callback = function * () {
			if (method in migration && Function.isGenerator(migration[method])) {
				yield * migration[method]();
			}
		}

		if (
			this._getSchemaGrammar(connection).supportsSchemaTransactions &&
			migration.withinTransaction
		) {
			yield connection.transaction(callback);
		} else {
			yield * callback();
		}
	}

    /**
     * Pretend to run the migrations.
     *
     * @protected
     * @param {Apricot/Database/Migration}  $migration
     * @param {String} method
     */
    * _pretendToRun (migration, method) {
    	let queries = yield * this._getQueries(migration, method);

		queries.forEach(query => {
			this.note({
				style: 'info',
				message: `${ getClass(migration) } {${ query.query }}`
			});
		});
    }

    /**
     * Reset the given migrations.
     *
     * @protected
     * @param {Array<Any>} migrations
     * @param {Array<String>} paths
     * @param {Boolean} pretend
     * @return {Array}
     */
    * _resetMigrations (migrations, paths, pretend = false) {
        // Since the getRan method that retrieves the migration name just gives us the
        // migration name, we will format the names into objects with the name as a
        // property on the objects so that we can pass it to the rollback method.
        migrations = migrations.map(m => {
            return {migration: m};
        });

        return yield * this._rollbackMigrations(migrations, paths, {pretend});
    }

    /**
     * Get all of the queries that would be run for a migration.
     *
     * @protected
     * @param {Apricot/Database/Migration} migration
     * @param {String} method
     * @return {Array<String>}
     */
    * _getQueries (migration, method) {
        // Now that we have the connections we can resolve it and pretend to run the
        // queries against the database returning the array of raw SQL statements
        // that would get fired against the database system for this migration.
        let db = this.resolveConnection(migration.connection);

        return yield db.pretend(function * () {
            if (method in migration && Function.isGenerator(migration[method])) {
                yield * migration[method]();
            }
        });
    }

    /**
     * Get the migrations for a rollback operation.
     *
     * @param {Object} options
     * @return {Array<Any>}
     */
    * _getMigrationsForRollback (options) {
        if (options.step || options.step > 0) {
            return yield * this._repository.getMigrations(options.step);
        } else {
        	return yield * this._repository.getLast();
		}
    }

    /**
     * Get the migration files that have not yet run.
     *
	 * @protected
     * @param {Object} files
     * @param {Array<String>} ran
     * @return {Array<String>}
     */
    _pendingMigrations(files, ran) {
        ran.forEach(item => {
            delete files[item];
        });

        return files;
    }

	/**
	 * Get the schema grammar out of a migration connection.
	 *
	 * @protected
	 * @param {Apricot/Database/Connections/Connection} connection
	 * @return {Apricot/Database/Schema/Grammars/Grammar}
	 */
	_getSchemaGrammar(connection) {
		let grammar = connection.schemaGrammar;

		if (grammar === null) {
			connection.useDefaultSchemaGrammar;
			grammar = connection.schemaGrammar;
		}

		return grammar;
	}

    /**
     * Rollback the given migrations.
     *
     * @protected
     * @param {Array<Object>} migrations
     * @param {Array<String>|String} paths
     * @param {Object} options
     * @return {Array<String>}
     */
    * _rollbackMigrations (migrations, paths, options) {
        let rolledBack = [];
        let files = yield this.getMigrationFiles(paths);
        // $this->requireFiles($files = $this->getMigrationFiles($paths));

        // Next we will run through all of the migrations and call the "down" method
        // which will reverse each migration in order. This getLast method on the
        // repository already returns these migration's names in reverse order.
		for (let i = 0; i < migrations.length; i++) {
			let migration = migrations[i];
			let file = files[migration.migration];

			if (!file) {
                this.note({
                    style: 'error',
                    message: `Migration not found: ${ migration.migration }`
                });

                continue;
			}

            rolledBack.push(file);

            yield * this._runDown(
                file,
                migration,
                options.pretend ? options.pretend : false
            );
		}

        return rolledBack;
    }

    /**
     * Run "down" a migration instance.
     *
	 * @protected
     * @param {String} file
     * @param {Object} migration
     * @param {Boolean} pretend
     */
    * _runDown (file, migration, pretend) {
        // First we will get the file name of the migration so we can resolve out an
        // instance of the migration. Once we get an instance we can either run a
        // pretend execution of the migration or we can run the real migration.
        let name = this._files.name(file);
        let instance = this.resolve(file);

        if (pretend) {
            return yield * this._pretendToRun(instance, 'down');
        }

        this.note({
			style: 'comment',
			message: `Rolling back: ${ name }`
        });

        yield * this._runMigration(instance, 'down');
        // Once we have successfully run the migration "down" we will remove it from
        // the migration repository so it will be considered to have not been run
        // by the application then will be able to fire by any later operation.
        let f = yield * this._repository.delete(migration);

        this.note({
			style: 'info',
			message: `Rolled back: ${ name }`
		});
    }
}

module.exports = Migrator;