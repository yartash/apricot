class BadMethodCall extends Error {
    constructor(message) {
        super(message);
    }
}

module.exports = BadMethodCall;