class JSDoc3Parse extends Error {
    get file () {
        return this._file;
    }
    
    get error () {
        return this._error;
    }
    
    constructor(error, file) {
        super(`File ${ file } JavaScript Doc3 parsing error.`);
        
        this._file = file;
        this._error = error;
    }
}

module.exports = JSDoc3Parse;