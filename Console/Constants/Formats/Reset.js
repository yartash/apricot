class Formats {
    static get all () {
        return '\x1b[0m';
    }
    
    static get bold () {
        return '\x1b[21m';
    }
    
    static get underlined () {
        return '\x1b[24m';
    }
    
    static get inverted () {
        return '\x1b[27m';
    }
}

module.exports = Formats;