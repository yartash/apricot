class NameMissing extends Error {    
    get command () {
        return this._command;
    }
    
    constructor(command) {
        super('Name argumnets is required.');
        
        this._command = command;
    }
}

module.exports = NameMissing;