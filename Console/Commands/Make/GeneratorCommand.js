const _ = require('lodash');

const Command = use('Apricot/Console/Command');

class GeneratorCommand extends Command {
    /**
     * Get the stub file for the generator.
     *
     * @protected
     * @return string
     */
    get _templatePath () {
        return '';
    }

    /**
     * Get the desired class name from the input.
     *
     * @protected
     * @return {String}
     */
    get _nameInput () {
        return this.argument('name').trim();
    }

    /**
     * Get the root namespace for the class.
     *
     * @protected
     * @return {String}
     */
    get _rootNamespace () {
        return application.appNamespace;
    }

    /**
     * Create a new controller creator command instance.
     *
     * @param {Apricot/Filesystem} files
     */
    constructor (files) {
        super();

        /**
         * The filesystem instance.
         *
         * @protected
         * @property {Apricot/Filesystem}
         */
        this._files = files;

        /**
         * The type of class being generated.
         *
         * @protected
         * @property {String}
         */
        this._type = '';
    }

    /**
     * Execute the console command.
     *
     * @public
     * @return {Boolean|Null}
     */
    * handler () {
        let name = this._qualifyClass(this._nameInput);
        let path = this._getPath(name);

        // First we will check to see if the class already exists. If it does, we don't want
        // to create the class and overwrite the user's code. So, we will bail out so the
        // code is untouched. Otherwise, we will continue generating this class' files.
        let exists = yield this._alreadyExists(this._nameInput);

        if ((!this.hasOption('force') || !this.option('force')) && exists) {
            this.error(`${ this._type } already exists!`);

            return false;
        }

        // Next, we will generate the path to the location where this class' file should get
        // written. Then, we will build the class and make the proper replacements on the
        // stub files so that it gets the correctly formatted namespace and class name.
        yield * this._makeDirectory(path);

        let content = yield * this._buildClass(name);

        yield * this._files.write(path, content);

        this.info(`${ this._type } created successfully.`);
    }

    /**
     * Parse the class name and format according to the root namespace.
     *
     * @protected
     * @param {String} name
     * @return {String}
     */
    _qualifyClass (name) {
        name = name.trimLeft('/');
        let rootNamespace = this._rootNamespace;

        if (name.startsWith(rootNamespace)) {
            return name;
        }

        return this._qualifyClass(`${ this._getDefaultNamespace(rootNamespace.trim('/')) }/${ name }`);
    }

    /**
     * Get the default namespace for the class.
     *
     * @protected
     * @param {String} rootNamespace
     * @return {String}
     */
    _getDefaultNamespace (rootNamespace) {
        return rootNamespace;
    }

    /**
     * Get the destination class path.
     *
     * @protected
     * @param {String} name
     * @return {String}
     */
    _getPath (name) {
        name = name.replace(this._rootNamespace, '');

        return `${ application.appPath(name) }.js`;
    }

    /**
     * Determine if the class already exists.
     *
     * @protected
     * @param {String} rawName
     * @return {Boolean}
     */
    _alreadyExists (rawName) {
        return this._files.exists(this._getPath(this._qualifyClass(rawName)));
    }

    /**
     * Build the directory for the class if necessary.
     *
     * @protected
     * @param {String} path
     * @return {String}
     */
    * _makeDirectory (path) {
        let directory = this._files.dirname(path);
        let exists = yield this._files.exists(directory);

        if (!exists) {
            yield this._files.makeDirecory(directory, 0o777, true, true);
        }

        return path;
    }

    /**
     * Build the class with the given name.
     *
     * @protected
     * @param {String} name
     * @return {String}
     */
    * _buildClass (name) {
        let template = yield * this._files.get(this._templatePath);
        let complie = _.template(template);

        return complie({
            dummyNamespace: this._getNamespace(name),
            dummyRootNamespace: this._rootNamespace,
            dummyClass: name.replace(`${ this._getNamespace(name) }/`, ''),
            namespacedDummyUserModel: use('Config').get('auth.providers.users.model')
        });
    }

    /**
     * Get the full namespace for a given class, without the class name.
     *
     * @protected
     * @param {String} name
     * @return {String}
     */
    _getNamespace (name) {
        return name.split('/').slice(0, -1).join('/').trim('/');
    }
}

module.exports = GeneratorCommand;