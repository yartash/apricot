const InvalidKeyFormatException = use('Exceptions/InvalidKeyFormat');

class Collection {
	static get numericType () {
		return 0;
	}

	static get associateType () {
		return 1;
	}
	
	set items (items) {
	    this._items = this._parseItems(items);
    }

    constructor(items = []) {
		this._queue = [];
        this._items = this._parseItems(this._getArrayableItems(items));
    }

    static make(items = []) {
    	return new Collection(items);
	}

	/**
	 * Map a collection and flatten the result by a single level.
	 *
	 * @public
	 * @param {Function} callback
	 * @return {Apricot/Support/Collection}
	 */
	flatMap (callback) {
		return this.map(callback).collapse();
	}

	/**
	 * Collapse the collection of items into a single array.
	 *
	 * @public
	 * @return {Apricot/Support/Collection}
	 */
	collapse ()	{
		this._queue
			.push(() => {
				return new Promise(resolve => {
					let items = new Map();

					this._items
						.forEach((value, key) => {
							if (this._type == Collection.numericType && Array.isArray(value)) {
								value.forEach((item, index) => {
									items.set(index, item);
								});
							} else if (this._type == Collection.associateType && Object.isObject(value)) {
								for (let index in value) {
									items.set(index, value[index])
								}
							} else {
								items.set(key, value);
							}
						});

					this._items = items;
					resolve();
				});
			});

		return this;
	}

	/**
	 * Run a map over each of the items.
	 *
	 * @param {Function} callback
	 * @return {Apricot/Support/Collection}
	 */
	map (callback) {
		this._queue
			.push(() => {
				return new Promise(resolve => {
					let keys = Array.from(this._items.keys());
					let items = new Map();

					let map = () => {
						if (keys.length > 0) {
							let key = keys.shift();
							let result = callback(this._items.get(key), key);

							if (Function.isPromise(result)) {
								result.then(value => {
									items.set(key, value);
									map();
								});
							} else {
								items.set(key, result);
								map();
							}
						} else {
							this._items = items;
							resolve();
						}
					}

					map();
				});
			});

		return this;
	}

	/**
	 * Run a filter over each of the items.
	 *
	 * @public
	 * @param {(Function|Null)} callback
	 * @return {Apricot/Support/Collection}
	 */
	filter (callback = null) {
		this._queue
			.push(() => {
				return new Promise(resolve => {
					let keys = Array.from(this._items.keys());
					let items = new Map();

					let filter = () => {
						if (keys.length > 0) {
							let key = keys.shift();
							let item = this._items.get(key);

							if (callback === null) {
								if (item !== false) {
									items.set(key, item);
								}

								filter();
							} else {
								let result = callback(this._items.get(key), key);

								if (Function.isPromise(result)) {
									result.then(value => {
										if (value !== false) {
											items.set(key, item);
										}

										filter();
									});
								} else {
									if (result !== false) {
										items.set(key, item);
									}

									filter();
								}
							}
						} else {
							this._items = items;
							resolve();
						}
					}

					filter();
				});
			});

		return this;
	}

    /**
     * Sort the collection using the given callback.
     *
     * @public
     * @param {(Function|String)} callback
     * @param  {Boolean} descending
     * @return {Apricot/Support/Collection}
     */
    sortBy (callback, descending = false) {
        this._queue
            .push(() => {
                callback = this._valueRetriever(callback);

                return new Promise(resolve => {
                    let keys = Array.from(this._items.keys());
                    let values = [];

                    let sort = () => {
                        if (keys.length > 0) {
                            let key = keys.shift();
                            let result = callback(this._items.get(key), key);

                            if (Function.isPromise(result)) {
                                result.then(value => {
                                    values.push(value);
                                    sort();
                                });
                            } else {
                                values.push(result);
                                sort();
                            }
                        } else {
                            let originalValuea = values.slice();
                            let items = new Map();

                            values.sort();

                            if (descending) {
                                values = values.reverse();
                            }

                            values.forEach((item, index) => {
                                items.set(index, this._items.get(originalValuea.indexOf(item)))
                            });

                            this._items = items;
                            resolve();
                        }
                    }

                    sort();
                })
            });

        return this;
    }

    /**
     * Reset the keys on the underlying array.
     *
     * @public
     * @return {Apricot/Support/Collection}
     */
    values () {
        this._queue
            .push(() => {
                return new Promise(resolve => {
                    let items = [];

                    this._items
                        .forEach(value => {
                            items.push(value);
                        });

                    
                    this._items = this._parseItems(items);
                    resolve();
                });
            });

        return this;
    }

    /**
     * Key an associative array by a field or using a callback.
     *
     * @public
     * @param {(Function|String)} callback
     * @return {Apricot/Support/Collection}
     */
    keyBy (callback) {
        this._queue
            .push(() => {
                callback = this._valueRetriever(callback);

                return new Promise(resolve => {
                    let keys = Array.from(this._items.keys());
                    let items = new Map();
                    let types = []

                    let keyBy = () => {
                        if (keys.length > 0) {
                            let key = keys.shift();
                            let result = callback(this._items.get(key), key);

                            if (Function.isPromise(result)) {
                                result.then(newKey => {
                                    types.push(this._getType(newKey));
                                    items.set(newKey, this._items.get(key));
                                    keyBy();
                                });
                            } else {
								types.push(this._getType(result));
								items.set(result, this._items.get(key));
                                keyBy();
                            }
                        } else {
                        	this._type = types.indexOf(Collection.associateType) > -1 ?
								Collection.associateType :
								Collection.numericType;
                            this._items = items;
                            resolve();
                        }
                    };

                    keyBy();
                });
            });

        return this;
    }

    /**
     * Get the values of a given key.
     *
     * @public
     * @param {(String|Array<String>)} value
     * @param {(String|Null)} key
     * @return {Apricot/Support/Collection}
     */
    pluck (value, key = null) {
        this._queue
            .push(() => {
                return new Promise(resolve => {
                    let keys = Array.from(this._items.keys());

                    [value, key] = this._explodePluckParameters(value, key);
                    let items = key === null ? [] : {};

                    this._items
                        .forEach(item => {
                            let itemValue = dataGet(item, value);

                            // If the key is "null", we will just append the value to the array and keep
                            // looping. Otherwise we will key the array using the value of the key we
                            // received from the developer. Then we'll return the final array form.
                            if (key === null) {
                                items.push(itemValue);
                            } else {
                                items[dataGet(item, key).toString()] = itemValue;
                            }
                        });

                    this._items = this._parseItems(items);
                    resolve();
                });
            });

        return this;
    }

    /**
     * Get all of the items in the collection.
     *
     * @public
     * @return {(Array<Any>|Object)}
     */
    all() {
        this._queue
            .push(() => {
                return new Promise(resolve => {
                    let result = this._type === Collection.numericType ? [] : {};

                    this._items.forEach((value, key) => {
                        result[key] = value;
                    });
                    
                    resolve(result);
                });
            });

        return this;
    }

    /**
     * Determine if the collection is empty or not.
     *
     * @public
     * @return {Boolean}
     */
    isEmpty () {
        this._queue
            .push(() => {
                return new Promise(resolve => {
                    resolve(this._items.keys().length == 0);
                });
            });

        return this;
    }

	/**
	 * Promise chain executor
	 *
	 * @public
	 * @return {Promise<Apricot/Support/Collection>}
	 */
	run () {
		return this._queue.length > 0 ?
			this._executeChain() :
			Promise.resolve(this);
	}

	/**
	 *
	 * @private
	 * @return {Promise<Apricot/Support/Collection>}
	 */
	_executeChain() {
		return new Promise(resolve => {
			let execute = promise => {
				promise.then(result => {
					if (this._queue.length > 0) {
						execute(this._queue.shift()());
					} else {
						resolve(result !== undefined ? result : this);
					}
				});
			};
			execute(this._queue.shift()());
		});
	}

	_getType(value) {
		switch (true) {
			case Number.isNumber(value):
				return Collection.numericType;
				break;
			case String.isString(value):
				return Collection.associateType;
				break;
			default:
				throw new InvalidKeyFormatException(this, value);
		}
    }

    _getArrayableItems(items) {
        switch (true) {
            case Array.isArray(items):
				return items;
			case Number.isNumber(items):
			case String.isString(items):
			case Boolean.isBoolean(items):
			case Date.isDate(items):
				return [items];
			default:
				return items;
        }
    }

    _parseItems (items) {
		this._type = Array.isArray(items) ?
			Collection.numericType :
			Collection.associateType;
		let result = new Map();

		for (let key in items) {
			result.set(key, items[key]);
		}

		return result;
	}

    /**
     * Get a value retrieving callback.
     *
     * @protected
     * @param {String} value
     * @return {Function}
     */
    _valueRetriever (value) {
        if (this._useAsCallable(value)) {
            return value;
        }

        return item => {
            return item[value];
        }
    }

    /**
     * Determine if the given value is callable, but not a string.
     *
     * @protected
     * @param {Any} value
     * @return {Boolean}
     */
    _useAsCallable (value) {
        return !String.isString(value) && Function.isFunction(value);
    }

    /**
     * Explode the "value" and "key" arguments passed to "pluck".
     *
     * @protected
     * @param {String|Array<String>} value
     * @param {String|Array<String>|Null} key
     * @return Array<Array<String>>
     */
    _explodePluckParameters (value, key) {
        value = String.isString(value) ? value.split('.') : value;
        key = key === null || Array.isArray(key) ? key : key.split('.');

        return [value, key];
    }
}

module.exports = Collection;