const Tag = use('Tag');
const Parameter = use('Parameter');

class Method {
	/**
	 * @public
	 * @return {string}
	 */
	get name () {
		return this._data.name;
	}

	/**
	 * @public
	 * @return {Array<Apricot/Reflection/Tag>}
	 */
	get tags () {
		return this._tags;
	}

	/**
	 * @public
	 * @return {Array<Apricot/Reflection/Parameter>}
	 */
	get parameters () {
		return this._parameters;
	}
	
	constructor (data) {
		this._data = data;
		this._tags = [];
		this._parameters = [];

		this._parse();
	}

	/**
	 * @public
	 * @param {string} titel
	 * @return {Apricot/Reflection/Tag}
	 */
	getTag (titel) {
		return this._tags
			.find(tag => {
				return tag.title == titel;
			});
	}

	/**
	 * @public
	 * @param {string} name
	 * @return {Apricot/Reflection/Parameter}
	 */
	getParameter (name) {
		return this._parameters
			.find(parameter => {
				return parameter.name == name;
			});
	}

	/**
	 * @private
	 * @return {void}
	 */
	_parse () {
		this._parseTga();
		this._parseParameter();
	}

	/**
	 * @private
	 * @return {void}
	 */
	_parseTga () {
		if (this._data.tags) {
			this._tags = this._data
				.tags
				.map(item => {
					return new Tag(item);
				});
		}
	}

	/**
	 * @private
	 * @return {void}
	 */
	_parseParameter () {
		if (this._data.params) {
			this._parameters = this._data
				.params
				.map(item => {
					return new Parameter(item);
				});
		}
	}
}

module.exports = Method;