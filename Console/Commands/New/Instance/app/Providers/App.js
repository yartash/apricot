const _ = require('lodash');

const BaseProvider = use('Apricot/Application/BaseProvider');

class AppProvider extends BaseProvider {
    boot() {
        
    }
}

module.exports = AppProvider;