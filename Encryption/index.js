const crypto = require('crypto');

class Encryption {
    get algorithm() {
        return 'aes-256-gcm';
    }

    get key() {
        return use('Config').get('app.key')
    }

    encrypt(text) {
        let iv = crypto.randomBytes(12);
        let cipher = crypto.createCipheriv(this.algorithm, this.key, iv);
        let encrypted = cipher.update(text, 'utf8', 'hex');

        encrypted += cipher.final('hex');

        return new Buffer(JSON.stringify({
            content: encrypted,
            iv: iv
        }))
            .toString('base64');
    }

    decrypt(text) {
        let encrypted = JSON
            .parse(
                new Buffer(
                    text,
                    'base64'
                )
                    .toString('utf8')
            );
        let cipher = crypto.createCipheriv(this.algorithm, this.key, new Uint8Array(encrypted.iv.data));

        return cipher.update(encrypted.content, 'hex', 'utf8') + cipher.final('utf8');
    }
}

module.exports = Encryption;