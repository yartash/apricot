class Formats {
    static get bold () {
        return '\x1b[1m';
    }
    
    static get underlined () {
        return '\x1b[4m';
    }
    
    static get inverted () {
        return '\x1b[7m';
    }
}

module.exports = Formats;