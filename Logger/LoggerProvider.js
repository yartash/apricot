const BaseProvider = use('Apricot/Application/BaseProvider');

class LoggerProvider extends BaseProvider {
    boot() {
        /**
         * @property { class }
         * @name global#log
         */
        Object.defineProperty(
            global,
            'log',
            {
                value: use('Logger'),
                configurable: false,
                writable: false,
                enumerable: false
            }
        );
    }

    register () {
        this.app.singleton('logger', () => {
            const Logger = use('Apricot/Logger');

            return new Logger();
        });
    }
}

module.exports = LoggerProvider;