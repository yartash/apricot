const Builder = use('Builder');

class PostgresBuilder extends Builder {
	/**
	 * Determine if the given table exists.
	 *
	 * @param  {string} table
	 * @return {boolean}
	 */
	hasTable (table) {
		return new Promise(resolve => {
			let result = this._parseSchemaAndTable(table);
			table = result.table;
            table = `${ this._connection.tablePrefix }${ table }`;

            this._connection
				.select(this._grammar.compileTableExists, [result.schema, table])
				.then(result => {
					resolve(result.rows.length > 0);
				});
        });
	}

    /**
     * Drop all tables from the database.
     *
	 * @public
     */
    dropAllTables () {
        return new Promise(resolve => {
            let tables = [];
            let excludedTables = ['spatial_ref_sys'];

            this._getAllTables()
                .then(result => {
                    result.rows.forEach(row => {
                        row = Object.values(row);
                        let table = row.first();

                        if (excludedTables.indexOf(table) == -1) {
                            tables.push(table);
                        }
                    });

                    if (tables.length == 0) {
                        resolve();
                    }

                    this._connection.statement(this._grammar.compileDropAllTables(tables))
                        .then(result => {
                            resolve();
                        });
                });
        });
    }

	/**
	 * Parse the table name and extract the schema and table.
	 *
	 * @protected
	 * @param {String} table
	 * @return {Array<String>}
	 */
	_parseSchemaAndTable (table) {
		table = table.split('.');
		let schema = this._connection.getConfig('schema');

		if (Array.isArray(schema)) {
			if (schema.indexOf(table[0]) > -1) {
				return {
					schema: table.shift(),
					table: table.join('.')
				}
			}

			schema = schema.first();
		}

		return {
			schema: schema ? schema : 'public',
			table: table.join('.')
		};
	}

    /**
     * Get all of the table names for the database.
     *
     * @protected
     * @return {Array<Object>}
     */
    _getAllTables () {
        return this._connection.select(
            this._grammar.compileGetAllTables(this._connection.getConfig('schema'))
        );
    }
}

module.exports = PostgresBuilder;