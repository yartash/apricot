const _ = require('lodash');
const mime = require('mime');
const dateFormat = require('dateformat');

const ResponseHeaderBag = use('ResponseHeaderBag');

const UnexpectedValueException = use('Apricot/Exceptions/UnexpectedValue');
const InvalidArgumentException = use('Apricot/Exceptions/InvalidArgument');

class Response {
    static get statusTexts() {
        return {
            100: 'Continue',
            101: 'Switching Protocols',
            102: 'Processing',            // RFC2518
            200: 'OK',
            201: 'Created',
            202: 'Accepted',
            203: 'Non-Authoritative Information',
            204: 'No Content',
            205: 'Reset Content',
            206: 'Partial Content',
            207: 'Multi-Status',          // RFC4918
            208: 'Already Reported',      // RFC5842
            226: 'IM Used',               // RFC3229
            300: 'Multiple Choices',
            301: 'Moved Permanently',
            302: 'Found',
            303: 'See Other',
            304: 'Not Modified',
            305: 'Use Proxy',
            307: 'Temporary Redirect',
            308: 'Permanent Redirect',    // RFC7238
            400: 'Bad Request',
            401: 'Unauthorized',
            402: 'Payment Required',
            403: 'Forbidden',
            404: 'Not Found',
            405: 'Method Not Allowed',
            406: 'Not Acceptable',
            407: 'Proxy Authentication Required',
            408: 'Request Timeout',
            409: 'Conflict',
            410: 'Gone',
            411: 'Length Required',
            412: 'Precondition Failed',
            413: 'Payload Too Large',
            414: 'URI Too Long',
            415: 'Unsupported Media Type',
            416: 'Range Not Satisfiable',
            417: 'Expectation Failed',
            418: 'I\'m a teapot',                                               // RFC2324
            421: 'Misdirected Request',                                         // RFC7540
            422: 'Unprocessable Entity',                                        // RFC4918
            423: 'Locked',                                                      // RFC4918
            424: 'Failed Dependency',                                           // RFC4918
            425: 'Reserved for WebDAV advanced collections expired proposal',   // RFC2817
            426: 'Upgrade Required',                                            // RFC2817
            428: 'Precondition Required',                                       // RFC6585
            429: 'Too Many Requests',                                           // RFC6585
            431: 'Request Header Fields Too Large',                             // RFC6585
            451: 'Unavailable For Legal Reasons',                               // RFC7725
            500: 'Internal Server Error',
            501: 'Not Implemented',
            502: 'Bad Gateway',
            503: 'Service Unavailable',
            504: 'Gateway Timeout',
            505: 'HTTP Version Not Supported',
            506: 'Variant Also Negotiates',                                     // RFC2295
            507: 'Insufficient Storage',                                        // RFC4918
            508: 'Loop Detected',                                               // RFC5842
            510: 'Not Extended',                                                // RFC2774
            511: 'Network Authentication Required',
        };
    }

    get isInvalid() {
        return this._statusCode < 100 || this._statusCode >= 600;
    }

    get isInformational() {
        return this._statusCode >= 100 && this._statusCode < 200;
    }

    get isEmpty() {
            return [204, 304].indexOf(this._statusCode) > -1;
    }

    get headersSent() {
        return this._response ? this._response._headerSent : false;
    }

    get content() {
        return this._content;
    }

    get protocolVersion() {
        return this._version;
    }

    get statusCode() {
        return this._statusCode;
    }

    get charset() {
        return this._charset;
    }

    constructor(content = '', status = 200, headers = {}) {
        this._headers = new ResponseHeaderBag(headers);
        this._original = this._content = content;
        this.setStatusCode(status);
        this.setProtocolVersion('1.0');

        /* RFC2616 - 14.18 says all Responses need to have a Date */
        if (!this._headers.has('Date')) {
            this.setDate(new Date());
        }
    }

    * make() {
        yield * this.setContent(this._original)
    }

    static create(content = '', status = 200, headers = {}) {
        return new Response(content, status, headers);
    }

    * setContent(content) {
        if (
            null !== content &&
            !_.isString(content) &&
            !_.isNumber(content) &&
            content.toString
        ) {
            throw new UnexpectedValueException(
                `The Response content must be a string or object implementing __toString(), "${ typeof content }" given.`
            );
        }

        this._content = content.toString();

        return this;
    }

    setStatusCode(code, text = null) {
        this._statusCode = code = parseInt(code);

        if (this.isInvalid) {
            throw new InvalidArgumentException(`The HTTP status code "${ code }" is not valid.`);
        }

        if (null === text) {
            this._statusText = Response.statusTexts[code] ? Response.statusTexts[code] : 'unknown status';

            return this;
        }

        if (false === text) {
            this._statusText = '';

            return this;
        }

        this._tatusText = text;

        return this;
    }

    setProtocolVersion(version) {
        this._version = version;

        return this;
    }

    setDate(date) {
        this._headers.set('Date', dateFormat(date, 'UTC:ddd, dd mmm yyyy HH:MM:ss GMT'));

        return this;
    }

    setCharset(charset) {
        this._charset = charset;

        return this;
    }

    * prepare(request) {
        let headers = this._headers;

        if (this.isInformational || this.isEmpty) {
            yield * this.setContent(null);
            headers.remove('Content-Type');
            headers.remove('Content-Length');
        } else {
            if (!headers.has('Content-Type')) {
                let mimeType = mime.getType('html');
                let charset = this.charset ? this.charset : 'UTF-8';

                headers.set('Content-Type', `${ mimeType }; charset=${ charset }`);
            } else if (headers.get('Content-Type').indexOf('charset') == -1) {
                headers.set('Content-Type', `${ headers.get('Content-Type') }; charset=${ charset }`);
            }

            if (headers.has('Transfer-Encoding')) {
                headers.remove('Content-Length');
            }

            if (request.method.toLowerCase() == 'head') {
                let length = headers.get('Content-Length');
                yield * this.setContent(null);

                if (length) {
                    headers.set('Content-Length', length);
                }
            }
        }

        if (request.protocol != '1.0') {
            this.setProtocolVersion(request.protocol);
        }

        if (
            this.protocolVersion == '1.0' &&
            headers.get('Cache-Control').indexOf('no-cache') != -1
        ) {
            headers.set('pragma', 'no-cache');
            headers.set('expires', -1);
        }

        return this;
    }

    sendHeaders() {
        if (this.headersSent) {
            return this;
        }

        if (!this._headers.has('Date')) {
            this.setDate(new Date());
        }

        _
            .each(
                this._headers.allPreserveCaseWithoutCookies(),
                (values, name) => {
                    _
                        .each(
                            values,
                            value => {
                                this
                                    ._response
                                    .setHeader(name, value);
                            }
                        )
                }
            );

        this
            ._response
            .setHeader(
                'Set-Cookie',
                _
                    .map(
                        this._headers.getCookies(),
                        cookie => {
                            return cookie.toString();
                        }
                    )
            );

		this
			._response
			.setHeader('Framework', 'Apricot JS');

        this
            ._response
            .writeHead(
                this._statusCode,
                this._statusText,
                {}
            );

        return this;
    }

    sendContent() {
        this
            ._response
            .write(
                this._content,
                'utf8'
            )

        return this;
    }

    send(response) {
        this._response = response;

        this.sendHeaders();
        this.sendContent();
        this._response.end();

        return this;
    }
}

module.exports = Response;