const Command = use('Apricot/Console/Commands/Migrate');

const MigrationRepository = use('Apricot/Console/Commands/Migrate/MigrationRepository');

class Install extends Command {
    get signature () {
        return 'migrate:install';
    }

    get name () {
        return 'Migrate Install';
    }

    get description () {
        return 'Create the migration repository';
    }

    get optionsDefinition () {
        return {
            database: {
                description: 'The database connection to use.',
                short: 'd',
            }
        };
    }
    
    constructor () {
        super();
        
        /**
         * The repository instance.
         *
         * @property {Apricot/Console/Commands/Migrate/MigrationRepository}
         */
        this._repository = MigrationRepository.instance;
    }

    * handler () {
        this._repository.source = this.option('database');

        yield * this._repository.createRepository();
    }
}

module.exports = Install;