class Schema {
    static get facadeAccessor() {
        return use('db').connection().schemaBuilder;
    }
}

module.exports = Schema;