class MigrationRepository {
    static get instance () {
        if (!MigrationRepository._instance) {
            MigrationRepository._instance = new MigrationRepository(
                use('DB'),
                use('Config').get('database.migrations')
            );
        }

        return MigrationRepository._instance;
    }

    set source(name) {
        this._connection = name;
    }

    get connection() {
        return this._resolver.connection(this._connection);
    }

	/**
	 * Get a query builder for the migration table.
	 *
     * @protected
	 * @return {Apricot/Database/Query/Builder}
	 */
	get table () {
		return this.connection.table(this._table);
	}

    /**
     * Get the last migration batch number.
     *
     * @public
     * @return {Number}
     */
    get lastBatchNumber () {
        return this.table.max('batch');
    }

    constructor (resolver, table) {
        this._resolver = resolver;
        this._table = table;
    }

    * repositoryExists() {
        let schema = this.connection.schemaBuilder;

        return yield schema.hasTable(this._table);
    }
    
    /**
     * Drop all of the database tables.
     *
     * @public
     */
    * dropAllTables() {
        let schema = this.connection.schemaBuilder;

        return yield schema.dropAllTables();
    }

    /**
     * Create the migration repository data store.
     */
    * createRepository () {
        let schema = this.connection.schemaBuilder;

        return yield schema.create(this._table, table => {
            // The migrations table is responsible for keeping track of which of the
            // migrations have actually run for the application. We'll create the
            // table to hold the migration file's path as well as the batch ID.
            table.increments('id');
            table.string('migration');
            table.integer('batch');
        });
    }

    /**
     * Get the completed migrations.
     *
     * @public
     * @return {Array<Any>}
     */
    * getRan () {
        let result = yield this.table
            .orderBy('batch', 'asc')
            .orderBy('migration', 'asc')
            .pluck('migration');
        
        return yield result.all();
    }
    
    /**
     * Get the last migration batch.
     *
	 * @public
     * @return {Array<Objcet>}
     */
    * getLast () {
    	let batch = yield this.lastBatchNumber;
        let result = yield this.table
			.where('batch', batch)
			.orderBy('migration', 'desc')
			.get();

        return yield result.all();
    }

    /**
     * Get list of migrations.
     *
     * @public
     * @param {Int} steps
     * @return {Array<Objcet>}
     */
    * getMigrations (steps) {
        let result = yield this.table
            .where('batch', '>=', '1')
            .orderBy('batch', 'desc')
            .orderBy('migration', 'desc')
            .take(steps)
            .get();

        return yield result.all();
    }

    /**
     * Get the completed migrations with their batch numbers.
     *
     * @return {Array<Any>}
     */
    * getMigrationBatches () {
        let result = yield this.table
            .orderBy('batch', 'asc')
            .orderBy('migration', 'asc')
            .pluck('batch', 'migration');
        
        return yield result.all();
    }

    /**
     * Get the next migration batch number.
     *
     * @public
     * @return {Number}
     */
    * getNextBatchNumber () {
        let lastBatchNumber = yield this.lastBatchNumber

        return lastBatchNumber + 1;
    }

	/**
	 * Log that a migration was run.
	 *
     * @public
	 * @param {String} file
	 * @param {Int} batch
	 */
	* log (file, batch)	{
	    let record = {
	        migration: file,
            batch: batch
	    };

		return yield this.table.insert(record);
	}

    /**
     * Remove a migration from the log.
     *
     * @public
     * @param {Object} migration
     */
    * delete (migration) {
		return yield this.table.where('migration', migration.migration).delete();
    }
}

module.exports = MigrationRepository;