const Fluent = use('Apricot/Support/Fluent');

class Blueprint {
    /**
     * Get the columns on the blueprint that should be added.
     *
     * @public
     * @return {Array<Apricot/Support/Fluent>}
     */
    get addedColumns () {
        return this._columns
            .filter(column => {
                return !column.change;
            });
    }

    /**
     * Get the columns on the blueprint that should be changed.
     *
     * @return {Array<Apricot/Support/Fluent>}
     */
    get changedColumns () {
        return this._columns
            .filter(column => {
                return !!column.change;
            });
    }

    /**
     * Determine if the blueprint has a create command.
     *
     * @protected
     * @return {boolean}
     */
    get creating () {
        return !!this._commands
            .find(command => {
                return command.name == 'create';
            });
    }

	/**
	 * Get the table the blueprint describes.
	 *
	 * @public
	 * @return {String}
	 */
	get table() {
		return this._table;
	}

    constructor (table, callback = null) {
        this._table = table;
        /**
         * The commands that should be run for the table.
         *
         * @property {Array}
         */
        this._commands = [];
        /**
         * The columns that should be added to the table.
         *
         * @property {Array<Apricot/Support/Fluent>}
         */
        this._columns = [];
		/**
		 * Whether to make the table temporary.
		 *
		 * @property {Boolean}
		 */
		this._temporary = false;
    
        if (callback !== null) {
            callback(this);
        }
    }

	/**
	 * Indicate that the table needs to be temporary.
	 *
	 * @public
	 */
	temporary () {
		this._temporary = true;
	}

    /**
     * Create a new auto-incrementing integer (4-byte) column on the table.
     *
     * @public
     * @param {String} column
     * @return {Apricot/Support/Fluent}
     */
    increments (column) {
        return this.unsignedInteger(column, true);
    }

    /**
     * Create a new unsigned integer (4-byte) column on the table.
     *
     * @public
     * @param {String} column
     * @param {boolean} autoIncrement
     * @return {Apricot/Support/Fluent}
     */
    unsignedInteger (column, autoIncrement = false) {
        return this.integer(column, autoIncrement, true);
    }

    /**
     * Create a new integer (4-byte) column on the table.
     *
     * @public
     * @param {String} column
     * @param {Boolean} autoIncrement
     * @param {Boolean} unsigned
     * @return {Apricot/Support/Fluent}
     */
    integer (column, autoIncrement = false, unsigned = false) {
        return this.addColumn('integer', column, {autoIncrement, unsigned});
    }

    /**
     * Create a new string column on the table.
     *
     * @param {String} column
     * @param {Number} length
     * @return {Apricot/Support/Fluent}
     */
    string (column, length = 255) {
        return this.addColumn('string', column, {length});
    }

	/**
	 * Adds the `remember_token` column to the table.
	 *
	 * @public
	 * @return {Apricot/Support/Fluent}
	 */
	rememberToken () {
		return this.string('remember_token', 100).nullable();
	}

	/**
	 * Add nullable creation and update timestamps to the table.
	 *
	 * @public
	 * @param {Number} precision
	 */
	timestamps (precision = 0) {
		this.timestamp('created_at', precision).nullable();
		this.timestamp('updated_at', precision).nullable();
	}

	/**
	 * Create a new timestamp column on the table.
	 *
	 * @public
	 * @param {String} column
	 * @param {Number} precision
	 * @return {Apricot/Support/Fluent}
	 */
	timestamp (column, precision = 0) {
		return this.addColumn('timestamp', column, { precision });
	}

    /**
     * Add a new column to the blueprint.
     *
     * @param {String} type
     * @param {String} name
     * @param {Object} parameters
     * @return {Apricot/Support/Fluent}
     */
    addColumn(type, name, parameters = {}) {
        let column = new Fluent(
        	Object.assign({type, name}, parameters),
			['unique', 'nullable', 'index']
		);
        this._columns.push(column);

        return column;
    }

    /**
     * Indicate that the table needs to be created.
     *
     * @public
     * @return {Apricot/Support/Fluent}
     */
    create () {
        return this._addCommand('create');
    }

    /**
     * Indicate that the table should be dropped if it exists.
     *
	 * @public
     * @return {Apricot/Support/Fluent}
     */
    dropIfExists () {
        return this._addCommand('dropIfExists');
    }

	/**
	 * Specify the primary key(s) for the table.
	 *
	 * @public
	 * @param {String|Array<String>} columns
	 * @param {String} name
	 * @param {String|Null} algorithm
	 * @return {Apricot/Support/Fluent}
	 */
	primary (columns, name = null, algorithm = null) {
		return this.indexCommand('primary', columns, name, algorithm);
	}

	/**
	 * Specify a unique index for the table.
	 *
	 * @public
	 * @param {String|Array<String>} columns
	 * @param {String} name
	 * @param {String|Null} algorithm
	 * @return {Apricot/Support/Fluent}
	 */
	unique (columns, name = null, algorithm = null) {
		return this._indexCommand('unique', columns, name, algorithm);
	}

	/**
	 * Specify an index for the table.
	 *
	 * @public
	 * @param {String|Array<String>} columns
	 * @param {String} name
	 * @param {String|Null} algorithm
	 * @return {Apricot/Support/Fluent}
	 */
	index (columns, name = null, algorithm = null) {
		return this._indexCommand('index', columns, name, algorithm);
	}

	/**
	 * Specify a spatial index for the table.
	 *
	 * @public
	 * @param {String|Array<String>} columns
	 * @param {String} name
	 * @return {Apricot/Support/Fluent}
	 */
	spatialIndex (columns, name = null) {
		return this._indexCommand('spatialIndex', columns, name);
	}

    /**
     * Execute the blueprint against the database.
     *
     * @public
     * @param  {Apricot/Database/Connection} connection
     * @param  {Apricot/Database/Schema/Grammars/Grammar} grammar
     */
    build (connection, grammar) {
        return Promise.all(
            this.toSql(connection, grammar)
                .map(statement => {
                    return connection.statement(statement);
                })
        );
    }

    /**
     * Get the raw SQL statements for the blueprint.
     *
     * @public
     * @param  {Apricot/Database/Connection} connection
     * @param  {Apricot/Database/Schema/Grammars/Grammar} grammar
     */
    toSql (connection, grammar) {
        this._addImpliedCommands(grammar);

		let statements = [];

		// Each type of command has a corresponding compiler function on the schema
		// grammar which is used to build the necessary SQL statements to build
		// the blueprint element, so we'll just call that compilers function.
		this._ensureCommandsAreValid(connection);

		this._commands.forEach(command => {
			let method = `compile${ command.name.ucfirst() }`;

			if (method in grammar && Function.isFunction(grammar[method])) {
				let sql = grammar[method](this, command, connection);

				if (sql) {
					statements.push(sql);
				}
			}
		});

		return statements;
    }

	/**
	 * Add the fluent commands specified on any columns.
	 *
	 * @public
	 * @param {Apricot/Database/Grammar} grammar
	 */
	addFluentCommands (grammar) {
		this._columns
			.forEach(column => {
				grammar.fluentCommands
					.forEach(commandName => {
						let attributeName = commandName.lcfirst();

						if (!(attributeName in column)) {
							return;
						}

						let value = column[attributeName];
						this._addCommand(commandName, {value, column});
					});
			});
	}

	//todo impliment after sqllite
	/**
	 * Ensure the commands on the blueprint are valid for the connection type.
	 *
	 * @protected
	 * @param {Apricot/Database/Connection} connection
	 */
	_ensureCommandsAreValid (connection) {
		// if ($connection instanceof SQLiteConnection &&
		// $this->commandsNamed(['dropColumn', 'renameColumn'])->count() > 1) {
		// throw new BadMethodCallException(
		// 	"SQLite doesn't support multiple calls to dropColumn / renameColumn in a single modification."
		// );
		// }
	}

    /**
     * Add a new command to the blueprint.
     *
     * @param {string} name
     * @param {Object} parameters
     * @return {Apricot/Support/Fluent}
     */
    _addCommand (name, parameters = {}) {
        let command = this._createCommand(name, parameters);
        this._commands.push(command);

        return command;
    }

    /**
     * Add the commands that are implied by the blueprint's state.
     *
     * @protected
	 * @param {Apricot/Database/Grammar} grammar
     */
    _addImpliedCommands (grammar) {
        if (this.addedColumns.length > 0 && !this.creating) {
            this._commands.unshift(this._createCommand('add'));
        }

        if (this.changedColumns.length > 0 && !this.creating) {
            this._commands.unshift(this._createCommand('change'));
        }

        this._addFluentIndexes();
        this.addFluentCommands(grammar);
    }

    /**
     * Add the index commands fluently specified on columns.
     *
     * @protected
     */
    _addFluentIndexes () {
        this._columns.forEach(column => {
            ['primary', 'unique', 'index', 'spatialIndex'].forEach(index => {
				let method = `method${ index.ucfirst() }`

            	if (column[method] === true) {
                    this[index](column.name);
                } else if (index in column) {
                    this[index](column.name, column[method]);
                }
            });
        });
    }

    /**
     * Create a new Fluent command.
     *
     * @protected
     * @param  {String} name
     * @param  {Object} parameters
     * @return {Apricot/Support/Fluent}
     */
    _createCommand (name, parameters = {}) {
        return new Fluent(Object.assign({name}, parameters));
    }

	/**
	 * Add a new index command to the blueprint.
	 *
	 * @protected
	 * @param {String} type
	 * @param {String|Array<String>} columns
	 * @param {String} index
	 * @param {String|Null} algorithm
	 * @return {Apricot/Support/Fluent}
	 */
	_indexCommand (type, columns, index, algorithm = null) {
		columns = Array.isArray(columns) ? columns : [columns];

		// If no name was specified for this index, we will create one using a basic
		// convention of the table name, followed by the columns, followed by an
		// index type, such as primary or index, which makes the index unique.
		index = index !== null ? index : this._createIndexName(type, columns);

		return this._addCommand(type, {index, columns, algorithm});
	}

	/**
	 * Create a default index name for the table.
	 *
	 * @protected
	 * @param {String} type
	 * @param {Array<String>} columns
	 * @return {String}
	 */
	_createIndexName (type, columns) {
		return `${ this.table }_${ columns.join('_') }_${ type }`.toLowerCase()
			.replace(/-|\./g, '_');
	}
}

module.exports = Blueprint;