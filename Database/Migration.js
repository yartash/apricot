class Migration {
    get connection () {
        return false;
    }

    get withinTransaction () {
    	return this._withinTransaction;
	}

    constructor () {
		/**
		 * Enables, if supported, wrapping the migration within a transaction.
		 *
		 * @protected
		 * @property {Boolean}
		 */
		this._withinTransaction = true;
	}
}

module.exports = Migration;