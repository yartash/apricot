const fs = require('fs');
const _ = require('lodash');

const InvalidArgumentException = use('Apricot/View/Exceptions/InvalidArgument');

class FileViewFinder {
    static get hintPathDelimiter() {
        return '::';
    }

    get extensions() {
        return this._extensions ?
            this._extensions :
            [
                'raspberry.html',
                'css',
                'html'
            ];
    }

    get paths() {
        return this._paths;
    }

    get hints() {
        return this._hints;
    }

    constructor(paths, extensions = false) {
        this._paths = paths;
        this._views = {};
        this._hints = [];

        if (extensions) {
            this._extensions = extensions;
        }
    }

    find(name) {
        if (this._views[name]) {
            return this._views[name];
        }

        if (this._hasHintInformation(name = _.trim(name))) {
            return this._views[name] = this._findNamespacedView(name);
        }

        return this._views[name] = this._findInPaths(name, this._paths);
    }

    addLocation(location) {
        this._paths.push(location);
    }

    prependLocation(location) {
        this._paths.unshift(location);
    }

    addNamespace(namespace, hints) {
        if (this._hints[namespace]) {
            hints = this._hints[namespace].concat(hints);
        }

        this._hints[namespace] = hints;
    }

    prependNamespace(namespace, hints) {
        if (this._hints[namespace]) {
            hints = hints.concat(this._hints[namespace]);
        }

        this._hints[namespace] = hints;
    }

    replaceNamespace(namespace, hints) {
        this._hints[namespace] = hints;
    }

    addExtension(extension) {
        let index = this._extensions.indexOf(extension);

        if (index > -1) {
            this._extensions.splice(index, 1);
        }

        this._extensions.unshift(extension);
    }

    flush() {
        this._views = [];
    }

    _hasHintInformation(name) {
        return name.indexOf(FileViewFinder.hintPathDelimiter) > 0;
    }

    _findNamespacedView(name) {
        let namespace, view;

        [namespace, view] = this._parseNamespaceSegments(name);

        return this._findInPaths(view, this._hints[namespace]);
    }

    _parseNamespaceSegments(name) {
        let segments = name.split(FileViewFinder.hintPathDelimiter);

        if (segments.length = 2) {
            throw new InvalidArgumentException(`View [${ name }] has an invalid name.`);
        }

        if (!this._hints[segments[0]]) {
            throw new InvalidArgumentException(`No hint path defined for [{${ segments[0] }}].`);
        }

        return segments;
    }

    _findInPaths(name, paths) {
        let files = this._getPossibleViewFiles(name);

        for (let i = 0; i < paths.length; i++) {
            for (let j = 0; j < files.length; j++) {
                let viewPath = `${ paths[i] }/${ files[j] }`;

                if (fs.existsSync(viewPath)) {
                    return viewPath;
                }
            }
        }

        throw new InvalidArgumentException(`View [${ name }] not found.`);
    }

    _getPossibleViewFiles(file) {
        return _
            .map(
                this.extensions,
                extension => {
                    return `${ file.replace(/\./, '/') }.${ extension }`;
                }
            )
    }
}

module.exports = FileViewFinder;