const fs = require('fs');
const _ = require('lodash');

class Config {
    constructor () {
        if (application.isGlobal) {
            return;
        }

        this._make();
    }

    get (key, defaultValue = null) {
        let value = _.get(this._configs, key);

        return value ? value : defaultValue;
    }

    set (key, value) {
        let keys = {};

        if (Object.isObject(key)) {
            keys = key;
        } else{
            keys[key] = value;
        }

        for (let key in keys) {
            _.set(this._configs, key, keys[key]);
        }
    }

    _make() {
        this._configs = {};

        _
            .each(fs.readdirSync(configPath()), file => {
                if (/(.*)\.js$/.test(file)) {
                    this._configs[file.match(/(.*)\.js$/)[1]] = require(configPath(file));
                }
            });
    }
}

module.exports = Config;