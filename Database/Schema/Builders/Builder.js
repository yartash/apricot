const Blueprint = use('Apricot/Database/Schema/Blueprint');

const LogicException = use('Apricot/Exceptions/Logic');

class Builder {
    /**
     * Set the Schema Blueprint resolver callback.
     *
     * @param {Function} resolver
     */
    set blueprintResolver (resolver) {
        this._resolver = resolver;
    }

	constructor (connection) {
		/**
		 * @protected
		 */
		this._connection = connection;
		/**
		 * @protected
		 */
		this._grammar = connection.schemaGrammar;
	}

	/**
	 * Determine if the given table exists.
	 *
	 * @param  {string} table
	 * @return {boolean}
	 */
	hasTable (table) {
		return new Promise(resolve => {
            table = `${ this._connection.tablePrefix }${ table }`;

            this._connection
				.select(this._grammar.compileTableExists, [table])
				.then(result => {
					resolve(result.rows.length > 0);
				});
        });
	}

    /**
     * Create a new table on the schema.
     *
     * @param {string} table
     * @param {Function} callback
     */
    create (table, callback) {
        return new Promise((resolve, reject) => {
            let blueprint = this._createBlueprint(table);
            blueprint.create();

            callback(blueprint);
            
            this._build(blueprint)
                .then(
                	result => {
                    	resolve(result);
                	},
					error => {
                		reject(error);
					}
				);
        });
    }

    /**
     * Drop a table from the schema if it exists.
     *
     * @public
     * @param {String} table
     */
    dropIfExists (table) {
        return new Promise((resolve, reject) => {
            let blueprint = this._createBlueprint(table);

            blueprint.dropIfExists();

            this._build(blueprint)
                .then(
                    result => {
                        resolve(result);
                    },
                    error => {
                        reject(error);
                    }
                );
        });
    }

    /**
     * Drop all tables from the database.
     *
     * @public
     * @throws \LogicException
     */
    dropAllTables () {
        throw new LogicException('This database driver does not support dropping all tables.');
    }

    /**
     * Execute the blueprint to build / modify the table.
     *
     *
     * @protected
     * @param  {Apricot/Database/Schema/Blueprint} blueprint
     */
    _build (blueprint) {
        return blueprint.build(this._connection, this._grammar);
    }

    /**
     * Create a new command set with a Closure.
     *
     * @protected
     * @param  string  $table
     * @param  {(Function|null)} callback
     * @return {Apricot/Database/Schema/Blueprint}
     */
    _createBlueprint (table, callback = null) {
        if (this._resolver) {
            return this._resolver(table, callback);
        }

        return new Blueprint(table, callback);
    }
}

module.exports = Builder;