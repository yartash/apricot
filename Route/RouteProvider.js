const BaseProvider = use('Apricot/Application/BaseProvider');

class RouteProvider extends BaseProvider {
    register() {
        this.app.singleton('route', () => {
            const Router = use('Apricot/Route/Router');

            return new Router();
        });
    }
}

module.exports = RouteProvider;