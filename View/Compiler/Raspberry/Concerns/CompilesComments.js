class CompilesComments {
    * _compileComments(content) {
        let pattern = `${ this.contentTags[0] }--([\s\S]*?)--${ this.contentTags[1] }`;

        return content.replace(new RegExp(pattern, 'gm'), '');
    }
}

module.exports = CompilesComments;