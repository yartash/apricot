Object.defineProperty(
    Number,
    'isNumber',
    {
        value: value => {
            return Object.prototype.toString.call(value) === '[object Number]';
        },
        configurable: false,
        writable: false,
        enumerable: false
    }
);

Object.defineProperty(
	Number,
	'parseNumber',
	{
		value: value => {
			return /^\d+\.\d+$/.test(value) ? parseFloat(value) : parseInt(value);
		},
		configurable: false,
		writable: false,
		enumerable: false
	}
);
