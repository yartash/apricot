const Constant = use('Constant');
const Member = use('Member');
const Method = use('Method');

class Reflection {
	/**
	 * @public
	 * @return {Array<Apricot/Reflection/Member>}
	 */
	get members () {
		return this._members;
	}

	/**
	 * @public
	 * @return {Array<Apricot/Reflection/Constant>}
	 */
	get constants () {
		return this._constants;
	}

	/**
	 * @public
	 * @return {Array<Apricot/Reflection/Method>}
	 */
	get methods () {
		return this._methods;
	}

	constructor (data) {
		this._data = data;
		this._constants = [];
		this._members = [];
		this._methods = [];

		this._parse();
	}

	/**
	 * @public
	 * @param {string} name
	 * @return {Apricot/Reflection/Constant}
	 */
	getConstant (name) {
		return this._constants
			.find(constant => {
				return constant.name == name;
			});
	}

	/**
	 * @public
	 * @param {string} name
	 * @return {Apricot/Reflection/Member}
	 */
	getMember (name) {
		return this._members
			.find(member => {
				return member.name == name;
			});
	}

	/**
	 * @public
	 * @param {string} name
	 * @return {Apricot/Reflection/Method}
	 */
	getMethod (name) {
		return this._methods
			.find(method => {
				return method.name == name;
			});
	}

	/**
	 * @private
	 * @return {void}
	 */
	_parse() {
		this._data
			.forEach(item => {
				switch (item.kind) {
					case 'constant':
						this._constants
							.push(new Constant(item));
						break;
					case 'member':
						this._members
							.push(new Member(item));
						break;
					case 'function':
						this._methods
							.push(new Method(item));
						break;

				}
			});
	}
}

module.exports = Reflection;