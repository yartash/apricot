const _ = require('lodash');

const BaseProvider = use('Apricot/Application/BaseProvider');
const Engine = use('Apricot/View/Engine');
const CompilerEngine = use('Apricot/View/Engine/Compiler');
const FileEngine = use('Apricot/View/Engine/File');
const RaspberryCompiler = use('Apricot/View/Compiler/Raspberry');
const FileViewFinder = use('Apricot/View/FileViewFinder');
const Factory = use('Apricot/View/Factory');

class ViewProvider extends BaseProvider {
    get engine() {
        return ['raspberry', 'file'];
    }

    register() {
        this.app.singleton('view', () => {
            let resolver = use('view.engine.resolver');
            let finder = use('view.finder');
            let env = new Factory(resolver, finder);
            // const Env = use('Apricot/Env');
            //
            return env;
        });

        this.app.bind('view.finder', () => {
            const Config = use('config');

            return new FileViewFinder(Config.get('view.paths'));
        });

        this.app.singleton('view.engine.resolver', () => {
            let resovler = new Engine();

            _
                .each(
                    this.engine,
                    engine => {
                        this[`_register${ _.upperFirst(engine) }Engine`](resovler)
                    }
                );

            return resovler;
        });
    }

    _registerRaspberryEngine(resolver) {
        this.app.singleton('raspberry.compiler', () => {
            return new RaspberryCompiler(use('Config').get('view.compiled'))
        });

        resolver.register(
            'raspberry',
            () => {
                return new CompilerEngine(use('raspberry.compiler'));
            }
        );
    }

    _registerFileEngine(resolver) {
        resolver.register(
            'file',
            () => {
                return new FileEngine();
            }
        );
    }
}

module.exports = ViewProvider;