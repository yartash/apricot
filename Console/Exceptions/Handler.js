class Handler {
    get error() {
        return this._error;
    }
    
    constructor(error) {
        this._error = error;
    }

    * render() {
        return this.error.message;
    }

    * execute() {
        return yield * this.render();
    }
}

module.exports = Handler;