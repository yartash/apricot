const _ = require('lodash');

class UriValidator {
    match(route, request) {
        let path = request.url == '/' ?
            '/' :
            `/${ _.trimStart(request.url, '/') }`

        return route
            .regex
            .test(decodeURIComponent(path));
    }
}

module.exports = UriValidator;