const Command = use('Apricot/Console/Commands/Migrate');

const Migrator = use('Apricot/Console/Commands/Migrate/Migrator');

class Reset extends Command {
	/**
	 * The console command name.
	 *
	 * @public
	 * @return {String}
	 */
	get signature () {
		return 'migrate:reset';
	}

	get name () {
		return 'Migrate Reset';
	}

	/**
	 * The console command description.
	 *
	 * @return {String}
	 */
	get description () {
		return 'Rollback all database migrations.';
	}

	get optionsDefinition () {
		return {
			database: {
				description: 'The database connection to use.',
				short: 'd',
			},
			force: {
				description: 'Force the operation to run when in production.',
				short: 'f'
			},
			path: {
				description: 'The path of migrations files to be executed.',
				short: 'p'
			},
			realpath: {
				description: 'Indicate any provided migration file paths are pre-resolved absolute paths.',
				short: 'r'
			},
            pretend: {
                description: 'Dump the SQL queries that would be run.',
                short: 'a'
            }
		};
	}

	get traits () {
		return [
			'Confirmable'
		];
	}

    constructor () {
        super();

        this._migrator = Migrator.instance;
    }

	* hundler () {
		if (!(yield * this.confirmToProceed())) {
			return;
		}

        this._migrator.connection = this.option('database');

        // First, we'll make sure that the migration table actually exists before we
        // start trying to rollback and re-run all of the migrations. If it's not
        // present we'll just bail out with an info message for the developers.
        let exists = yield * this._migrator.repositoryExists();

        if (!exists) {
            return this.comment('Migration table not found.');
        }

        yield * this._migrator.reset(this.migrationPaths, this.option('pretend'));

        // Once the migrator has run we will grab the note output and send it out to
        // the console screen, since the migrator itself functions without having
        // any instances of the OutputInterface contract passed into the class.
        this._migrator
            .notes
            .forEach(note => {
                this[note.style](note.message);
            });
	}
}

module.exports = Reset;