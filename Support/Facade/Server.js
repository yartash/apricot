class Server {
    static get facadeAccessor() {
        return 'server';
    }
}

module.exports = Server;