const BaseKernel = use('Apricot/Console/Kernel');

class Kernel extends BaseKernel {
    get commands () {
        return [
            // 'App/Console/Commands/SomeCommand',
        ];
    }
}

module.exports = Kernel;