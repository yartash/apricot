class InvalidArgument extends Error {
    constructor(message) {
        super(message);
    }
}

module.exports = InvalidArgument;