const _ = require('lodash');

class RouteCollection {
    get routes() {
        return this._allRoutes;
    }

    constructor() {
        this._routes = {};
        this._allRoutes = [];
        this._nameList = {};
        this._actionList = {};
    }

    add(route) {
        this._addToCollection(route);
        this._addLookup(route);

        return route;
    }

    match(request) {
        let route = this._check(this._get(request.method), request);

        if (route !== undefined) {
            return route.bind(request);
        }

        return false;
    }

    _addToCollection(route) {
        let domainAndUrl = route.domain + route.url;

        _
            .each(
                route.methods,
                method => {
                    if (this._routes[method] === undefined) {
                        this._routes[method] = {};
                    }

                    this._routes[method][domainAndUrl] = route;
                    this._allRoutes.push({
                        key: method + domainAndUrl,
                        value: route
                    })
                }
            )
    }

    _addLookup(route) {
        let action = route.action;

        if (action['as'] !== undefined) {
            this._nameList[action['as']] = route;
        }

        if (action['controller'] !== undefined) {
            this._actionList[_.trim(action['controller'], '\\')] = route;
        }
    }

    _get(method) {
        if (method == null) {
            return this._routes;
        }

        return this._routes[method] !== undefined ?
            this._routes[method] :
            [];
    }

    _check(routes, request) {
        return _
            .find(
                routes,
                route => {
                    return route.matches(request);
                }
            )
    }
}

module.exports = RouteCollection;