const _ = require('lodash');

const FileViewFinder = use('Apricot/View/FileViewFinder');
const View = use('Apricot/View');

const InvalidArgumentException = use('Apricot/View/Exceptions/InvalidArgument');

class Factory {
    get extensions() {
        return {
            'raspberry.html': 'raspberry',
            'html': 'html',
            'css': 'file'
        }
    }

    get shared() {
        return this._shared;
    }

    get traits() {
        return [
            'ManagesAsset'
        ];
    }

    constructor(engines, finder) {
        this._engines = engines;
        this._composers = {};
        this._finder = finder;
        this._shared = {};

        this._renderCount = 0;

        this._registerTrait();
        this.share('__env', this);
    }

    share(key, value = null) {
        let keys = {};

        if (_.isObject(key)) {
            keys = key;
        } else {
            keys[key] = value;
        }

        _
            .each(
                keys,
                (value, key) => {
                    this._shared[key] = value;
                }
            );

        return value;
    }

    file(path, data = {}, mergeData = {}) {
        data = _.assignIn(mergeData, data);
    }

    composer(view, generator) {
        if (!this._composers[view]) {
            this._composers[view] = new Set();
        }

        this._composers[view].add(generator);
    }

    make(view, data = {}, mergeData = {}) {
        view = this._normalizeViewName(view);
        data = _.assignIn(mergeData, data);

        let path = this._finder.find(view);

        return new View(
            this,
            this.getEngineFromPath(path),
            view,
            path,
            data,
            this._composers[view]
        );
    }

    incrementRender() {
        this._renderCount++;
    }

    decrementRender() {
        this._renderCount--;
    }

    flushStateIfDoneRendering() {
        //to do add
    }

    getEngineFromPath(path) {
        let extension = this._getExtension(path);

        if (!extension) {
            throw new InvalidArgumentException(`Unrecognized extension in file: ${ path }`);
        }

        return this._engines.resolve(this.extensions[extension]);
    }

    _normalizeViewName(name) {
        let delimiter = FileViewFinder.hintPathDelimiter;

        if (name.indexOf(delimiter) === -1) {
            return name.replace(new RegExp('\/', 'g'), '.');
        }

        let namespace;
        [namespace, name] = name.split(delimiter);

        return `${ namespace }.${ delimiter }.${ name.replace(new RegExp('\/', 'g'), '.') }`;
    }

    _getExtension(path) {
        let extensions = _.keys(this.extensions);

        return _
            .find(
                extensions,
                extension => {
                    return path.endsWith(`.${ extension }`);
                }
            );
    }

    _registerTrait() {
        _
            .each(
                this.traits,
                trait => {
                    let traitClass = use(`Concerns/${ trait }`);
                    _
                        .chain(
                            Object
                                .getOwnPropertyNames(
                                    Object
                                        .getPrototypeOf(new traitClass())
                                )
                        )
                        .filter(
                            method => {
                                return ['constructor'].indexOf(method) == -1;
                            }
                        )
                        .each(
                            method => {
                                this[method] = traitClass.prototype[method];
                            }
                        )
                        .value();
                }
            );
    }
}

module.exports = Factory;