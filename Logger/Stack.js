class Stack {
    get method() {
        return this._method;
    }

    get file() {
        return this._file;
    }

    get line() {
        return this._line;
    }

    get position() {
        return this._position;
    }

    constructor() {
        this._make();
    }

    _make() {
        let stacks = (new Error())
            .stack
            .split(/\n\s+/);
        let info = stacks[8]
            .match(/^at\s*([\w|\\.| ]+)\s\((.+):(\d+):(\d+)\)$/);

        info.shift();

        this._method = info[0];
        this._file = info[1];
        this._line = info[2];
        this._position = info[3];
    }
}

module.exports = Stack;