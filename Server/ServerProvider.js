const BaseProvider = use('Apricot/Application/BaseProvider');

class ServerProvider extends BaseProvider {
    register() {
        this.app.singleton('server', () => {
            const Server = use('Apricot/Server');

            return new Server;
        });
    }
}

module.exports = ServerProvider;