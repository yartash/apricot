const process = require('process');

const Input = use('Apricot/Console/Input');
const Output = use('Apricot/Console/Output');

const GeneratorHandler = use('Apricot/Support/GeneratorHandler');

class Peach {
    constructor() {
        this._commands = [];
        this._output = Output.instance;
    }

    addCommand(instance) {
        this._commands.push(instance);
    }
    
    match(commandText) {
        return this._commands
            .find(command => {
                return command.check(commandText);
            });
    }

    run () {
        process.on('unhandledRejection', error => {
			(new GeneratorHandler()).executor(this._exceptionHandle(error));
        });
        
        return new Promise(resolve => {
			(new GeneratorHandler(resolve)).executor(this._handle());
        });
    }
    
    * _handle () {
        let input = new Input();
        let command = this.match(input.text);
        
        if (!command) {
            return yield * this._help();
        }

        command.bindInput(input);
        
        yield * command.handler();

		return this._output.close();
    }

    * _exceptionHandle(error) {
		const ExceptionHandler = use(
		    application.isGlobal ?
                'Apricot/Console/Exceptions/Handler' :
                'App/Exceptions/Handlers/Command'
        );
		let handler = new ExceptionHandler(error);
		let result = yield * handler.execute();

		//todo

		this._output.error(result);

		return this._output.close();
    }
    
    * _help () {
        console.log('kkkk');
    }
}

module.exports = Peach;