const BaseResponse = use('Apricot/Http/Response');

class Response extends BaseResponse {
    * setContent(content) {
        if (content.render) {
            content = yield * content.render();
        }

        yield * super.setContent(content);

        return this;
    }
}

module.exports = Response;