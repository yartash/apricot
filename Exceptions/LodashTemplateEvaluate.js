class LodashTemplateEvaluate extends Error {
    get lodashError() {
        return this._error;
    }

    constructor(error) {
        super('Lodash template evaluation error.');

        this._error = error
    }
}

module.exports = LodashTemplateEvaluate;