const Connection = use('Connection');
const SchemaGrammar = use('Apricot/Database/Schema/Grammars/PostgresGrammar');
const PostgresBuilder = use('Apricot/Database/Schema/Builders/PostgresBuilder');

class PostgresConnection extends Connection {
	/**
	 * @protected
	 * @return {*}
	 */
	get defaultSchemaGrammar () {
		return this.withTablePrefix(new SchemaGrammar());
	}

	/**
	 * Get a schema builder instance for the connection.
	 *
	 * @public
	 * @return {Apricot/Database/Schema/Builders/PostgresBuilder}
	 */
	get schemaBuilder () {
		if (!this._schemaGrammar) {
			this.useDefaultSchemaGrammar;
		}

		return new PostgresBuilder(this);
	}

    disconnect() {
        let g = 0;
    }

    /**
     * Commit the active database transaction.
     *
     * @public
     */
    commit () {
    	return new Promise(resolve => {
    		if (this._transactions == 1) {
    			this.client
					.then(client => {
						return client.query('COMMIT')
					})
					.then(() => {
						this._transactions = Math.max(0, this._transactions - 1);

						//todo
						//$this->fireConnectionEvent('committed');

						resolve();
					});
			}
		});
    }

    _causedByLostConnection(error) {
        let message = error.message;

        return message.contains([
            'connect ECONNREFUSED',
            'write ECONNRESET'
        ]);
    }

	/**
	 * Create a transaction within the database.
	 *
	 * @protected
	 */
	_createTransaction () {
		return new Promise(resolve => {
			if (this._transactions == 0) {
				this.client
					.then(client => {
						client.query('BEGIN')
							.then(result => {
								resolve();
							}, error => {
								this._handleBeginTransactionException(error)
									.then(() => {
										resolve();
									});
							});
					});
			} else if (this._transactions >= 1 && this.queryGrammar.supportsSavepoints) {
				this._createSavepoint();
			}
		});
	}

    /**
     * Perform a rollback within the database.
     *
     * @protected
     * @param {Int} toLevel
     */
    _performRollBack (toLevel) {
		if (toLevel == 0) {
			return this.client.then(client => {
				return client.query('ROLLBACK');
			});
		} else if (this.queryGrammar.supportsSavepoints) {
			return this.clinet.then(client => {
				return client.query(this.queryGrammar.compileSavepointRollBack(`trans${ this._transactions + 1 }`));
			});
		}
    }
}

module.exports = PostgresConnection;