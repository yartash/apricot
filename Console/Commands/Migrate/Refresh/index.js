const Command = use('Apricot/Console/Commands/Migrate');

const Migrator = use('Apricot/Console/Commands/Migrate/Migrator');

class Refresh extends Command {
	/**
	 * The console command name.
	 *
	 * @public
	 * @return {String}
	 */
	get signature () {
		return 'migrate:refresh';
	}

	get name () {
		return 'Migrate Refresh';
	}

	/**
	 * The console command description.
	 *
	 * @return {String}
	 */
	get description () {
		return 'Reset and re-run all migrations.';
	}

    get optionsDefinition () {
        return {
            database: {
                description: 'The database connection to use.',
                short: 'd',
            },
            force: {
                description: 'Force the operation to run when in production.',
                short: 'f'
            },
            path: {
                description: 'The path of migrations files to be executed.',
                short: 'p'
            },
            realpath: {
                description: 'Indicate any provided migration file paths are pre-resolved absolute paths.',
                short: 'r'
            },
            seed: {
                description: 'Indicates if the seed task should be re-run.',
                short: 's'
            },
            seeder: {
                description: 'The class name of the root seeder.',
                short: 't'
            },
            step: {
                description: 'Force the migrations to be run so they can be rolled back individually.',
                short: 'b'
            },
        };
    }

	get traits () {
		return [
			'Confirmable'
		];
	}

    constructor () {
        super();

        this._migrator = Migrator.instance;
    }

	* hundler () {
		if (!(yield * this.confirmToProceed())) {
			return;
		}

        // Next we'll gather some of the options so that we can have the right options
        // to pass to the commands. This includes options such as which database to
        // use and the path to use for the migration. Then we'll run the command.
        let database = this.option('database');
        let path = this.option('path');
        let force = this.option('force');

        // If the "step" option is specified it means we only want to rollback a small
        // number of migrations before migrating again. For example, the user might
        // only rollback and remigrate the latest four migrations instead of all.
        let step = this.option('step', 0);

        if (step > 0) {
            yield * this._runRollback(database, path, step, force);
        } else {
            yield * this._runReset(database, path, force);
        }

        // The refresh command is essentially just a brief aggregate of a few other of
        // the migration commands and just provides a convenient wrapper to execute
        // them in succession. We'll also see if we need to re-seed the database.
        yield * this.call(
            'migrate',
            [],
            {database, path, force}
        );

        if (this.option('seed') || this.option('seeder')) {
            yield * this._runSeeder(database);
        }
	}

    /**
     * Run the rollback command.
     *
     * @protected
     * @param {String} database
     * @param {String} path
     * @param {Boolean} step
     * @param {Boolean} force
     */
    * _runRollback (database, path, step, force) {
        yield * this.call(
            'migrate:rollback',
            [],
            {database, path, step, force}
        );
    }

    /**
     * Run the reset command.
     *
     * @protected
     * @param {String} database
     * @param {String} path
     * @param {Boolean} force
     */
    * _runReset (database, path, force) {
        yield * this.call(
            'migrate:reset',
            [],
            {database, path, force}
        );
    }

    /**
     * Run the database seeder command.
     *
     * @param {String} database
     */
    * _runSeeder (database) {
        yield * this.call(
            'db:seed',
            [],
            {database, path, force}
        );
    }
}

module.exports = Refresh;