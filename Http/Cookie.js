const _ = require('lodash');
const dateFormat = require('dateformat');

const InvalidArgumentException = use('Apricot/Exceptions/InvalidArgument');

class Cookie {
    static get sameSiteLax() {
        return 'lax';
    }

    static get sameSiteStrict() {
        return 'strict';
    }

    static fromString(cookie, decode = true) {
        let data = {
            expires: 0,
            path: '/',
            domain: null,
            secure: false,
            httponly: false,
            samesite: null
        };
        let key, value;
        _
            .each(
                cookie.split(';'),
                option => {
                    if (option.indexOf('=') === -1) {
                        key = _.trim(option);
                        value = true;
                    } else {
                        [key, value] = _.trim(option).split('=');
                        key = _.trim(key);
                        value = _.trim(value);
                    }

                    if (!data['name']) {
                        data['name'] = decode ? decodeURIComponent(key) : key;
                        data['value'] = true === value ? null : (decode ? decodeURIComponent(value) : value);

                        return true;
                    }

                    switch (key = key.toLowerCase()) {
                        case 'name':
                        case 'value':
                            break;
                        case 'max-age':
                            data['expires'] = new Date().getTime() + parseInt(value);
                            break;
                        default:
                            data[key] = value;
                            break;
                    }
                }
            );

        return new Cookie(
            data.name,
            data.value,
            data.expires,
            data.path,
            data.domain,
            data.secure,
            data.httponly,
            data.samesite
        );
    }

    get isSecure() {
        return this._secure;
    }

    get sameSite() {
        return this._sameSite;
    }

    get isHttpOnly() {
        return this._httpOnly;
    }

    get isCleared() {
        return this._expire < (new Date()).getTime();
    }

    get name() {
        return this._name;
    }

    get value() {
        return this._value;
    }

    get expiresTime() {
        return this._expire;
    }

    get path() {
        return this._path;
    }

    get domain() {
        return this._domain;
    }

    get maxAge() {
        return this._expire !== 0 ?
            this._expire - (new Date()).getTime() :
            0;
    }

    constructor(
        name,
        value = null,
        expire = 0,
        path = '/',
        domain = null,
        secure = false,
        httpOnly = true,
        sameSite = null
    ) {
        if (/[=,; \t\r\n\013\104]/.test(name)) {
            throw new InvalidArgumentException(`The cookie name "${ name }" contains invalid characters.`)
        } else if (name === '') {
            throw new InvalidArgumentException('The cookie name cannot be empty.');
        }

        if (expire instanceof Date) {
            expire = expire.getTime();
        } else if (!_.isNumber(expire)) {
            expire = (new Date(expire)).getTime();

            if (_.isNaN(expire)) {
                throw new InvalidArgumentException('The cookie expiration time is not valid.');
            }
        }

        this._name = name;
        this._value = value;
        this._domain = domain;
        this._expire = expire > 0 ? parseInt(expire) : 0;
        this._path = path !== '' ? path : '/';
        this._secure = secure;
        this._httpOnly = httpOnly;

        if (sameSite !== null) {
            sameSite = sameSite.toLowerCase();
        }

        if ([Cookie.sameSiteLax, Cookie.sameSiteStrict, null].indexOf(sameSite) == -1) {
            throw new InvalidArgumentException('The "sameSite" parameter value is not valid.');
        }

        this._sameSite= sameSite;
    }

    toString() {
        let result = this.name  + '=';

        if (this.value.toString() === '') {
            result += `deleted; expires=${ dateFormat((new Date()).getTime() - 31536001000, 'ddd, dd-mm-yyyy HH:MM:ss Z') }; max-age=-31536001`;
        } else {
            result += this.value;

            if (this.expiresTime !== 0) {
                result += `; expires=${ dateFormat(this.expiresTime, 'ddd, dd-mm-yyyy HH:MM:ss Z') }; max-age=${ this.maxAge }`;
            }
        }

        if (this.path) {
            result += `; path=${ this.path }`;
        }

        if (this.domain) {
            result += `; domain=${ this.domain }`;
        }

        if (this.isSecure === true) {
            result += '; secure';
        }

        if (this.isHttpOnly === true) {
            result += '; httponly';
        }

        if (this.sameSite !== null) {
            result += `; samesite=${ this.sameSite }`;
        }

        return result;
    }
}

module.exports = Cookie;