class NoRouteAction extends Error {
    constructor(url) {
        super(`Route for [${ url }] has no action.`);
    }
}

module.exports = NoRouteAction;