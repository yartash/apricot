const fs = require('fs');
const _ = require('lodash');

const LodashTemplateEvaluateException = use('Apricot/Exceptions/LodashTemplateEvaluate');

class Lodash {
    get(path, data = {}) {
        return this._evaluatePath(path, data);
    }

    _evaluatePath(path, data) {
        return new Promise(resolve => {
            try {
                var buffer = _
                    .trimStart(
                        _
                            .template(
                                fs.readFileSync(path, 'utf8')
                            )(data)
                    );

                resolve(buffer);
            } catch (error) {
                Promise.reject(new LodashTemplateEvaluateException(error));
            }
        });
    }
}

module.exports = Lodash;