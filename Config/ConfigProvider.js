const BaseProvider = use('Apricot/Application/BaseProvider');

class ConfigProvider extends BaseProvider {
    register() {
        this.app.singleton('config', () => {
            const Config = use('Apricot/Config');

            return new Config;
        });
    }
}

module.exports = ConfigProvider;