const path = require('path');
const fs = require('fs');
const _ = require('lodash');

class Application {
    static set isGlobal (value) {
        Application._isGlobal = value;
    }
    
    static get isGlobal () {
        return Application._isGlobal;
    }
    
     get isGlobal () {
        return Application._isGlobal;
    }
    
    static get instance() {
        if (!Application._instance) {
            Application._instance = new Application();
        }

        return Application._instance;
    }

    get appNamespace() {
        return 'App';
    }

    get frameworkNamespace() {
        return 'Apricot';
    }

    get providers() {
        if (!this._providerCache) {
            let providers = new Set();
            _
                .each(this._bootstarp.providers, provider => {
                    providers.add(provider);
                });

            this._providerCache = Array.from(providers);
        }

        return this._providerCache
    }
    
    get frameworkVersion () {
		try {
			let json = JSON.parse(fs.readFileSync(this.frameworkPath('package.json')));

			return json && json.version ? json.version : '0';
		} catch (error) {
			return '0';
		}
	}

    constructor() {
        if (this.isGlobal) {
            this._bootstarp = {
                providers: [
					'Apricot/FileSystem/FileSystemProvider',
					'Apricot/Console/PeachProvider'
                ],
                aliases: {
					File: 'Apricot/Support/Facade/File',
					Peach: 'Apricot/Support/Facade/Peach'
                }
            };
        } else {
            let core = require('./App');
            let app = require(path.join(this.basePath(), 'bootstrap', 'app'));
            core.aliases.__proto__ = app.aliases;

            this._bootstarp = {
                providers: core.providers.concat(app.providers),
                aliases: core.aliases
            };
        }
        
        this._providers = {};

        this._setGlobalVariable();
        this.use('Apricot/Helpers');
        this._register();
    }

    frameworkPath (dir) {
        return path.normalize(
            dir ?
                path.join(__dirname, '..', dir) :
                path.join(__dirname, '..')
        );
    }

    basePath(dir) {
        return path.normalize(
            dir ?
                path.join(process.cwd(), dir) :
                process.cwd()
        );
    }

    appPath(dir) {
        return path.normalize(
            dir ?
                path.join(this.basePath(), 'app', dir) :
                path.join(this.basePath(), 'app')
        );
    }

    databasePath(dir) {
        return path.normalize(
            dir ?
                path.join(this.basePath(), 'database', dir) :
                path.join(this.basePath(), 'database')
        );
    }

    resourcePath(dir) {
        return path.normalize(
            dir ?
                path.join(this.basePath(), 'resources', dir) :
                path.join(this.basePath(), 'resources')
        );
    }

    storagePath(dir) {
        return path.normalize(
            dir ?
                path.join(this.basePath(), 'storage', dir) :
                path.join(this.basePath(), 'storage')
        );
    }

    configPath(dir) {
        return path.normalize(
            dir ?
                path.join(this.basePath(), 'config', dir) :
                path.join(this.basePath(), 'config')
        );
    }

    publicPath(dir) {
        return path.normalize(
            dir ?
                path.join(this.basePath(), 'public', dir) :
                path.join(this.basePath(), 'public')
        );
    }

    singleton(namespace, closure) {
        this._bind(namespace, closure, true);
    }

    bind(namespace, closure) {
        this._bind(namespace, closure);
    }

    makeNameSpace(baseNameSpace, toPath) {
        if (toPath.startsWith(this.appNamespace)) {
            return toPath;
        }

        return path.normalize(`${this.appNamespace}/${baseNameSpace}/${toPath}`);
    }

    make(namespace, injectionMap = {}) {
        let type = this._getClassType(namespace);
        let instance;

        if (type === 'provider' || type === 'alias') {
            return this.use(namespace)
        }

        if (type === 'autoLoad') {
            instance = this._autoLoad(namespace)
        }

        if (!this._isClass(instance)) {
            return instance;
        }

        if (!instance.inject || _.size(instance.inject) === 0) {
            return new instance();
        }

        if (instance.inject.length < this._introspect(instance.toString())) {
            throw new Error('Attributes count and injections property count not mach.');
        }

        injectionMap[instance] = instance.inject;

        const resolvedInjections = _.map(instance.inject, injection => {
            if (_.values(injectionMap).indexOf(injection) > - 1) {
                throw new Error('Ցիկլիկ ինյեկցիա');
            }

            return this.make(injection, injectionMap);
        });

        return new (Function.prototype.bind.apply(instance, [null].concat(resolvedInjections)))();
    };

    makeFunction(binding) {
        let parts = binding.split('.');

        if (parts.length !== 2) {
            throw new Error('Unable to make ' + binding)
        }

        let instance = this.make(parts[0]);
        let method = parts[1];

        if (!instance[method]) {
            throw new Error(method + ' does not exists on ' + parts[0])
        }

        return {instance, method};
    }

    use(namespace) {
        let type = this._getClassType(namespace);

        switch (type) {
            case 'provider':
                return this._resolveProvider(this._providers[namespace]);
            case 'autoLoad':
                return this._autoLoad(namespace);
            case 'alias':
                return this._resolveAlias(this._bootstarp.aliases[namespace]);
            default:
                return '';
        }
    }
    
    namesapceToPath (namespace) {
		if (namespace.startsWith(this.appNamespace)) {
			namespace = namespace.replace(this.appNamespace, this.appPath());
		} else if(namespace.startsWith(this.frameworkNamespace)) {
			namespace = namespace.replace(this.frameworkNamespace, this.frameworkPath());
		} else {
		    switch (true) {
                case fs.existsSync(this.databasePath(path.join('seeds', `${ namespace.replace('.js') }.js`))):
                    namespace = this.databasePath(path.join('seeds', namespace));
                    break;
                default:
                    namespace = path.join(this._getCurrentDirectory(), namespace);
                    break;
            }
		}
		
		return path.normalize(namespace);
	}

    _resolveAlias(namespace) {
        let instance = this._autoLoad(namespace);

        return String.isString(instance.facadeAccessor) ?
            this.use(instance.facadeAccessor) :
			instance.facadeAccessor;
    }

    _setGlobalVariable() {
        /**
         * @property { Application }
         * @name global#application
         */
        Object.defineProperty(
            global,
            'application',
            {
                value: this,
                configurable: false,
                writable: false,
                enumerable: false
            }
        );

        /**
         * @property { function }
         * @name global#use
         */
        Object.defineProperty(
            global,
            'use',
            {
                value: this.use.bind(this),
                configurable: false,
                writable: false,
                enumerable: false
            }
        );

        /**
         * @property { function }
         * @name global#basePath
         */
        Object.defineProperty(
            global,
            'basePath',
            {
                value: this.basePath.bind(this),
                configurable: false,
                writable: false,
                enumerable: false
            }
        );

        /**
         * @property { function }
         * @name global#appPath
         */
        Object.defineProperty(
            global,
            'appPath',
            {
                value: this.appPath.bind(this),
                configurable: false,
                writable: false,
                enumerable: false
            }
        );

        /**
         * @property { function }
         * @name global#configPath
         */
        Object.defineProperty(
            global,
            'configPath',
            {
                value: this.configPath.bind(this),
                configurable: false,
                writable: false,
                enumerable: false
            }
        );

        /**
         * @property { function }
         * @name global#databasePath
         */
        Object.defineProperty(
            global,
            'databasePath',
            {
                value: this.databasePath.bind(this),
                configurable: false,
                writable: false,
                enumerable: false
            }
        );

        /**
         * @property { function }
         * @name global#publicPath
         */
        Object.defineProperty(
            global,
            'publicPath',
            {
                value: this.publicPath.bind(this),
                configurable: false,
                writable: false,
                enumerable: false
            }
        );

        /**
         * @property { function }
         * @name global#resourcePath
         */
        Object.defineProperty(
            global,
            'resourcePath',
            {
                value: this.resourcePath.bind(this),
                configurable: false,
                writable: false,
                enumerable: false
            }
        );

        /**
         * @property { function }
         * @name global#storagePath
         */
        Object.defineProperty(
            global,
            'storagePath',
            {
                value: this.storagePath.bind(this),
                configurable: false,
                writable: false,
                enumerable: false
            }
        );
    }

    _register() {
        _
            .each(this.providers, provider => {
                const providerClass = this.use(provider);

                if (providerClass === false) {
                    return;
                }

                const providerInstance = new providerClass();

                providerInstance.register();
                providerInstance.boot();
            });
    }

    _getClassType(namespace) {
        if (!_.isString(namespace)) {
            return 'unknown';
        } else if (this._providers[namespace]) {
            return 'provider';
        } else if (this._bootstarp.aliases[namespace]) {
            return 'alias';
        } else {
            return 'autoLoad';
        }
    }

    _bind(namespace, closure, singleton = false) {
        if (!_.isFunction(closure)) {
            throw new Error('Invalid _arguments, bind expects a callback');
        }

        namespace = namespace.trim();

        this._providers[namespace] = {
            closure: closure,
            singleton: singleton,
            instance: null
        };
    }

    _resolveProvider(provider) {
        if (!provider.singleton) {
            return provider.closure(this);
        }

        provider.instance = provider.instance || provider.closure(this);

        return provider.instance;
    }

    _autoLoad(namespace) {
        let original = namespace;

		namespace = this.namesapceToPath(namespace);

        try {
            const className = require(namespace);
            className.namespace = original;

            return className;
        } catch (e) {
            throw e;
        }
    }

    _getCurrentDirectory() {
        let stacks = (new Error())
            .stack
            .split(/\n\s+/);
        let match = stacks[4] ?
            stacks[5].match(/^at\s*([\w\\. <>]+)\s\((.+):(\d+):(\d+)\)$/) :
            [];

        return match && match.length ?
            path.normalize(path.dirname(match[2])) :
            '';
    }

    _isClass(binding) {
        return _.isFunction(binding) && _.isFunction(binding.constructor) && binding.name;
    };

    _introspect(definition) {
        let _arguments = /(constructor|^function)\s*\w*\(([\s\S]*?)\)/.exec(definition);

        if (!_arguments || !_arguments[2]) {
            return [];
        }

        _arguments = _arguments.trim();

        if (_arguments.length === 0) {
            return [];
        }

        return _.map(_arguments.splice(/[ ,\n\r\t]+/), function (argument) {
            return argument.replace(/_/g, '/')
        });
    };
}

Application._isGlobal = false;

module.exports = Application;