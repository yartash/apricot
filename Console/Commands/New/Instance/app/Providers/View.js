const BaseViewProvider = use('Apricot/View/ViewProvider');

class ViewProvider extends BaseViewProvider {

}

module.exports = ViewProvider;