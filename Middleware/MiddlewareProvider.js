const BaseProvider = use('Apricot/Application/BaseProvider');

class MiddlewareProvider extends BaseProvider {
    register() {
        this.app.singleton('middleware', () => {
            const Middleware = use('Apricot/Middleware');

            return new Middleware();
        });
    }

    boot() {
        use('App/Http/Kernel');
    }
}

module.exports = MiddlewareProvider;