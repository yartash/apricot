class InvalidRouteAction extends Error {
    constructor(uses) {
        super(`Invalid route action: ${ uses }`);
    }
}

module.exports = InvalidRouteAction;