const Controller = use('App/Http/Controllers/Controller');

class WelcomeController extends Controller {
    * index() {
        return view('welcome');
    }
}

module.exports = WelcomeController;