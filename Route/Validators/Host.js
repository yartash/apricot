class HostValidator {
    match(route, request) {
        if (route.domain === '') {
            return true;
        }

        return request.host.match(new RegExp(`^${ route.domain.replace('.', '\\.') }$`, 'i')) !== null;
    }
}

module.exports = HostValidator;