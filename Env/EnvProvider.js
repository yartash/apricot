const BaseProvider = use('Apricot/Application/BaseProvider');

class EnvProvider extends BaseProvider {
    register() {
        this.app.singleton('env', () => {
            const Env = use('Apricot/Env');

            return new Env;
        });
    }
}

module.exports = EnvProvider;