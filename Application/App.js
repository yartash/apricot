module.exports = {
    providers: [
        'Apricot/Env/EnvProvider',
        'Apricot/Config/ConfigProvider',
        'Apricot/Logger/LoggerProvider',
        'App/Providers/Route',
        'Apricot/Middleware/MiddlewareProvider',
        'App/Providers/View',
        'Apricot/Server/ServerProvider'
    ],

    aliases: {
        Env: 'Apricot/Support/Facade/Env',
        Config: 'Apricot/Support/Facade/Config',
        Route: 'Apricot/Support/Facade/Route',
        Logger: 'Apricot/Support/Facade/Logger',
        Middleware: 'Apricot/Support/Facade/Middleware',
        Server: 'Apricot/Support/Facade/Server',
        View: 'Apricot/Support/Facade/View'
    }
};