const path = require('path');

const Stack = use('Apricot/Logger/Stack');

class Stream {
    get levels() {
        return [
            'debug',
            'info',
            'warning',
            'error',
            'fatal'
        ]
    }

    get stack() {
        return new Stack();
    }

    get level() {
        return this._level;
    }

    get templatePath() {
        return path.join(__dirname, '..', 'Templates');
    }

    get option() {
        return this._option;
    }

    constructor(level) {
        this._level = level;
    }

    _validateLevel(level) {
        return this.levels.indexOf(level) > -1;
    }

    _parseOption(option) {
        option = String.isString(option) ?
            {} :
            option;

        this._option = Object.assign(this.defaultOption, option);
    }
}

module.exports = Stream;