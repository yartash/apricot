const Command = use('Apricot/Console/Commands/Migrate');

const Migrator = use('Apricot/Console/Commands/Migrate/Migrator');

class Rollback extends Command {
    /**
     * The console command name.
     *
     * @public
     * @return {String}
     */
    get signature () {
        return 'migrate:rollback';
    }

    get name () {
        return 'Migrate';
    }

    /**
     * The console command description.
     *
     * @return {String}
     */
    get description () {
        return 'Rollback the last database migration.';
    }

    get optionsDefinition () {
        return {
            database: {
                description: 'The database connection to use.',
                short: 'd',
            },
            force: {
                description: 'Force the operation to run when in production.',
                short: 'f'
            },
            path: {
                description: 'The path of migrations files to be executed.',
                short: 'p'
            },
            pretend: {
                description: 'Dump the SQL queries that would be run.',
                short: 'a'
            },
            step: {
                description: 'Force the migrations to be run so they can be rolled back individually.',
                short: 'b'
            },
			realpath: {
				description: 'Indicate any provided migration file paths are pre-resolved absolute paths.',
            	short: 'r'
			}
        };
    }

    get traits () {
        return [
            'Confirmable'
        ];
    }
    
    constructor () {
        super();

        this._migrator = Migrator.instance;
    }

    * handler () {
		if (!(yield * this.confirmToProceed())) {
			return;
		}

        this._migrator.connection = this.option('database');

        // Next, we will check to see if a path option has been defined. If it has
        // we will use the path relative to the root of this installation folder
        // so that migrations may be run for any path within the applications.
        yield * this._migrator.rollback(
            this.migrationPaths,
            {
                pretend: this.option('pretend'),
                step: this.option('step'),
            }
        );

        // Once the migrator has run we will grab the note output and send it out to
        // the console screen, since the migrator itself functions without having
        // any instances of the OutputInterface contract passed into the class.
        this._migrator
            .notes
            .forEach(note => {
                this[note.style](note.message);
            });
    }
}

module.exports = Rollback;