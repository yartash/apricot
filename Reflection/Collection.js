const jsDoc = require('jsdoc3-parser');

const Reflection = use('Apricot/Reflection')

const JSDoc3ParseException = use('Exceptions/JSDoc3Parse');

class Collection {
	static get instance () {
		if (!Collection._instance) {
			Collection._instance = new Collection();
		}

		return Collection._instance;
	}

	constructor () {
		this._reflections = [];
	}

	find (namespace) {
		return new Promise(resolve => {
			let reflection = this._reflections
				.find(reflection => {
					return reflection.namespace == namespace;
			 	});

			if (reflection) {
				resolve(reflection);
			} else {
				this.parse(application.namesapceToPath(namespace))
					.then(reflection => {
						resolve(reflection);
					});
			}
		});
	}

	parse (path) {
		if (!path.endsWith('.js')) {
			path += '.js';
		}

		return new Promise(resolve => {
			jsDoc(path, (error, result) => {
				if (error) {
					throw new JSDoc3ParseException(error, path);
				}

				let reflection = new Reflection(result);

				this._reflections.push(reflection);

				resolve(reflection);
			})
		});
	}
}

module.exports = Collection;