const BaseGrammar = use('Apricot/Database/Grammar');

const Expression = use('Apricot/Database/Query/Expression');
const Blueprint = use('Apricot/Database/Schema/Blueprint');
const Fluent = use('Apricot/Support/Fluent');

class Grammar extends BaseGrammar {
	/**
	 * Get the fluent commands for the grammar.
	 *
	 * @return {Array<String>}
	 */
	get fluentCommands () {
		return this._fluentCommands;
	}

	/**
	 * The possible column modifiers.
	 *
	 * @protected
	 * @return {Array<String>}
	 */
	get modifiers () {
		return [];
	}

	/**
	 * Check if this Grammar supports schema changes wrapped in a transaction.
	 *
	 * @public
	 * @return {Boolean}
	 */
	get supportsSchemaTransactions () {
		return this._transactions;
	}

	constructor () {
		super();

		this._fluentCommands = [];

		/**
		 * If this Grammar supports schema changes wrapped in a transaction.
		 *
		 * @protected
		 * @property {Boolean}
		 */
		this._transactions = false;
	}

	/**
	 * Wrap a table in keyword identifiers.
	 *
	 * @public
	 * @param {*} table
	 * @return {String}
	 */
	wrapTable (table) {
		return super.wrapTable(table instanceof Blueprint ? table.table : table);
	}

	/**
	 * Wrap a value in keyword identifiers.
	 *
	 * @param {(Apricot/Database/Query/Expression|String)} value
	 * @param {Boolean} prefixAlias
	 * @return {String}
	 */
	wrap (value, prefixAlias = false) {
		return super.wrap(value instanceof Fluent ? value.name : value, prefixAlias);
	}

	/**
	 * Get the SQL for the column data type.
	 *
	 * @protected
	 * @param {Apricot/Support/Fluent} column
	 * @return {String}
	 */
	_getType (column) {
		return this[`_type${ column.type.ucfirst() }`](column);
	}

	/**
	 * Add the column modifiers to the definition.
	 *
	 * @protected
	 * @param {String} sql
	 * @param {Apricot/Database/Schema/Blueprint} blueprint
	 * @param {Apricot/Support/Fluent} column
	 * @return {String}
	 */
	_addModifiers (sql, blueprint, column) {
		this.modifiers.forEach(modifier => {
			let method = `_modify${modifier}`;

			if (method in this) {
				sql += this[method](blueprint, column);
			}
		});

		return sql;
	}

	/**
	 * Compile the blueprint's column definitions.
	 *
	 * @protected
	 * @param {Apricot/Database/Schema/Blueprint} blueprint
	 * @return {Array<String>}
	 */
	_getColumns (blueprint) {
		let columns = [];

		blueprint.addedColumns
			.forEach(column => {
				let sql = `${ this.wrap(column) } ${ this._getType(column) }`;

				columns.push(this._addModifiers(sql, blueprint, column));
			});

		return columns;
	}

	/**
	 * Format a value so that it can be used in "default" clauses.
	 *
	 * @protected
	 * @param {*} value
	 * @return {String}
	 */
	_getDefaultValue (value) {
		if (value instanceof Expression) {
			return value;
		}

		return Boolean.isBoolean(value) ?
			`'${ parseInt(value) }'` :
			`'${ value.toString() }'`;
	}
}

module.exports = Grammar;