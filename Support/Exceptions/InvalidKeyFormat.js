class InvalidKeyFormat extends Error {
    get collection () {
    	return this._collection;
	}

	get key () {
    	return this._key;
	}

    constructor (collection, key) {
        super(`Invalid key format ${ typeof key } from collection item`);
        
        this._collection = collection;
        this._key = key;
    }
}

module.exports = InvalidKeyFormat;