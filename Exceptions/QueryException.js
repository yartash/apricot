class QueryException extends Error {
    get sql() {
        return this._sql;
    }

    get bindings() {
        return this._bindings;
    }

    get previous() {
        return this._previous;
    }
    
    get message() {
        return this._previous.message;
    }

    constructor(query, bindings, previous){
        super(previous.message);

        this._sql = query;
        this._bindings = bindings;
        this._previous = previous;
    }
}

module.exports = QueryException;