const _ = require('lodash');
const pluralize = require('pluralize');

const Trait = use('Apricot/Support/Trait');

const MongoBuilder = use('Apricot/Database/Query/MongoBuilder');
const PostgresBuilder = use('Apricot/Database/Query/PostgresBuilder');
const GrapeMongoBuilder = use('Apricot/Database/Grape/MongoBuilder');
const GrapePostgresBuilder = use('Apricot/Database/Grape/PostgresBuilder');

const PropertyOrMethodNotExistException = use('Apricot/Exceptions/PropertyOrMethodNotExist');
const BadMethodCallException = use('Apricot/Exceptions/BadMethodCall');

class Model {
    static get proxy() {
        return {
            get: (target, name) => {
                if (name === 'prototype') {
                    return target.prototype;
                } else if (target[name] !== undefined) {
                    if (Function.isFunction(target[name])) {
                        return (...args) => {
                            return Model[name].apply({'_model': target}, args);
                        };
                    } else {
                        return target[name];
                    }
                } else {
                    Promise.reject(new PropertyOrMethodNotExistException(target.namespace));
                }
            }
        }
    }

    static set resolver(value) {
        Model._resolver = value;
    }

    static get resolver() {
        return Model._resolver;
    }

    static get traits() {
        return [
            'Concerns/GuardsAttributes',
            'Concerns/HasAttributes'
        ]
    }

    get proxy() {
        return {
            get: (target, name) => {
                if (name === 'prototype') {
                    return (new target).__proto__;
                } else {
                    return (...args) => {
                        if (target[name]) {
                            return target[name].apply(target, args);
                        } else {
                            let query = target.newQuery();

                            if (query[name]) {
                                return query[name].apply(query, args);
                            } else {
                                throw new BadMethodCallException(
                                    `Call to undefined method ${ query.constructor.name }.${ name }()`
                                );
                            }
                        }

                        return target[name].apply(target, args);
                    };
                }
            }
        }
    }

    get collection() {
        return '';
    }

    get table() {
        return '';
    }

    get connectionName() {
        return this._connection;
    }

    get connection() {
        return Model.resolveConnection(this.connectionName);
    }

    get from() {
        if (!this._collection && !this._table) {
            return pluralize.plural(this.constructor.name).snakeCase();
        }
        
        return this._table ? this._table : this._collection;
    }

    get globalScopes() {
        let identifier = this.constructor.name;

        return Model._globalScopes[identifier] ? Model._globalScopes[identifier] : {};
    }

    constructor(attributes = {}) {
        this._defaultOptions = {
            connection: undefined,
            table: '',
            primaryKey: 'id',
            keyType: 'int',
            incrementing: true,
            with: {},
            withCount: [],
            perPage: 15,
            exists: false,
            wasRecentlyCreated: false
        };

        Trait.make(
            Model,
            Model.traits,
            'Apricot/Database/Grape/',
            Trait.normalType,
            (property, value, type) => {
                if (type == 'normal' && property == 'options') {
                    Object.assign(this._defaultOptions, value);

                    return false;
                }
            }
        );

        this._init(attributes);

        this._proxy = new Proxy(
            this,
            this.proxy
        );

        return this._proxy;
    }

    static all(...args) {
        let query = (new this._model).newQuery();

        return args.length ? query.get(args) : query.get();
    }

    static clearBootedModels() {
        Model._booted = {};
        Model._globalScopes = {};
    }

    static resolveConnection(connection = '') {
        return Model.resolver.connection(connection);
    }

    static _boot() {
        Model._bootTraits();
    }

    static _bootTraits() {
        //todo
    }

    fill(attributes) {
        let totallyGuarded = this.totallyGuarded();
        let g = {ff:33, ggg:77};
        g.flip();
//     foreach ($this->fillableFromArray($attributes) as $key => $value) {
//     $key = $this->removeTableFromKey($key);
//
//     // The developers may choose to place some attributes in the "fillable" array
//     // which means only those attributes may be set through mass assignment to
//     // the model, and all others will just get ignored for security reasons.
//     if ($this->isFillable($key)) {
//     $this->setAttribute($key, $value);
// } elseif ($totallyGuarded) {
//     throw new MassAssignmentException($key);
// }
// }

        return this;
    }

    newInstance(args, exists = false) {
        let model = new this.constructor(args);
    }

    newQuery() {
        let builder = this.newQueryWithoutScopes();
        let scopes = this.globalScopes;

        for (let identifier in scopes) {
            builder.withGlobalScope(identifier, scopes[identifier]);
        }

        return builder;
    }

    newQueryWithoutScopes() {
        let builder = this.newEloquentBuilder(this._newBaseQueryBuilder());
        
        return builder
            .setModel(this)
            .setProxy(this._proxy)
            .with(this)
            .withCount(this);
    }

    newEloquentBuilder(query) {
        switch (query.driver) {
            case 'mongo':
                return new GrapeMongoBuilder(query);
            case 'pgsql':
                return new GrapePostgresBuilder(query);
        }
    }

    syncOriginal() {
        this._original = this._attributes;

        return this;
    }

    _init(attributes) {
        this._parseOptions();
        this._bootIfNotBooted();
        this.syncOriginal();
        this.fill(attributes);
    }

    _bootIfNotBooted() {
        if (!Model.booted[this.constructor.name]) {
            Model.booted[this.constructor.name] = true;
            //todo event booting

            Model._boot();

            //todo event booted
        }
    }

    _parseOptions() {
        let properties = Object.assign(this._defaultOptions, this.options);

        for(let key in properties) {
            this[`_${ key }`] = properties[key];
        }
    }

    _newBaseQueryBuilder() {
        let connection = this.connection;

        switch (connection.driver) {
            case 'mongo':
                return new MongoBuilder(connection);
            case 'pgsql':
                return new PostgresBuilder(connection);
        }
    }
}

Model.booted = {};

Trait.make(
    Model,
    Model.traits,
    'Apricot/Database/Grape/',
    Trait.staticType
);

module.exports = new Proxy(Model, Model.proxy);