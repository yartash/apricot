class PropertyOrMethodNotExist extends Error {
    constructor(namespace, key) {
        super(`Propery or method does not exist at class ${ namespace }`);

        this._key = key;
        this._namespace = namespace;
    }
}

module.exports = PropertyOrMethodNotExist;