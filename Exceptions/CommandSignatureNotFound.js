class CommandSignatureNotFound extends Error {
    constructor(command) {
        super(`Command signature property is missing in ${ command }.`);
    }
}

module.exports = CommandSignatureNotFound;