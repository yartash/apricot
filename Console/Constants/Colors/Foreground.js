class Foreground {
    static get system () {
        return '\x1b[39m';
    }

    static get black () {
        return '\x1b[30m';
    }

    static get red () {
        return '\x1b[31m';
    }

    static get green () {
        return '\x1b[32m';
    }

    static get yellow () {
        return '\x1b[33m';
    }

    static get blue () {
        return '\x1b[34m';
    }

    static get magenta () {
        return '\x1b[35m';
    }

    static get cyan () {
        return '\x1b[36m';
    }

    static get lightGray () {
        return '\x1b[37m';
    }

    static get darkGray () {
        return '\x1b[90m';
    }

    static get lightRed () {
        return '\x1b[91m';
    }

    static get lightGreen () {
        return '\x1b[92m';
    }

    static get lightYellow () {
        return '\x1b[93m';
    }

    static get lightBlue () {
        return '\x1b[94m';
    }

    static get lightMagenta () {
        return '\x1b[95m';
    }

    static get lightCyan () {
        return '\x1b[96m';
    }

    static get white () {
        return '\x1b[97m';
    }
}

module.exports = Foreground;