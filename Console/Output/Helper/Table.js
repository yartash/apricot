const TableStyle = use('TableStyle');

const InvalidArgumentException = use('Apricot/Exceptions/InvalidArgument')

class Table {
    /**
     * Sets table style.
     *
     * @public
     * @param {Apricot/Console/Output/Helper/TableStyle|String} name The style name or a TableStyle instance
     */
    set style (name) {
        this._style = this._resolveStyle(name);
    }

    constructor (output) {
        /**
         * Table headers.
         *
         * @private
         * @property {Array<String>}
         */
        this._headers = [];

        /**
         * Table rows.
         *
         * @private
         * @property {Array<String>}
         */
        this._rows = [];

        /**
         * Column widths cache.
         *
         * @private
         * @property {Array<Int>}
         */
        this._effectiveColumnWidths = [];

        /**
         * Number of columns cache.
         *
         * @private
         * @property {Int}
         */
        this._numberOfColumns = 0;

        /**
         * @private
         * @property {Apricot/Console/Output}
         */
        this._output = output;

        /**
         * @private
         * @property {Apricot/Console/Output/Helper/TableStyle}
         */
        this._style;

        /**
         * @private
         * @property {Array<Apricot/Console/Output/Helper/TableStyle>}
         */
        this._columnStyles = [];

        /**
         * User set column widths.
         *
         * @private
         * @property {Array<Int>}
         */
        this._columnWidths = [];

        if (Table.styles === null) {
            Table.styles = Table._initStyles();
        }

        this.style = 'default';
    }

    static _initStyles () {
        let borderless = new TableStyle();
        borderless.setHorizontalBorderChar('=')
    // ->setVerticalBorderChar(' ')
    // ->setCrossingChar(' ')
    // ;
    //
    //     $compact = new TableStyle();
    //     $compact
    //         ->setHorizontalBorderChar('')
    // ->setVerticalBorderChar(' ')
    // ->setCrossingChar('')
    // ->setCellRowContentFormat('%s')
    // ;
    //
    //     $styleGuide = new TableStyle();
    //     $styleGuide
    //         ->setHorizontalBorderChar('-')
    // ->setVerticalBorderChar(' ')
    // ->setCrossingChar(' ')
    // ->setCellHeaderFormat('%s')
    // ;
    //
    //     return array(
    //         'default' => new TableStyle(),
    //     'borderless' => $borderless,
    //     'compact' => $compact,
    //     'symfony-style-guide' => $styleGuide,
    // );
    }

    /**
     * @private
     */
    _resolveStyle (name) {
        if (name instanceof TableStyle) {
            return name;
        }

        if (Table.styles[name]) {
            return Table.styles[name];
        }

        throw new InvalidArgumentException(`Style "${ name }" is not defined.`);
    }
}

Table.styles = null;

module.exports = Table;