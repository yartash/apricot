const Command = use('Apricot/Console/Command');

const Model = use('Apricot/Database/Grape/Model');
const GeneratorHandler = use('Apricot/Support/GeneratorHandler');

class Seed extends Command {
	get signature () {
		return 'db:seed';
	}

	get name () {
		return 'Seed';
	}

	get description () {
		return 'Seed the database with records';
	}

    get optionsDefinition () {
        return {
            class: {
                description: 'The class name of the root seeder.',
                short: 'c',
                default: 'DatabaseSeeder'
            },
            database: {
                description: 'The database connection to seed.',
                short: 'd'
            },
            force: {
                description: 'Force the operation to run when in production.',
                short: 'f'
            }
        };
    }

	get traits () {
		return [
			'Confirmable'
		];
	}

	/**
	 * Get the name of the database connection to use.
	 *
	 * @protected
	 * @return {String}
	 */
	get _database () {
		let database = this.option('database');

		return database ? database : use('Config').get('database.default');
	}

    /**
     * Get a seeder instance from the container.
     *
     * @protected
     * @return {Apricot/Database/Seeder}
     */
    get _seeder () {
        let instance = application.make(this.option('class'));
        instance.command = this;

        return instance;
    }

	constructor () {
		super();

		/**
		 * The connection resolver instance.
		 *
		 * @protected
		 * @property {Apricot/Database/Connection}
		 */
		this._resolver = use('DB');
	}

	* handler () {
		if (!(yield * this.confirmToProceed())) {
			return;
		}

		this._resolver.defaultConnection = this._database;

        yield Model.unguarded(() => {
            return new Promise(resolve => {
                (new GeneratorHandler(resolve)).executor(this._seeder.invoke());
            })
        });
	}
}

module.exports = Seed;