const readline = require('readline');

const ForegroundColor = use('Apricot/Console/Constants/Colors/Foreground');
const BackgroundColor = use('Apricot/Console/Constants/Colors/Background');
const FormatSet = use('Apricot/Console/Constants/Formats/Set');
const FormatReset = use('Apricot/Console/Constants/Formats/Reset');

class Index {
    static get instance () {
        if (!Index._instance) {
            Index._instance = new Index();
        }

        return Index._instance;
    }

    constructor () {
        this._createInterface(true)
    }

    ask (text) {
        return new Promise(resolve => {
            this._createInterface();

            this._readline.question(text, answer => {
                this._createInterface(true);
                resolve(answer);
            });
        });
    }

	confirm (text) {
		return this.ask(`${ text } (yes/no) [no]:\n> `)
            .then(result => {
                return result === '' ?
                    false :
                    (
                        result === 'yes' ? true : false
                    );
            });
	}

    error (text) {
        this.writeLine(`${ ForegroundColor.red }${ FormatSet.bold }Error${ FormatReset.bold} ${ ForegroundColor.red }${ text }`);
    }

    info (text) {
        this.writeLine(`${ ForegroundColor.blue }Info${ ForegroundColor.blue } ${ text }`);
    }

	comment (text) {
        this.writeLine(`${ ForegroundColor.yellow }Comment${ ForegroundColor.yellow } ${ text }`);
    }

	alert (text) {
		this.writeLine(`${ BackgroundColor.yellow }${ ForegroundColor.white }${ '*'.repeat(text.length + 12) }`);
		this.writeLine(`${ BackgroundColor.yellow }${ ForegroundColor.white }*     ${ text }     *`);
		this.writeLine(`${ BackgroundColor.yellow }${ ForegroundColor.white }${ '*'.repeat(text.length + 12) }`);
		this.writeLine('');
	}

    writeLine (text) {
        this._readline.write(`${ FormatReset.all }${ text }\n`);
    }

    write (text) {
        this._readline.write(`${ FormatReset.all }${ text }`);
    }
    
    close () {
        this._readline.close();
    }

    /**
     * @private
     * @param {boolean} terminal=false
     */
    _createInterface (terminal = false) {
        if (this._readline) {
            this.close();
        }

        this._readline = readline.createInterface({
            input: process.stdin,
            output: process.stdout,
            terminal: terminal
        });
    }
}

Index._instance = null;

module.exports = Index;