class CompilesLoops {
    compileEach(expression) {
        let matches = expression
            .match(/^([a-zA-Z0-9]+)\s*:?\s*([a-zA-Z0-9]+)?\s+in\s+([a-zA-Z0-9]+)$/);

        if (matches.length) {
            matches.shift();
            matches = matches.reverse();

            return `<% _.each(${ matches[0] }, (${ matches[1] ? `${ matches[1] }, ${ matches[2] }` : matches[2] }) => { %>`;
        }
    }

    compileWhile(expression) {
        return `<% while(${ expression }) { %>`;
    }

    compileEndEach() {
        return `<% }) %>`;
    }

    compileEndWhile() {
        return `<% } %>`;
    }
}

module.exports = CompilesLoops;