Object.defineProperty(
    Object,
    'isObject',
    {
        value: value => {
            return Object.prototype.toString.call(value) === '[object Object]';
        },
        configurable: true,
        writable: true,
        enumerable: false
    }
);

Object.defineProperty(
    Object.prototype,
    'flip',
    {
        value: function() {
            let result = Object.create(null);;

            for (let key in this) {
                if (String.isString(this[key]) || Number.isNumber(this[key])) {
                    result[this[key]] = key;
                }
            }

            return result;
        },
        configurable: false,
        writable: false,
        enumerable: false
    }
);

Object.defineProperty(
    Object.prototype,
    'except',
    {
        value: function(fileds) {
            let result = Object.create(null);

            for (let key in this) {
                if (fileds.indexOf(key) == -1) {
                    result[key] = this[key];
                }
            }

            return result;
        },
        configurable: false,
        writable: false,
        enumerable: false
    }
);