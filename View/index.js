const _ = require('lodash');

const Collection = use('Apricot/Support/Collection');

class View {
    constructor(factory, engine, view, path, data = {}, composer = new Set()) {
        this._view = view;
        this._path = path;
        this._engine = engine;
        this._factory = factory;
        this._data = data;
        this._composer = composer;
    }

    * render(callback = null) {
        try {
            let contents = yield * this._renderContents();
            let response = callback !== null ? callback(this, contents) : null;

            this._factory.flushStateIfDoneRendering();

            return response !== null ? response : contents;
        }
        catch (err) {
            let h = 0;
        }
    }

    * _renderContents() {
        this._factory.incrementRender();

        yield this._makeComposerData();

        let content = yield * this._getContents();

        this._factory.decrementRender();

        return content;
    }

    * _getContents() {
        return yield * this
            ._engine
            .get(
                this._path,
                this._gatherData()
            )
    }

    _gatherData() {
        return _
            .forIn(
                _.assignIn(this._factory.shared, this._data, this._composerData),
                value => {
                    if (value.render) {
                        return value.render();
                    } else {
                        return value;
                    }
                }
            )
    }

    _makeComposerData() {
        return new Promise(resolve => {
            let allPromise = [];

            for (let callback of this._composer) {
                allPromise.push(
                    new Promise(resolve => {
                        function executor(generator, yieldValue) {
                            let next = generator.next(yieldValue);

                            if (!next.done) {
                                if (
                                    next.value.constructor
                                ) {
                                    switch (true) {
                                        case next.value.constructor.name == 'Promise':
                                            next.value.then(
                                                result => {
                                                    if (result instanceof Collection) {
                                                        result.run().then(result => {
                                                            executor(handler, result);
                                                        });
                                                    } else {
                                                        executor(generator, result);
                                                    }
                                                },
                                                err => {
                                                    generator.throw(err);
                                                }
                                            );
                                            break;
                                        case
                                        next.value.constructor[Symbol.toStringTag] &&
                                        next.value.constructor[Symbol.toStringTag] == 'GeneratorFunction':

                                            break;
										case next.value instanceof Collection:
											next.value.run().then(
												result => {
													this._executeHandle(handler, result.first())
												},
												err => {
													handler.throw(err);
												}
											);
											break;
                                    }
                                }
                            } else {
                                resolve(next.value);
                            }
                        }

                        executor(callback());
                    })
                );
            }

            Promise
                .all(allPromise)
                .then(data => {
                    let result = {};

                    for (let item of data) {
                        result = _.assignIn(result, item);
                    }

                    this._composerData = result;
                    resolve();
                });
        });
    }
}

module.exports = View;