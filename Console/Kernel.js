class Kernel {    
    get coreCommands () {
        return [
            'Apricot/Console/Commands/New',
            'Apricot/Console/Commands/Seed'
        ]
            .concat(
                this.makeCommand,
                this.migrateCommand
            );
    }

    /**
     * Get Migrate commands.
     * @return {Array<string>}
     */
    get migrateCommand () {
        return [
            'Apricot/Console/Commands/Migrate/Migrate',
            'Apricot/Console/Commands/Migrate/Install',
            'Apricot/Console/Commands/Migrate/Rollback',
            'Apricot/Console/Commands/Migrate/Fresh',
            'Apricot/Console/Commands/Migrate/Reset',
            'Apricot/Console/Commands/Migrate/Refresh',
            'Apricot/Console/Commands/Migrate/Status'
        ]
    }

    /**
     * Get Make commands.
     * @return {Array<string>}
     */
    get makeCommand () {
        return [
            'Apricot/Console/Commands/Make/Migration',
            'Apricot/Console/Commands/Make/Middleware',
            'Apricot/Console/Commands/Make/Command',
            'Apricot/Console/Commands/Make/Provider',
            'Apricot/Console/Commands/Make/Seeder',
        ]
    }

	get commands () {
		return [];
	}

    constructor () {
    }
    
    handler () {
        let command = Peach.match(this.signature);

        if (!command) {

        } else {
            this._commandNotFound()
        }
    }

    _registerCommand () {
             
    }

    _commandNotFound () {
        this._output
            .error(`Not found this command`)
    }
}

module.exports = Kernel;