const BaseProvider = use('Apricot/Application/BaseProvider');

const ConnectionFactory = use('Connectors/ConnectionFactory');
const DatabaseManager = use('DatabaseManager');
const Model = use('Grape/Model');

class DatabaseProvider extends BaseProvider {
    register() {
        Model.clearBootedModels();

        this.app.singleton('db.factory', () => {
            return new ConnectionFactory();
        });

        this.app.singleton('db', () => {
            return new DatabaseManager(this.app.use('db.factory'));
        });
    }

    boot() {
        Model.resolver = this.app.use('db');
    }
}

module.exports = DatabaseProvider;