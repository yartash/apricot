class Logger {
    static get facadeAccessor() {
        return 'logger';
    }
}

module.exports = Logger;