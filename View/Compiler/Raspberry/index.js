const fs = require('fs');
const _ = require('lodash');

const Compiler = use('Apricot/View/Compiler');

class Raspberry extends Compiler {
    get traits() {
        return [
            'Raspberry/Concerns/CompilesAsset',
            'Raspberry/Concerns/CompilesComments',
            'Raspberry/Concerns/CompilesConditionals',
            'Raspberry/Concerns/CompilesEchos',
            'Raspberry/Concerns/CompilesLoops',
        ]
    }

    get verbatimPlaceholder() {
        return '@__verbatim__@';
    }

    get contentTags() {
        return ['{{', '}}'];
    }

    get rawTags() {
        return ['{!!', '!!}'];
    }

    get escapedTags() {
        return ['{{{', '}}}'];
    }

    get echoFormat() {
        return '_.escape(%s)';
    }

    get compilers() {
        return [
            'Comments',
            'Extensions',
            'Statements',
            'Echos'
        ];
    }

    get extensions() {
        return this._extensions;
    }

    get path() {
        return this._path;
    }

    set path(value) {
        this._path = value;
    }

    constructor(cachePath) {
        super(cachePath);

        this._verbatimBlocks = [];
        this._extensions = [];
        this._customDirectives = {};
        this._footer = [];
    }

    * compile(path = null) {
        if (path) {
            this.path = path;
        }

        if (!_.isNull(this._cachePath)) {
            let content = yield * this._compileString(fs.readFileSync(this.path, 'utf8'));

            fs.writeFileSync(this.getCompiledPath(this.path), content, 'utf8')
        }
    }

    extend(closure) {
        this._extensions.push[closure];
    }

    * _compileString(content) {
        if (content.indexOf('@verbatim') > -1) {
            content = this._storeVerbatimBlocks(content);
        }

        if (content.indexOf('@js') > -1) {
            content = this._storeJavaScriptBlocks(content);
        }

        content = yield * this._parseTemplate(content);

        if (this._verbatimBlocks.length) {
            content = this._restoreVerbatimBlocks(content);
        }

        return content;
    }

    _storeVerbatimBlocks(content) {
        return content.replace(/@verbatim([\s\S]*?)@endverbatim/gm, (...match) => {
            this._verbatimBlocks.push(match[1]);

            return this.verbatimPlaceholder;
        });
    }

    _storeJavaScriptBlocks(content) {
        return content.replace(/@js([\s\S]*?)@endJs/gm, (...match) => {
            return `<% ${ match[1] } %>`;
        });
    }

    _restoreVerbatimBlocks(content) {
        return content.replace(new RegExp(this.verbatimPlaceholder, 'gm'), () => {
            return this._verbatimBlocks.shift();
        });
    }

    * _parseTemplate(content) {
        let compilers = this.compilers;

        for (let i = 0; i < compilers.length; i++) {
            content = yield * this[`_compile${ compilers[i] }`](content);
        }

        return content;
    }

    * _compileExtensions(content) {
        let extensions = this.extensions;

        for (let i = 0; i < extensions.length; i++) {
            content = yield extension.call(this, content);
        }

        return content;
    }

    * _compileStatements(content) {
        return content.replace(/\B@(@?\w+(?:::\w+)?)([ 	]*)(\(([^()]+)*\))?/gm, (...match) => {
            return this._compileStatement(match);
        });
    }

    _compileStatement(match) {
        if (match[1].indexOf('@') > -1) {
            match[0] = match[3] ? match[1] + match[3] : match[1];
        } else if (this._customDirectives[match[1]]) {
            match[0] = this._callCustomDirective(match[1], match[3] ? match[3] : '')
        } else if (this[`compile${ _.upperFirst(match[1]) }`]) {
            match[0] = this[`compile${ _.upperFirst(match[1]) }`]
                .apply(
                    this,
                    (
                        match[3] ?
                            _
                                .map(
                                    match[4].split(/,\s*/),
                                    param => {
                                        return param;
                                    }
                                ) :
                            []
                    )
                );
        }

        return match[3] ? match[0] : match[0] + match[2];
    }

    _callCustomDirective(name, value) {
        value = value.replace(/[(|)]/g, '')

        return this._customDirectives[name](_.trim(value));
    }
}

module.exports = Raspberry;