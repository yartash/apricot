Object.defineProperty(
	Date,
	'isDate',
	{
		value: value => {
			return Object.prototype.toString.call(value) === '[object Date]';
		},
		configurable: false,
		writable: false,
		enumerable: false
	}
);